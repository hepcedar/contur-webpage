How this works
--------------

The webpages are built as part of the CI, and for commits to `main`, they are also deployed here:
https://hepcedar.gitlab.io/contur-webpage/

So the development workflow should be to prepare the webpages for a new release in a branch named for the release, then when it is ready and the release is made, merge it into `main`. Probably a good idea to keep the release-named branch around too for archive documenatation purposes.

To build the webpages locally, clone the repo and execute `make` in the Sphinx directory.

The following files should not be edited by hand because they are built automatically by ``contur``.

-  ``contur-anas.bib`` bibliography file for all rivet analyses used by Contur.
-  ``datasets/data-list.rst`` web page listing all the analyses (by pool) used by Contur
-  Files in ``datasets/SM``. Webpages giving info on the Standard Model theory predictions known to Contur.

To rebuild those files, you need a working contur installation. Executing ``make all`` or ``make webpages``
in contur will to this. If the environment variable `$CONTUR_WEBDIR` is set appropriately to the root
of your webpage project clone, then they will be written to the right place in the Sphinx area. If not,
you can copy them over by hand.




