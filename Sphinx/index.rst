Constraints On New Theories Using Rivet
=======================================
**Exploring the sensitivity of unfolded collider measurements to BSM models**

The write-up for Contur 2.0, the first general user release, is available
here: :cite:`Buckley:2021neu`. However, the `README pages <https://gitlab.com/hepcedar/contur/-/blob/main/README.md?ref_type=heads>`_ 
on our gitlab
project are more up to date. 

We also have a support mailing list where volunteer developers will do their best to answer your
questions: contur-support@cern.ch



Jump to the source code and documentation

.. toctree::
   :maxdepth: 1 

   codedocs/running

**Contents:**

.. toctree::
   :maxdepth: 1

   introduction
   method/index
   datasets/index
   results/index
   share/plot-format
   zzz_contur_bibl
   paper/acknowledgments
   
For the method, please cite "Constraining new physics with collider measurements of Standard Model signatures" :cite:`Butterworth:2016sqg`.
   
..
  Indices and tables
  ==================

  * :ref:`genindex`
  * :ref:`modindex`
  * :ref:`search`
