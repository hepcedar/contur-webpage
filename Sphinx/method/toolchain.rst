Toolchain
---------


.. figure:: /images/contur-toolchain.png
      :alt: Contur Toolchain
      
The software tool chain used in :cite:`Butterworth:2016sqg`. Other generators and interfaces may also be 
substituted for e.g. Feynrules or Herwig7.

Contur exploits three important developments to survey existing
measurements and set limits on new physics.

#. SM predictions for differential and exclusive, or semi-exclusive,
   final states are made using sophisticated calculational software,
   often embedded in Monte Carlo generators capable of simulating full,
   realistic final states :cite:`Buckley:2011ms`. These
   generators now incorporate matrix-elements for higher-order processes
   matched to logarithmic parton showers, and successful models of soft
   physics such as hadronisation and the underlying event. 

#. As the search for many of the favoured BSM scenarios has been
   unsuccessful, there has been a diverisification of models for new
   physics, including simplified models :cite:`Alves:2011wf,Abercrombie:2015wmb`, 
   complementing potentially ultra-violet complete theories such as Supersymmetry, and effective lower-energy theories (EFTs).   
   All these approaches are readily imported into the event generators moentioned above, thus
   allowing the rapid prediction of their impact on a wide variety of
   final states simultaneously. In this paper we make extensive use of
   these capabilities within
   Herwig 7 :cite:`Bellm:2015jjp,Bahr:2008pv`.

#. The precision measurements from the LHC have mostly been made in a
   manner which minimises their model-dependence. That is, they are
   defined in terms of final-state signatures in fiducial regions
   well-matched to the acceptance of the detector. Many such
   measurements are readily available for analysis and comparison in the
   Rivet library :cite:`Buckley:2010ar,Bierlich:2019rhm`.

These three developments together make it possible to efficiently bring
the power of a very wide range of data to bear on the search for new
physics. While such a generic approach is unlikely to compete in terms
of sensitivity with a search optimised for a specific theory,
the breadth of potential signatures and models which can be covered
makes it a powerful complementary approach. [1]_ 

.. [1]
   Limits from existing searches can sometimes be applied to new models,
   for example by accessing archived versions of the original analysis
   code and detector simulation via the
   RECAST :cite:`Cranmer:2010hk` project, or by independent
   implementations of experimental searches, see, for example,
   Refs. :cite:`Conte:2012fm,Drees:2013wra,Kraml:2013mwa,Papucci:2014rja,Barducci:2014ila`.
