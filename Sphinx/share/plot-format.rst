Explanation of Contur and Rivet plot format
-------------------------------------------


**Heatmaps and contours**

The black solid line is the 95% confidence exclusion, and the black dashed line is 68%. The black dotted line is the
95% expected exclusion.

The multi-coloured plots show which final state signature gave the dominant exclusion at each parameter point. The
legend refers to the pool definitions on the "measurements" page (see sidebar).

In the yellow-and-green double-plot layout, the left-hand plot shows the exclusion conturs, and the right-hand plot shows the binned
heatmap from which the contour on the left is drawn. This indicates the actual
parameter binning used, and shows the full range of sensitivity, according to the colour key on the far right.

Sometimes also relevant limits from theoretical constraints, or from other measurements not used in Contur, are also shownn.

When running using data as background, the white solid line is the 95% confidence exclusion, and the white dashed line is 68%.

**Contur web page for a single parameter point**

For a run on a single parameter point (yoda file), an html page with links to all the histograms may be produced using contur-mkhtml.

The main index page starts with the overall exclusion, and lists the parameters if the parameter file was available. 
Then the plots from each category which gave this exclusion are shown. Only the most discrepant measurement from each category is used,
and the most sensitive expected and actual exclusion plots are shown (when we have a SM prediction to use) or the exclusion using
data as background (when we don't).

Sometimes the "most discrepant" measurement is actually several non-overlapping histograms, which are statistically uncorrelated and
so are treated effectively as a signal histogram by Contur.

Below this is a comprehensive list of all plots considered, along with links to the experimental papers and the plots themselves.

**Rivet plots**

The plots all have two sections.

*   The top shows either

   *    (if we have a SM prediction available) the measurement (black point with error bars) compared to the sum of the SM prediction plus any contribution from the BSM scenario under consideration (blue histogram). The SM prediction is also shown as a green histogram.
   *    (if we don't have a SM prediction available) the measurement (black point with error bars) compared to the sum of the measurement plus any contribution from the BSM scenario under consideration (red histogram). 

*   The lower section displays the same information, but now as a significance ratio of the stacked  BSM+SM (blue histogram) or BSM+data (red) over the measurement.  The yellow band indicates the one sigma deviation level (taking into account the uncertainties on the measurement, SM prediction, and the MC statistical uncertainties on the BSM signal.  So, loosely, if the blue or red histogram has an excursion beyond the yellow band, expect an exclusion of 68% or more (although correlations may affect that).

If correlation information has not been used, then the number in square brackets in the legend indicates the bin number of the 
bin giving the exclusion (Contur will have used only the most discrepant bin, as a conservative way of accounting for correlated 
uncertainties). If correlations have been used, the legend will say "correlated all" since all bins can contribute.
The number next to this is the confidence level of the exclusion (i.e. 1-p), either from that bin, or from the whole histogram 
in the correlated case.

