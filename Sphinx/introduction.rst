Introduction
============

.. role:: smallcaps

**Is the Standard Model Isolated?**
	   
The Large Hadron Collider (LHC) is probing physics in a new kinematic region, at energies around and above the electroweak 
symmetry-breaking scale. With the discovery of the Higgs boson :cite:`Aad:2012tfa,Chatrchyan:2012ufa`, ATLAS and CMS 
demonstrated that the understanding of electroweak symmetry-breaking within 
the Standard Model (SM) is broadly correct, and thus that the theory is potentially valid well above the TeV scale. 
Many precision measurements have been published, reaching into this new kinematic domain.

The predictions of the SM are so far generally in agreement with the data. Many dedicated searches for physics
beyond the SM (BSM) have excluded a wide range of possible scenarios. Nevertheless, there are reasons to be confident that 
the SM is not the full story; examples include the gravitational evidence for dark matter, 
the large preponderance of matter over antimatter in the universe, and the existence of gravity itself. None of
these can be easily accommodated within the SM alone.

This motivates a continued campaign to make precise measurements and calculations at higher energies and luminosities, 
and to exploit these to narrow the range of possible BSM scenarios and highlight viable new theories, or at least constrain the energy 
scale at which new physics might be observed at future experiments. 
*Whether BSM physics is discovered or not at the LHC or its high-luminosity upgrades, there is a need to extract the clearest and most generic information about physics in this new energy regime above the electroweak symmetry-breaking scale.*

A combination of comprehensive measurements, such as those used by Contur, and specific searches for less generic final states 
(such as long lived particles, or dark showers, for example) should be able to answer the question as to whether BSM physics is
close enough to the electroweak symmetry-breaking scale to be within direct reach of the LHC or, to coin a phrase, 
establish the isolated Standard Model (Wells, Zhang and Zhao, 2017 :cite:`Wells:2017aoy`, 
see also `Is the Standard Model Isolated? <https://www.theguardian.com/science/life-and-physics/2017/mar/12/is-the-standard-model-isolated>`_ )

------------------

The original Contur procedure is defined in a 'white paper' :cite:`Butterworth:2016sqg`, which should be used as a 
reference for the method. More detail is also given in :cite:`Yallup:2020aph`.

Currently, Contur produces combined sensitivity limits derived from comparisons between theoretical BSM simulations and data at the 
particle-level. 
That is, the theory simulates a fully-exclusive final state, and the data have been corrected for detector effects. 

Contur exploits the fact that particle-level differential measurements made in fiducial regions of phase-space have a high 
degree of model-independence. These measurements can therefore be compared to BSM physics implemented in Monte Carlo 
generators in a very generic way, allowing a wider array of models and final states to be considered than is typically the case. 
The Contur approach should be seen as complementary to the discovery potential of direct searches, being designed to 
eliminate inconsistent BSM proposals in a context where many (but perhaps not all) measurements are consistent with the 
Standard Model.

Although most of the results here do not currently do this, Contur can also make direct use of precision SM calculations, and so
can in principle in future highlight BSM scenarios which describe the data better than the SM. Should any exist. 


-------------------

**What to expect of these pages**

These pages should contain the current status of the Contur project, results obtained so far, 
and documentation for using it yourself. They define the methodology, and provide an archive of the models 
and datasets tested with the tools. Contur is intended to be easily extensible not just to considering new scenarios, 
but to the inclusion of additional data in the limit setting process. When particularly significant new models or datasets 
are added, it is anticipated that further papers will be written and linked as snapshots of progress. But these pages should 
always contain the most up-to-date status.
	   
