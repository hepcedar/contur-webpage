Acknowledgments
===============

This work started at the *Interdisciplinary Workshop on Models,
simulations and data at LHC* in Edinburgh, and continued in the 2015 (and later) Les
Houches meetings on TeV-scale physics, and two MCnet schools in Göttingen.
The authors thank the organisers, especially Michela Massimi, Fawzi
Boudjema and Steffen Schumann. They also thank Josh McFayden for useful
discussions, and STFC for financial support. This work was supported in
part by the European Union as part of the FP7 Marie Curie Initial
Training Network MCnetITN (PITN-GA-2012-315877). MK is supported by the
German Research Foundation DFG through the research unit 2239 “New
physics at the LHC". Other acknowledgements for specific :doc:`results <../results/index>` are noted on
the appropriate pages.

