BSM Models
----------

Several BSM models have been used with Contur already, and the results can be seen :doc:`here <../results/index>`. If you
want to do further studies with these models, the UFO files for most of them, along with some example Herwig input and parameter steering
files, are available in Contur's `Model repository <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models>`_. 

Other models are available
for example in the `Feynrules Model Database <https://feynrules.irmp.ucl.ac.be/wiki/ModelDatabaseMainPage>`_.

