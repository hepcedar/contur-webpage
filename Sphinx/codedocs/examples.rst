Examples
--------

Running `make check` when you have contur installed will run some tests including regression tests on some pre-make
Rivet output.

For more examples, the best place to start is the  `README file <https://gitlab.com/hepcedar/contur/-/blob/main/README.md>`_ . 




