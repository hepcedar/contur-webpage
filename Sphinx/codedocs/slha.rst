Running on SUSY Les Houches Accord (SLHA) files
-----------------------------------------------

Rather than taking its parameters directly from ``param_file.dat``, contur
can run over all SUSY Les Houches Accord :cite:`Allanach:2008qq` 
files in a given directory. It uses the pyslha package
:cite:`Buckley:2013jua` to read and write the SLHA files.

**Scanning over a directory full of SLHA files**

To tell it to do this, 
your ``param_file.dat`` should look like this::

   #Based on configObj python template
   [Run]
   generator ='[path to my environment setup]setupEnv.sh'
   contur ='[path to my contur directory]setupContur.sh'

   [Parameters]
   [[slha_file]]
   mode = DIR
   name = "/work/myslha/files"

where the SLHA files are all in the ``/work/myslha/files`` directory.

The ``LHC.in`` file then does not need to contain explicit parameter settings, but 
must have the lines::

   # read model
   # MSSM
   read MSSM.model
   cd /Herwig/NewPhysics

   setup MSSM/Model {slha_file}

**Scanning from a single template SLHA file**

It is also possible (though not strictly correct) to do a scan based on a single SLHA file,
scaling the parameters in the file by different values as the scan progresses.

To do this, place the SLHA file in your working directory, and set up the ``param_file.dat`` giving its 
name and the parameter blocks you wish to scale, as follows::

   #Based on configObj python template
   [Run]
   generator ='[path to my environment setup]setupEnv.sh'
   contur ='[path to my contur directory]setupContur.sh'

   [Parameters]
   [[slha_file]]
   mode = SINGLE
   name = RPV-UDD.slha
   [[MASS]]
   mode = LIN
   start = 0.2
   stop = 2.0
   number = 10
   [[RVLAMUDD]]
   mode = LIN
   start = 0.1
   stop = 10.0
   number = 10

As you can see, as well as the name of the base SLHA file, we give a range of values by which parameters
in the file this will be scaled. The parameter name is the name of the block. All parameters in the
block will be multiplied by the parameter values specific in the usual way. All the usual modes
can be used. 

The ``LHC.in`` still has to contain fields for all parameters, even though the SLHA block names are not 
directly used there and should be commented out, as follows::

   setup MSSM/Model {slha_file}
   # Changing MASS {MASS} and RPV UDD coupling {RVLAMUDD}

**Making and plotting heatmaps with SLHA parameters**

To make heatmaps and store parameters from the SLHA files, see the ``-S SLHA`` option, where
``SLHA`` is the name of the SLHA parameter block required (so far only tested with the MASS block).
If the SLHA files are unevenly space, you may also wish to rebin the heatmap points using the ``-BW``
and ``-BO`` options.


