Using Contur
============


Getting Contur
--------------

Check out the contur code from our repository_ at gitlab and checkout the release you want to use. The README
file there contains setup instructions.

  .. _repository: https://gitlab.com/hepcedar/contur

We always recommend the latest release_.

  .. _release: https://gitlab.com/hepcedar/contur/-/releases

Running Contur
--------------

The manual for Contur 2.0, the first general user release, is available
here: :cite:`Buckley:2021neu`, and contains examples and instructions on
how to run Contur.

You can find a video tutorial on running Contur here_, and there are also some
updated slides_ for the same tutorial.

  .. _here: https://www.youtube.com/channel/UCvlP4hgV-U-sI8-3opxmi3Q/playlists
  .. _slides: https://gitlab.com/hepcedar/mcnet-schools/dresden-2021/-/tree/main/contur

To get some information about the available options, see ``contur --help``.

.. toctree::
   :maxdepth: 1

   examples	      
   models
   slha   

Code Documention
----------------

Available at this link_.

  .. _link: https://hepcedar.gitlab.io/contur/


Adding to Contur
----------------


Adding a new `measurement`_.
   
  .. _`measurement`: https://gitlab.com/hepcedar/contur/-/blob/main/data/DB/README.md


Adding new `SM theory predictions`_.
   
  .. _`SM theory predictions`: https://gitlab.com/hepcedar/contur/-/blob/main/data/Theory/README.md


