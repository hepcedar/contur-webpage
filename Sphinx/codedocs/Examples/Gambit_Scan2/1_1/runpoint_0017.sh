source /unix/cedar/software/cos7/Herwig-repo_Rivet-repo/setupEnv.sh;
source /home/jmb/svn-managed/contur-dev/setupContur.sh;
cd /unix/atlas3/jmb/Work/Gambit/Scan2/myscan01/13TeV/0017;
Herwig read LHC.in -I /unix/atlas3/jmb/Work/Gambit/Scan2/GridPack -L /unix/atlas3/jmb/Work/Gambit/Scan2/GridPack;
Herwig run LHC.run --seed=101 --tag=runpoint_0017 --numevents=30000;
