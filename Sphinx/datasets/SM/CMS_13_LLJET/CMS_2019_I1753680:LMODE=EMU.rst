:orphan:

Standard Model Predictions for CMS_2019_I1753680:LMODE=EMU
==========================================================


 aMC@NLO :cite:`CMS:2019raw`: Generated from data taken from the paper
 Combined p-value for this prediction is 1.00E+00.


   :doc:`CMS_2019_I1753680:LMODE=EMU prediction A <CMS_2019_I1753680:LMODE=EMU/CMS_2019_I1753680:LMODE=EMU_A>`.
