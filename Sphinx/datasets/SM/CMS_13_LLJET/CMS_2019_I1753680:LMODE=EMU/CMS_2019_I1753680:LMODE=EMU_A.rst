:orphan:

Standard Model Predictions for CMS_2019_I1753680:LMODE=EMU
==========================================================

Generated from data taken from the paper (Prediction ID A)



Stored in file: CMS_2019_I1753680:LMODE=EMU-Theory.yoda 

aMC@NLO: d26-x01-y01

.. figure:: d26-x01-y01_A.png
           :scale: 80%

aMC@NLO: d26-x01-y02

.. figure:: d26-x01-y02_A.png
           :scale: 80%

aMC@NLO: d27-x01-y01

.. figure:: d27-x01-y01_A.png
           :scale: 80%

aMC@NLO: d27-x01-y02

.. figure:: d27-x01-y02_A.png
           :scale: 80%

aMC@NLO: d28-x01-y01

.. figure:: d28-x01-y01_A.png
           :scale: 80%

aMC@NLO: d28-x01-y02

.. figure:: d28-x01-y02_A.png
           :scale: 80%

