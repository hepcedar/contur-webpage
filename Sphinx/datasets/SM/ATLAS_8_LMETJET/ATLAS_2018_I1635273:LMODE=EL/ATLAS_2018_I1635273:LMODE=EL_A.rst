:orphan:

Standard Model Predictions for ATLAS_2018_I1635273:LMODE=EL
===========================================================

Generated from data taken from the paper (Prediction ID A)



Stored in file: ATLAS_2018_I1635273:LMODE=EL-Theory.yoda 

SHERPA 2.2.1 NLO: d01-x01-y01

.. figure:: d01-x01-y01_A.png
           :scale: 80%

SHERPA 2.2.1 NLO: d06-x01-y01

.. figure:: d06-x01-y01_A.png
           :scale: 80%

SHERPA 2.2.1 NLO: d11-x01-y01

.. figure:: d11-x01-y01_A.png
           :scale: 80%

SHERPA 2.2.1 NLO: d16-x01-y01

.. figure:: d16-x01-y01_A.png
           :scale: 80%

SHERPA 2.2.1 NLO: d21-x01-y01

.. figure:: d21-x01-y01_A.png
           :scale: 80%

SHERPA 2.2.1 NLO: d26-x01-y01

.. figure:: d26-x01-y01_A.png
           :scale: 80%

SHERPA 2.2.1 NLO: d28-x01-y01

.. figure:: d28-x01-y01_A.png
           :scale: 80%

SHERPA 2.2.1 NLO: d30-x01-y01

.. figure:: d30-x01-y01_A.png
           :scale: 80%

SHERPA 2.2.1 NLO: d32-x01-y01

.. figure:: d32-x01-y01_A.png
           :scale: 80%

