:orphan:

Standard Model Predictions for ATLAS_2018_I1635273:LMODE=EL
===========================================================


 SHERPA 2.2.1 NLO :cite:`ATLAS:2017irc`: Generated from data taken from the paper
 Combined p-value for this prediction is 9.96E-01.


   :doc:`ATLAS_2018_I1635273:LMODE=EL prediction A <ATLAS_2018_I1635273:LMODE=EL/ATLAS_2018_I1635273:LMODE=EL_A>`.
