:orphan:

Standard Model Predictions for ATLAS_2015_I1408516:LMODE=EL
===========================================================


 Bizon et al :cite:`Bizon:2018foh`: Digitised files from author. NNLO + N3LL: NNLOJET + RADISH.
 Combined p-value for this prediction is 9.94E-02.


   :doc:`ATLAS_2015_I1408516:LMODE=EL prediction A <ATLAS_2015_I1408516:LMODE=EL/ATLAS_2015_I1408516:LMODE=EL_A>`.
