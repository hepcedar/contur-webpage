:orphan:

Standard Model Predictions for ATLAS_2015_I1408516:LMODE=MU
===========================================================


 Bizon et al :cite:`Bizon:2018foh`: Digitised files from author. NNLO + N3LL: NNLOJET + RADISH.
 Combined p-value for this prediction is 5.93E-12.


   :doc:`ATLAS_2015_I1408516:LMODE=MU prediction A <ATLAS_2015_I1408516:LMODE=MU/ATLAS_2015_I1408516:LMODE=MU_A>`.
