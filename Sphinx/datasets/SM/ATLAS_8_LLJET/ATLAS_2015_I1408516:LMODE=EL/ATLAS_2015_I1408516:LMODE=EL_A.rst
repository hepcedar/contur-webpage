:orphan:

Standard Model Predictions for ATLAS_2015_I1408516:LMODE=EL
===========================================================

Digitised files from author. NNLO + N3LL: NNLOJET + RADISH. (Prediction ID A)



Stored in file: ATLAS_2015_I1408516:LMODE=EL-Theory.yoda 

Bizon et al: d02-x01-y01

.. figure:: d02-x01-y01_A.png
           :scale: 80%

Bizon et al: d03-x01-y01

.. figure:: d03-x01-y01_A.png
           :scale: 80%

Bizon et al: d04-x01-y01

.. figure:: d04-x01-y01_A.png
           :scale: 80%

Bizon et al: d05-x01-y01

.. figure:: d05-x01-y01_A.png
           :scale: 80%

Bizon et al: d06-x01-y01

.. figure:: d06-x01-y01_A.png
           :scale: 80%

Bizon et al: d07-x01-y01

.. figure:: d07-x01-y01_A.png
           :scale: 80%

Bizon et al: d08-x01-y01

.. figure:: d08-x01-y01_A.png
           :scale: 80%

Bizon et al: d09-x01-y01

.. figure:: d09-x01-y01_A.png
           :scale: 80%

Bizon et al: d10-x01-y01

.. figure:: d10-x01-y01_A.png
           :scale: 80%

Bizon et al: d11-x01-y01

.. figure:: d11-x01-y01_A.png
           :scale: 80%

Bizon et al: d12-x01-y01

.. figure:: d12-x01-y01_A.png
           :scale: 80%

Bizon et al: d13-x01-y01

.. figure:: d13-x01-y01_A.png
           :scale: 80%

Bizon et al: d14-x01-y01

.. figure:: d14-x01-y01_A.png
           :scale: 80%

Bizon et al: d15-x01-y01

.. figure:: d15-x01-y01_A.png
           :scale: 80%

Bizon et al: d16-x01-y01

.. figure:: d16-x01-y01_A.png
           :scale: 80%

Bizon et al: d17-x01-y01

.. figure:: d17-x01-y01_A.png
           :scale: 80%

Bizon et al: d18-x01-y01

.. figure:: d18-x01-y01_A.png
           :scale: 80%

Bizon et al: d19-x01-y01

.. figure:: d19-x01-y01_A.png
           :scale: 80%

Bizon et al: d20-x01-y01

.. figure:: d20-x01-y01_A.png
           :scale: 80%

Bizon et al: d21-x01-y01

.. figure:: d21-x01-y01_A.png
           :scale: 80%

Bizon et al: d22-x01-y01

.. figure:: d22-x01-y01_A.png
           :scale: 80%

Bizon et al: d26-x01-y01

.. figure:: d26-x01-y01_A.png
           :scale: 80%

Bizon et al: d27-x01-y01

.. figure:: d27-x01-y01_A.png
           :scale: 80%

Bizon et al: d28-x01-y01

.. figure:: d28-x01-y01_A.png
           :scale: 80%

