:orphan:

Standard Model Predictions for CMS_2017_I1518399
================================================


 PowhegBoxZpWp :cite:`Altakach:2020ugg,Altakach:2020azd,Altakach:2021lkq`: As used in Altakach et all arXiv:2111.15406
 Combined p-value for this prediction is 9.07E-01.


   :doc:`CMS_2017_I1518399 prediction A <CMS_2017_I1518399/CMS_2017_I1518399_A>`.
