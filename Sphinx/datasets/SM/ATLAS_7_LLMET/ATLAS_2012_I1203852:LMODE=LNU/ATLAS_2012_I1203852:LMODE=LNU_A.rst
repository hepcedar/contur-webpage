:orphan:

Standard Model Predictions for ATLAS_2012_I1203852:LMODE=LNU
============================================================

From measurement paper. See additional references therein. (Prediction ID A)



Stored in file: ATLAS_2012_I1203852:LMODE=LNU-Theory.yoda 

PowhegBox+gg2zz: d01-x01-y03

.. figure:: d01-x01-y03_A.png
           :scale: 80%

