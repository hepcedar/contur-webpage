:orphan:

Standard Model Predictions for ATLAS_2012_I1203852:LMODE=LNU
============================================================


 PowhegBox+gg2zz :cite:`Alioli:2010xd,Melia:2011tj,Binoth:2008pr`: From measurement paper. See additional references therein.
 Combined p-value for this prediction is 9.57E-01.


   :doc:`ATLAS_2012_I1203852:LMODE=LNU prediction A <ATLAS_2012_I1203852:LMODE=LNU/ATLAS_2012_I1203852:LMODE=LNU_A>`.
