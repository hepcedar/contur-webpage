:orphan:

Standard Model Predictions for ATLAS_2016_I1448301:LMODE=NU
===========================================================

MCFM NLO calculations, from measurement paper (Prediction ID A)



Stored in file: ATLAS_2016_I1448301:LMODE=NU-Theory.yoda 

MCFM: d02-x01-y01

.. figure:: d02-x01-y01_A.png
           :scale: 80%

MCFM: d04-x01-y01

.. figure:: d04-x01-y01_A.png
           :scale: 80%

