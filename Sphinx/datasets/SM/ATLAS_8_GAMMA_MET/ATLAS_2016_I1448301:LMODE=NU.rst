:orphan:

Standard Model Predictions for ATLAS_2016_I1448301:LMODE=NU
===========================================================


 MCFM :cite:`Campbell:2011bn`: MCFM NLO calculations, from measurement paper
 Combined p-value for this prediction is 5.33E-01.


   :doc:`ATLAS_2016_I1448301:LMODE=NU prediction A <ATLAS_2016_I1448301:LMODE=NU/ATLAS_2016_I1448301:LMODE=NU_A>`.
