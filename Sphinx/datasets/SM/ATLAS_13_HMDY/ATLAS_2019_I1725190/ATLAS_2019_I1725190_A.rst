:orphan:

Standard Model Predictions for ATLAS_2019_I1725190
==================================================

The \"Theory prediction\" is the background fit to data from the measurement paper. (Prediction ID A)



Stored in file: ATLAS_2019_I1725190-Theory.yoda 

fit to data: d01-x01-y01

.. figure:: d01-x01-y01_A.png
           :scale: 80%

fit to data: d02-x01-y01

.. figure:: d02-x01-y01_A.png
           :scale: 80%

