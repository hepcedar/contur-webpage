:orphan:

Standard Model Predictions for ATLAS_2018_I1698006:LVETO=ON
===========================================================

Generated from data taken from the paper (Prediction ID A)



Stored in file: ATLAS_2018_I1698006:LVETO=ON-Theory.yoda 

NNLO MCFM: d02-x01-y01

.. figure:: d02-x01-y01_A.png
           :scale: 80%

NNLO MCFM: d03-x01-y01

.. figure:: d03-x01-y01_A.png
           :scale: 80%

NNLO MCFM: d04-x01-y01

.. figure:: d04-x01-y01_A.png
           :scale: 80%

NNLO MCFM: d05-x01-y01

.. figure:: d05-x01-y01_A.png
           :scale: 80%

