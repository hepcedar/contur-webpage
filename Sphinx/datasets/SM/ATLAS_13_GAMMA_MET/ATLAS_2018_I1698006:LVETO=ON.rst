:orphan:

Standard Model Predictions for ATLAS_2018_I1698006:LVETO=ON
===========================================================


 NNLO MCFM :cite:`ATLAS:2018nci`: Generated from data taken from the paper
 Combined p-value for this prediction is 1.00E+00.


   :doc:`ATLAS_2018_I1698006:LVETO=ON prediction A <ATLAS_2018_I1698006:LVETO=ON/ATLAS_2018_I1698006:LVETO=ON_A>`.
