:orphan:

Standard Model Predictions for ATLAS_2016_I1448301:LMODE=LL
===========================================================


 NNLO (MMHT2014) :cite:`ATLAS:2016qjc`: Generated from data taken from the paper
 Combined p-value for this prediction is 9.28E-01.


   :doc:`ATLAS_2016_I1448301:LMODE=LL prediction A <ATLAS_2016_I1448301:LMODE=LL/ATLAS_2016_I1448301:LMODE=LL_A>`.
