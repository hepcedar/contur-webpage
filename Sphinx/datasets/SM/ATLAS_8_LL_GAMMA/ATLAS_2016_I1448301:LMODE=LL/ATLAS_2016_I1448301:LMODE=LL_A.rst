:orphan:

Standard Model Predictions for ATLAS_2016_I1448301:LMODE=LL
===========================================================

Generated from data taken from the paper (Prediction ID A)



Stored in file: ATLAS_2016_I1448301:LMODE=LL-Theory.yoda 

NNLO (MMHT2014): d05-x01-y01

.. figure:: d05-x01-y01_A.png
           :scale: 80%

NNLO (MMHT2014): d06-x01-y01

.. figure:: d06-x01-y01_A.png
           :scale: 80%

NNLO (MMHT2014): d09-x01-y01

.. figure:: d09-x01-y01_A.png
           :scale: 80%

NNLO (MMHT2014): d10-x01-y01

.. figure:: d10-x01-y01_A.png
           :scale: 80%

