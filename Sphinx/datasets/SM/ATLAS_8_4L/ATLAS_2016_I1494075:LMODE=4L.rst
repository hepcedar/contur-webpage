:orphan:

Standard Model Predictions for ATLAS_2016_I1494075:LMODE=4L
===========================================================


 POWHEG + Pythia :cite:`ATLAS:2016bxw`: Generated from ATLAS setup, with scale and stat uncertainties
 Combined p-value for this prediction is 8.93E-02.


   :doc:`ATLAS_2016_I1494075:LMODE=4L prediction A <ATLAS_2016_I1494075:LMODE=4L/ATLAS_2016_I1494075:LMODE=4L_A>`.
