:orphan:

Standard Model Predictions for ATLAS_2016_I1494075:LMODE=4L
===========================================================

Generated from ATLAS setup, with scale and stat uncertainties (Prediction ID A)



Stored in file: ATLAS_2016_I1494075:LMODE=4L-Theory.yoda 

POWHEG + Pythia: d02-x01-y01

.. figure:: d02-x01-y01_A.png
           :scale: 80%

POWHEG + Pythia: d03-x01-y01

.. figure:: d03-x01-y01_A.png
           :scale: 80%

POWHEG + Pythia: d04-x01-y01

.. figure:: d04-x01-y01_A.png
           :scale: 80%

POWHEG + Pythia: d05-x01-y01

.. figure:: d05-x01-y01_A.png
           :scale: 80%

