:orphan:

Standard Model Predictions for ATLAS_2016_I1494075:LMODE=2L2NU
==============================================================


 POWHEG + Pythia :cite:`ATLAS:2016bxw`: Generated from ATLAS setup, with scale and stat uncertainties
 Combined p-value for this prediction is 4.43E-01.


   :doc:`ATLAS_2016_I1494075:LMODE=2L2NU prediction A <ATLAS_2016_I1494075:LMODE=2L2NU/ATLAS_2016_I1494075:LMODE=2L2NU_A>`.
