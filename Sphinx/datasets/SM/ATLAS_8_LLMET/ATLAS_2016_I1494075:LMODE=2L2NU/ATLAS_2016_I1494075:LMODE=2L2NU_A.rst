:orphan:

Standard Model Predictions for ATLAS_2016_I1494075:LMODE=2L2NU
==============================================================

Generated from ATLAS setup, with scale and stat uncertainties (Prediction ID A)



Stored in file: ATLAS_2016_I1494075:LMODE=2L2NU-Theory.yoda 

POWHEG + Pythia: d06-x01-y01

.. figure:: d06-x01-y01_A.png
           :scale: 80%

POWHEG + Pythia: d07-x01-y01

.. figure:: d07-x01-y01_A.png
           :scale: 80%

POWHEG + Pythia: d08-x01-y01

.. figure:: d08-x01-y01_A.png
           :scale: 80%

