:orphan:

Standard Model Predictions for ATLAS_2024_I2768921:LMODE=DILEPTON
=================================================================

Central value from Hepdata (https://doi.org/10.17182/hepdata.146899.v1) Uncertainties from difference to prediction B (Prediction ID A)



Stored in file: ATLAS_2024_I2768921:LMODE=DILEPTON-Theory_A.yoda 

MG5_aMC+H7: d12-x01-y01

.. figure:: d12-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d14-x01-y01

.. figure:: d14-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d16-x01-y01

.. figure:: d16-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d34-x01-y01

.. figure:: d34-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d36-x01-y01

.. figure:: d36-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d38-x01-y01

.. figure:: d38-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d40-x01-y01

.. figure:: d40-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d42-x01-y01

.. figure:: d42-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d50-x01-y01

.. figure:: d50-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d52-x01-y01

.. figure:: d52-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d54-x01-y01

.. figure:: d54-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d56-x01-y01

.. figure:: d56-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d58-x01-y01

.. figure:: d58-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d60-x01-y01

.. figure:: d60-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d62-x01-y01

.. figure:: d62-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d64-x01-y01

.. figure:: d64-x01-y01_A.png
           :scale: 80%

MG5_aMC+H7: d66-x01-y01

.. figure:: d66-x01-y01_A.png
           :scale: 80%

