:orphan:

Standard Model Predictions for ATLAS_2018_I1707015:LMODE=DILEPTON
=================================================================


 MG5_aMC + Pythia8 :cite:`ATLAS:2018sos`: Generated from data taken from the paper
 Combined p-value for this prediction is 9.60E-01.


   :doc:`ATLAS_2018_I1707015:LMODE=DILEPTON prediction A <ATLAS_2018_I1707015:LMODE=DILEPTON/ATLAS_2018_I1707015:LMODE=DILEPTON_A>`.
