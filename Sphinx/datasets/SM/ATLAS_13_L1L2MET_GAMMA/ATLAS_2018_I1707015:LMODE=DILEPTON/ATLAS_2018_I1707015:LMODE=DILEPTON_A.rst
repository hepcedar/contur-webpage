:orphan:

Standard Model Predictions for ATLAS_2018_I1707015:LMODE=DILEPTON
=================================================================

Generated from data taken from the paper (Prediction ID A)



Stored in file: ATLAS_2018_I1707015:LMODE=DILEPTON-Theory.yoda 

MG5_aMC + Pythia8: d06-x01-y01

.. figure:: d06-x01-y01_A.png
           :scale: 80%

MG5_aMC + Pythia8: d07-x01-y01

.. figure:: d07-x01-y01_A.png
           :scale: 80%

MG5_aMC + Pythia8: d08-x01-y01

.. figure:: d08-x01-y01_A.png
           :scale: 80%

MG5_aMC + Pythia8: d09-x01-y01

.. figure:: d09-x01-y01_A.png
           :scale: 80%

MG5_aMC + Pythia8: d10-x01-y01

.. figure:: d10-x01-y01_A.png
           :scale: 80%

