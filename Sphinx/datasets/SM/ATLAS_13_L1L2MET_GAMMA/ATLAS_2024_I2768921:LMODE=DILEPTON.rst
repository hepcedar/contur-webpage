:orphan:

Standard Model Predictions for ATLAS_2024_I2768921:LMODE=DILEPTON
=================================================================


 MG5_aMC+H7 :cite:`ATLAS:2024hmk`: Central value from Hepdata (https://doi.org/10.17182/hepdata.146899.v1) Uncertainties from difference to prediction B
 Combined p-value for this prediction is 1.26E-01.


   :doc:`ATLAS_2024_I2768921:LMODE=DILEPTON prediction A <ATLAS_2024_I2768921:LMODE=DILEPTON/ATLAS_2024_I2768921:LMODE=DILEPTON_A>`.

 MG5_aMC+P8 :cite:`ATLAS:2024hmk`: Central value from Hepdata (https://doi.org/10.17182/hepdata.146899.v1) Uncertainties from difference to prediction A
 Combined p-value for this prediction is 2.72E-02.


   :doc:`ATLAS_2024_I2768921:LMODE=DILEPTON prediction B <ATLAS_2024_I2768921:LMODE=DILEPTON/ATLAS_2024_I2768921:LMODE=DILEPTON_B>`.
