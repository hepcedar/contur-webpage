:orphan:

Standard Model Predictions for ATLAS_2019_I1718132:LMODE=MUMU
=============================================================

Generated from the mean of data taken from the paper (Prediction ID A)



Stored in file: ATLAS_2019_I1718132:LMODE=MUMU-Theory.yoda 

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d03-x01-y01

.. figure:: d03-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d06-x01-y01

.. figure:: d06-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d09-x01-y01

.. figure:: d09-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d12-x01-y01

.. figure:: d12-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d15-x01-y01

.. figure:: d15-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d21-x01-y01

.. figure:: d21-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d24-x01-y01

.. figure:: d24-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d27-x01-y01

.. figure:: d27-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d33-x01-y01

.. figure:: d33-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d36-x01-y01

.. figure:: d36-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d39-x01-y01

.. figure:: d39-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d42-x01-y01

.. figure:: d42-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d45-x01-y01

.. figure:: d45-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d48-x01-y01

.. figure:: d48-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d51-x01-y01

.. figure:: d51-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d54-x01-y01

.. figure:: d54-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d57-x01-y01

.. figure:: d57-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d60-x01-y01

.. figure:: d60-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d63-x01-y01

.. figure:: d63-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d66-x01-y01

.. figure:: d66-x01-y01_A.png
           :scale: 80%

