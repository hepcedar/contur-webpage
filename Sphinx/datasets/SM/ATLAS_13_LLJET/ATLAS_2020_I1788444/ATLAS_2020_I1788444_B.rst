:orphan:

Standard Model Predictions for ATLAS_2020_I1788444
==================================================

Taken from HEPData record https://doi.org/10.17182/hepdata.94219 (Prediction ID B)



Stored in file: ATLAS_2020_I1788444-Theory_B.yoda 

MGaMC+Py8 Zbb 4FNS (NLO): d01-x01-y01

.. figure:: d01-x01-y01_B.png
           :scale: 80%

MGaMC+Py8 Zbb 4FNS (NLO): d02-x01-y01

.. figure:: d02-x01-y01_B.png
           :scale: 80%

MGaMC+Py8 Zbb 4FNS (NLO): d03-x01-y01

.. figure:: d03-x01-y01_B.png
           :scale: 80%

MGaMC+Py8 Zbb 4FNS (NLO): d04-x01-y01

.. figure:: d04-x01-y01_B.png
           :scale: 80%

MGaMC+Py8 Zbb 4FNS (NLO): d05-x01-y01

.. figure:: d05-x01-y01_B.png
           :scale: 80%

MGaMC+Py8 Zbb 4FNS (NLO): d06-x01-y01

.. figure:: d06-x01-y01_B.png
           :scale: 80%

MGaMC+Py8 Zbb 4FNS (NLO): d07-x01-y01

.. figure:: d07-x01-y01_B.png
           :scale: 80%

MGaMC+Py8 Zbb 4FNS (NLO): d08-x01-y01

.. figure:: d08-x01-y01_B.png
           :scale: 80%

MGaMC+Py8 Zbb 4FNS (NLO): d09-x01-y01

.. figure:: d09-x01-y01_B.png
           :scale: 80%

MGaMC+Py8 Zbb 4FNS (NLO): d10-x01-y01

.. figure:: d10-x01-y01_B.png
           :scale: 80%

MGaMC+Py8 Zbb 4FNS (NLO): d11-x01-y01

.. figure:: d11-x01-y01_B.png
           :scale: 80%

MGaMC+Py8 Zbb 4FNS (NLO): d12-x01-y01

.. figure:: d12-x01-y01_B.png
           :scale: 80%

MGaMC+Py8 Zbb 4FNS (NLO): d13-x01-y01

.. figure:: d13-x01-y01_B.png
           :scale: 80%

MGaMC+Py8 Zbb 4FNS (NLO): d14-x01-y01

.. figure:: d14-x01-y01_B.png
           :scale: 80%

