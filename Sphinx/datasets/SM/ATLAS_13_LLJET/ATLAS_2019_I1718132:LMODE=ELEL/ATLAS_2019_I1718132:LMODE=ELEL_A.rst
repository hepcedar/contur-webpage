:orphan:

Standard Model Predictions for ATLAS_2019_I1718132:LMODE=ELEL
=============================================================

Generated from the mean of data taken from the paper (Prediction ID A)



Stored in file: ATLAS_2019_I1718132:LMODE=ELEL-Theory.yoda 

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d02-x01-y01

.. figure:: d02-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d05-x01-y01

.. figure:: d05-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d08-x01-y01

.. figure:: d08-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d11-x01-y01

.. figure:: d11-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d14-x01-y01

.. figure:: d14-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d17-x01-y01

.. figure:: d17-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d20-x01-y01

.. figure:: d20-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d23-x01-y01

.. figure:: d23-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d26-x01-y01

.. figure:: d26-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d29-x01-y01

.. figure:: d29-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d32-x01-y01

.. figure:: d32-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d35-x01-y01

.. figure:: d35-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d38-x01-y01

.. figure:: d38-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d41-x01-y01

.. figure:: d41-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d44-x01-y01

.. figure:: d44-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d50-x01-y01

.. figure:: d50-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d53-x01-y01

.. figure:: d53-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d59-x01-y01

.. figure:: d59-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Sherpa2.1.1: d62-x01-y01

.. figure:: d62-x01-y01_A.png
           :scale: 80%

