:orphan:

Standard Model Predictions for ATLAS_2019_I1718132:LMODE=ELEL
=============================================================


 MADGRAPH5_aMC+Pythia and Sherpa2.1.1 :cite:`ATLAS:2019ebv`: Generated from the mean of data taken from the paper
 Combined p-value for this prediction is 7.17E-01.


   :doc:`ATLAS_2019_I1718132:LMODE=ELEL prediction A <ATLAS_2019_I1718132:LMODE=ELEL/ATLAS_2019_I1718132:LMODE=ELEL_A>`.
