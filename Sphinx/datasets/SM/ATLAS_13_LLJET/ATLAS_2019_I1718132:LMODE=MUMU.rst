:orphan:

Standard Model Predictions for ATLAS_2019_I1718132:LMODE=MUMU
=============================================================


 MADGRAPH5_aMC+Pythia and Sherpa2.1.1 :cite:`ATLAS:2019ebv`: Generated from the mean of data taken from the paper
 Combined p-value for this prediction is 1.00E+00.


   :doc:`ATLAS_2019_I1718132:LMODE=MUMU prediction A <ATLAS_2019_I1718132:LMODE=MUMU/ATLAS_2019_I1718132:LMODE=MUMU_A>`.
