:orphan:

Standard Model Predictions for ATLAS_2018_I1707015:LMODE=SINGLE
===============================================================


 MG5_aMC + Pythia8 :cite:`ATLAS:2018sos`: Generated from data taken from the paper
 Combined p-value for this prediction is 1.00E+00.


   :doc:`ATLAS_2018_I1707015:LMODE=SINGLE prediction A <ATLAS_2018_I1707015:LMODE=SINGLE/ATLAS_2018_I1707015:LMODE=SINGLE_A>`.
