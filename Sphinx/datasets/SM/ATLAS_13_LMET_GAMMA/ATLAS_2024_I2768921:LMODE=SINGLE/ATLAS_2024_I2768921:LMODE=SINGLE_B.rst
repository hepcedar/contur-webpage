:orphan:

Standard Model Predictions for ATLAS_2024_I2768921:LMODE=SINGLE
===============================================================

Central value from Hepdata (https://doi.org/10.17182/hepdata.146899.v1) Uncertainties from difference to prediction A (Prediction ID B)



Stored in file: ATLAS_2024_I2768921:LMODE=SINGLE-Theory_B.yoda 

MG5_aMC+P8: d06-x01-y01

.. figure:: d06-x01-y01_B.png
           :scale: 80%

MG5_aMC+P8: d08-x01-y01

.. figure:: d08-x01-y01_B.png
           :scale: 80%

MG5_aMC+P8: d10-x01-y01

.. figure:: d10-x01-y01_B.png
           :scale: 80%

MG5_aMC+P8: d22-x01-y01

.. figure:: d22-x01-y01_B.png
           :scale: 80%

MG5_aMC+P8: d24-x01-y01

.. figure:: d24-x01-y01_B.png
           :scale: 80%

MG5_aMC+P8: d26-x01-y01

.. figure:: d26-x01-y01_B.png
           :scale: 80%

MG5_aMC+P8: d28-x01-y01

.. figure:: d28-x01-y01_B.png
           :scale: 80%

MG5_aMC+P8: d30-x01-y01

.. figure:: d30-x01-y01_B.png
           :scale: 80%

MG5_aMC+P8: d32-x01-y01

.. figure:: d32-x01-y01_B.png
           :scale: 80%

MG5_aMC+P8: d44-x01-y01

.. figure:: d44-x01-y01_B.png
           :scale: 80%

MG5_aMC+P8: d46-x01-y01

.. figure:: d46-x01-y01_B.png
           :scale: 80%

MG5_aMC+P8: d48-x01-y01

.. figure:: d48-x01-y01_B.png
           :scale: 80%

