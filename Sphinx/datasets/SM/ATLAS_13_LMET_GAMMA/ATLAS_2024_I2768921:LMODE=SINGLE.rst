:orphan:

Standard Model Predictions for ATLAS_2024_I2768921:LMODE=SINGLE
===============================================================


 MG5_aMC+H7 :cite:`ATLAS:2024hmk`: Central value from Hepdata (https://doi.org/10.17182/hepdata.146899.v1) Uncertainties from difference to prediction B
 Combined p-value for this prediction is 9.51E-04.


   :doc:`ATLAS_2024_I2768921:LMODE=SINGLE prediction A <ATLAS_2024_I2768921:LMODE=SINGLE/ATLAS_2024_I2768921:LMODE=SINGLE_A>`.

 MG5_aMC+P8 :cite:`ATLAS:2024hmk`: Central value from Hepdata (https://doi.org/10.17182/hepdata.146899.v1) Uncertainties from difference to prediction A
 Combined p-value for this prediction is 4.13E-07.


   :doc:`ATLAS_2024_I2768921:LMODE=SINGLE prediction B <ATLAS_2024_I2768921:LMODE=SINGLE/ATLAS_2024_I2768921:LMODE=SINGLE_B>`.
