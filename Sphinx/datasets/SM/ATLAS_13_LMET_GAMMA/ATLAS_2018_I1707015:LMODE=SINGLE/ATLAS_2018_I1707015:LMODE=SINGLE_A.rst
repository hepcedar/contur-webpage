:orphan:

Standard Model Predictions for ATLAS_2018_I1707015:LMODE=SINGLE
===============================================================

Generated from data taken from the paper (Prediction ID A)



Stored in file: ATLAS_2018_I1707015:LMODE=SINGLE-Theory.yoda 

MG5_aMC + Pythia8: d03-x01-y01

.. figure:: d03-x01-y01_A.png
           :scale: 80%

MG5_aMC + Pythia8: d04-x01-y01

.. figure:: d04-x01-y01_A.png
           :scale: 80%

MG5_aMC + Pythia8: d05-x01-y01

.. figure:: d05-x01-y01_A.png
           :scale: 80%

