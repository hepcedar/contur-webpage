:orphan:

Standard Model Predictions for ATLAS_2022_I2077575
==================================================

Plots digitised from experimental paper. Uncertainties taken as the envelope of theory predictions. (Prediction ID A)



Stored in file: ATLAS_2022_I2077575-Theory.yoda 

POWHEG+Pythia8: d02-x01-y01

.. figure:: d02-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d100-x01-y01

.. figure:: d100-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d101-x01-y01

.. figure:: d101-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d102-x01-y01

.. figure:: d102-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d103-x01-y01

.. figure:: d103-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d104-x01-y01

.. figure:: d104-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d105-x01-y01

.. figure:: d105-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d106-x01-y01

.. figure:: d106-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d107-x01-y01

.. figure:: d107-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d108-x01-y01

.. figure:: d108-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d109-x01-y01

.. figure:: d109-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d110-x01-y01

.. figure:: d110-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d111-x01-y01

.. figure:: d111-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d112-x01-y01

.. figure:: d112-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d113-x01-y01

.. figure:: d113-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d114-x01-y01

.. figure:: d114-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d115-x01-y01

.. figure:: d115-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d116-x01-y01

.. figure:: d116-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d117-x01-y01

.. figure:: d117-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d118-x01-y01

.. figure:: d118-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d119-x01-y01

.. figure:: d119-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d120-x01-y01

.. figure:: d120-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d121-x01-y01

.. figure:: d121-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d122-x01-y01

.. figure:: d122-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d123-x01-y01

.. figure:: d123-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d124-x01-y01

.. figure:: d124-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d125-x01-y01

.. figure:: d125-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d126-x01-y01

.. figure:: d126-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d127-x01-y01

.. figure:: d127-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d128-x01-y01

.. figure:: d128-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d129-x01-y01

.. figure:: d129-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d130-x01-y01

.. figure:: d130-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d131-x01-y01

.. figure:: d131-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d132-x01-y01

.. figure:: d132-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d133-x01-y01

.. figure:: d133-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d134-x01-y01

.. figure:: d134-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d135-x01-y01

.. figure:: d135-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d136-x01-y01

.. figure:: d136-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d137-x01-y01

.. figure:: d137-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d138-x01-y01

.. figure:: d138-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d139-x01-y01

.. figure:: d139-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d140-x01-y01

.. figure:: d140-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d141-x01-y01

.. figure:: d141-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d142-x01-y01

.. figure:: d142-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d143-x01-y01

.. figure:: d143-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d144-x01-y01

.. figure:: d144-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d145-x01-y01

.. figure:: d145-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d146-x01-y01

.. figure:: d146-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d75-x01-y01

.. figure:: d75-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d76-x01-y01

.. figure:: d76-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d77-x01-y01

.. figure:: d77-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d78-x01-y01

.. figure:: d78-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d79-x01-y01

.. figure:: d79-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d80-x01-y01

.. figure:: d80-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d81-x01-y01

.. figure:: d81-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d82-x01-y01

.. figure:: d82-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d83-x01-y01

.. figure:: d83-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d84-x01-y01

.. figure:: d84-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d85-x01-y01

.. figure:: d85-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d86-x01-y01

.. figure:: d86-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d87-x01-y01

.. figure:: d87-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d88-x01-y01

.. figure:: d88-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d89-x01-y01

.. figure:: d89-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d90-x01-y01

.. figure:: d90-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d91-x01-y01

.. figure:: d91-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d92-x01-y01

.. figure:: d92-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d93-x01-y01

.. figure:: d93-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d94-x01-y01

.. figure:: d94-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d95-x01-y01

.. figure:: d95-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d96-x01-y01

.. figure:: d96-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d97-x01-y01

.. figure:: d97-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d98-x01-y01

.. figure:: d98-x01-y01_A.png
           :scale: 80%

POWHEG+Pythia8: d99-x01-y01

.. figure:: d99-x01-y01_A.png
           :scale: 80%

