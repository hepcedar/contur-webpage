:orphan:

Standard Model Predictions for CMS_2018_I1662081
================================================

As used in Altakach et all arXiv:2111.15406 (Prediction ID A)



Stored in file: CMS_2018_I1662081-Theory.yoda 

PowhegBoxZpWp: d08-x01-y01

.. figure:: d08-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d09-x01-y01

.. figure:: d09-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d10-x01-y01

.. figure:: d10-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d11-x01-y01

.. figure:: d11-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d12-x01-y01

.. figure:: d12-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d13-x01-y01

.. figure:: d13-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d14-x01-y01

.. figure:: d14-x01-y01_A.png
           :scale: 80%

