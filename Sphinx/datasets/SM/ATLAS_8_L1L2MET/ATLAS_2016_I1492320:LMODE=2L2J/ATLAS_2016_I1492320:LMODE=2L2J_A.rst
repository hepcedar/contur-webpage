:orphan:

Standard Model Predictions for ATLAS_2016_I1492320:LMODE=2L2J
=============================================================

Taken from measurement paper (Prediction ID A)



Stored in file: ATLAS_2016_I1492320:LMODE=2L2J-Theory.yoda 

MadGraph: d01-x01-y02

.. figure:: d01-x01-y02_A.png
           :scale: 80%

