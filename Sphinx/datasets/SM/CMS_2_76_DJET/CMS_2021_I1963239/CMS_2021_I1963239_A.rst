:orphan:

Standard Model Predictions for CMS_2021_I1963239
================================================

Taken from the HEPData record of the experimental paper (Prediction ID A)



Stored in file: CMS_2021_I1963239-Theory.yoda 

Pythia8 4C: d07-x01-y01

.. figure:: d07-x01-y01_A.png
           :scale: 80%

Pythia8 4C: d08-x01-y01

.. figure:: d08-x01-y01_A.png
           :scale: 80%

Pythia8 4C: d09-x01-y01

.. figure:: d09-x01-y01_A.png
           :scale: 80%

Pythia8 4C: d10-x01-y01

.. figure:: d10-x01-y01_A.png
           :scale: 80%

Pythia8 4C: d11-x01-y01

.. figure:: d11-x01-y01_A.png
           :scale: 80%

Pythia8 4C: d12-x01-y01

.. figure:: d12-x01-y01_A.png
           :scale: 80%

Pythia8 4C: d19-x01-y01

.. figure:: d19-x01-y01_A.png
           :scale: 80%

Pythia8 4C: d20-x01-y01

.. figure:: d20-x01-y01_A.png
           :scale: 80%

Pythia8 4C: d21-x01-y01

.. figure:: d21-x01-y01_A.png
           :scale: 80%

Pythia8 4C: d22-x01-y01

.. figure:: d22-x01-y01_A.png
           :scale: 80%

Pythia8 4C: d23-x01-y01

.. figure:: d23-x01-y01_A.png
           :scale: 80%

Pythia8 4C: d24-x01-y01

.. figure:: d24-x01-y01_A.png
           :scale: 80%

