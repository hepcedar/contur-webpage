:orphan:

Standard Model Predictions for ATLAS_2021_I1887997
==================================================


 NNLOJet :cite:`ATLAS:2021mbt`: Taken from the HEPData record of the experimental paper doi.org/10.17182/hepdata.104925
 Combined p-value for this prediction is 1.00E+00.


   :doc:`ATLAS_2021_I1887997 prediction A <ATLAS_2021_I1887997/ATLAS_2021_I1887997_A>`.
