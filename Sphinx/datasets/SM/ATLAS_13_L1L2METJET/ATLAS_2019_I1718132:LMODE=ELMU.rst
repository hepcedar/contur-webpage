:orphan:

Standard Model Predictions for ATLAS_2019_I1718132:LMODE=ELMU
=============================================================


 MADGRAPH5_aMC+Pythia and Powheg :cite:`ATLAS:2019ebv`: Generated from the mean of data taken from the paper
 Combined p-value for this prediction is 1.00E+00.


   :doc:`ATLAS_2019_I1718132:LMODE=ELMU prediction A <ATLAS_2019_I1718132:LMODE=ELMU/ATLAS_2019_I1718132:LMODE=ELMU_A>`.
