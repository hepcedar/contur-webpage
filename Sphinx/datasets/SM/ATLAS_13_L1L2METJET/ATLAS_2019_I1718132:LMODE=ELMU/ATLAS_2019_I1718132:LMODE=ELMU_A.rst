:orphan:

Standard Model Predictions for ATLAS_2019_I1718132:LMODE=ELMU
=============================================================

Generated from the mean of data taken from the paper (Prediction ID A)



Stored in file: ATLAS_2019_I1718132:LMODE=ELMU-Theory.yoda 

MADGRAPH5_aMC+Pythia and Powheg: d04-x01-y01

.. figure:: d04-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d07-x01-y01

.. figure:: d07-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d10-x01-y01

.. figure:: d10-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d13-x01-y01

.. figure:: d13-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d16-x01-y01

.. figure:: d16-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d19-x01-y01

.. figure:: d19-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d22-x01-y01

.. figure:: d22-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d25-x01-y01

.. figure:: d25-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d28-x01-y01

.. figure:: d28-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d31-x01-y01

.. figure:: d31-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d34-x01-y01

.. figure:: d34-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d37-x01-y01

.. figure:: d37-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d40-x01-y01

.. figure:: d40-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d43-x01-y01

.. figure:: d43-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d46-x01-y01

.. figure:: d46-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d49-x01-y01

.. figure:: d49-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d52-x01-y01

.. figure:: d52-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d55-x01-y01

.. figure:: d55-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d58-x01-y01

.. figure:: d58-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d61-x01-y01

.. figure:: d61-x01-y01_A.png
           :scale: 80%

MADGRAPH5_aMC+Pythia and Powheg: d67-x01-y01

.. figure:: d67-x01-y01_A.png
           :scale: 80%

