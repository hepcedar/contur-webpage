:orphan:

Standard Model Predictions for ATLAS_2021_I1852328
==================================================


 MATRIX nNNLO x NLO EW :cite:`Sherpa:2019gpd,ATLAS:2021jgw`: Rescaled to b-veto measurement using data. See measurement paper for full details. HEPData record at https://doi.org/10.17182/hepdata.100511.v1
 Combined p-value for this prediction is 1.00E+00.


   :doc:`ATLAS_2021_I1852328 prediction A <ATLAS_2021_I1852328/ATLAS_2021_I1852328_A>`.
