:orphan:

Standard Model Predictions for ATLAS_2016_I1492320:LMODE=3L
===========================================================

Taken from measurement paper (Prediction ID A)



Stored in file: ATLAS_2016_I1492320:LMODE=3L-Theory.yoda 

MadGraph: d01-x01-y01

.. figure:: d01-x01-y01_A.png
           :scale: 80%

