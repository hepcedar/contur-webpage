:orphan:

Standard Model Predictions for ATLAS_2012_I1203852:LMODE=LL
===========================================================

From measurement paper. See additional references therein. (Prediction ID A)



Stored in file: ATLAS_2012_I1203852:LMODE=LL-Theory.yoda 

PowhegBox+gg2zz: d01-x01-y01

.. figure:: d01-x01-y01_A.png
           :scale: 80%

PowhegBox+gg2zz: d01-x01-y02

.. figure:: d01-x01-y02_A.png
           :scale: 80%

