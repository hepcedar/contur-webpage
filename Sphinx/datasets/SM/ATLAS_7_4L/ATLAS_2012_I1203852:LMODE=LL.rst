:orphan:

Standard Model Predictions for ATLAS_2012_I1203852:LMODE=LL
===========================================================


 PowhegBox+gg2zz :cite:`Alioli:2010xd,Melia:2011tj,Binoth:2008pr`: From measurement paper. See additional references therein.
 Combined p-value for this prediction is 3.24E-01.


   :doc:`ATLAS_2012_I1203852:LMODE=LL prediction A <ATLAS_2012_I1203852:LMODE=LL/ATLAS_2012_I1203852:LMODE=LL_A>`.
