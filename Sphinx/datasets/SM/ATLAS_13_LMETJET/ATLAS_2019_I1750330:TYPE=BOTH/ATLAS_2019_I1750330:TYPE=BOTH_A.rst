:orphan:

Standard Model Predictions for ATLAS_2019_I1750330:TYPE=BOTH
============================================================

As used in Altakach et all arXiv:2111.15406 (Prediction ID A)



Stored in file: ATLAS_2019_I1750330:TYPE=BOTH-Theory.yoda 

PowhegBoxZpWp: d04-x01-y01

.. figure:: d04-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d08-x01-y01

.. figure:: d08-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d12-x01-y01

.. figure:: d12-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d16-x01-y01

.. figure:: d16-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d20-x01-y01

.. figure:: d20-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d24-x01-y01

.. figure:: d24-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d28-x01-y01

.. figure:: d28-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d32-x01-y01

.. figure:: d32-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d36-x01-y01

.. figure:: d36-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d40-x01-y01

.. figure:: d40-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d44-x01-y01

.. figure:: d44-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d48-x01-y01

.. figure:: d48-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d52-x01-y01

.. figure:: d52-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d843-x01-y01

.. figure:: d843-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d847-x01-y01

.. figure:: d847-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d851-x01-y01

.. figure:: d851-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d855-x01-y01

.. figure:: d855-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d859-x01-y01

.. figure:: d859-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d863-x01-y01

.. figure:: d863-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d867-x01-y01

.. figure:: d867-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d871-x01-y01

.. figure:: d871-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d875-x01-y01

.. figure:: d875-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d879-x01-y01

.. figure:: d879-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d883-x01-y01

.. figure:: d883-x01-y01_A.png
           :scale: 80%

PowhegBoxZpWp: d887-x01-y01

.. figure:: d887-x01-y01_A.png
           :scale: 80%

