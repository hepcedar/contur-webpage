:orphan:

Standard Model Predictions for ATLAS_2022_I2037744
==================================================

Central values taken from HEPData record https://www.hepdata.net/record/134011?version=2, uncertainties generated from paper (Prediction ID A)



Stored in file: ATLAS_2022_I2037744-Theory.yoda 

Powheg + Pythia 8: d03-x01-y01

.. figure:: d03-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d06-x01-y01

.. figure:: d06-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d09-x01-y01

.. figure:: d09-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d12-x01-y01

.. figure:: d12-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d15-x01-y01

.. figure:: d15-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d18-x01-y01

.. figure:: d18-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d21-x01-y01

.. figure:: d21-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d24-x01-y01

.. figure:: d24-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d27-x01-y01

.. figure:: d27-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d30-x01-y01

.. figure:: d30-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d33-x01-y01

.. figure:: d33-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d36-x01-y01

.. figure:: d36-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d39-x01-y01

.. figure:: d39-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d42-x01-y01

.. figure:: d42-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d45-x01-y01

.. figure:: d45-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d48-x01-y01

.. figure:: d48-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d51-x01-y01

.. figure:: d51-x01-y01_A.png
           :scale: 80%

Powheg + Pythia 8: d54-x01-y01

.. figure:: d54-x01-y01_A.png
           :scale: 80%

