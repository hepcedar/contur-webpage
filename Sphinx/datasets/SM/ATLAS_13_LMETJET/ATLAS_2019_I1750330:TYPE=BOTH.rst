:orphan:

Standard Model Predictions for ATLAS_2019_I1750330:TYPE=BOTH
============================================================


 PowhegBoxZpWp :cite:`Altakach:2020ugg,Altakach:2020azd,Altakach:2021lkq`: As used in Altakach et all arXiv:2111.15406
 Combined p-value for this prediction is 1.00E+00.


   :doc:`ATLAS_2019_I1750330:TYPE=BOTH prediction A <ATLAS_2019_I1750330:TYPE=BOTH/ATLAS_2019_I1750330:TYPE=BOTH_A>`.
