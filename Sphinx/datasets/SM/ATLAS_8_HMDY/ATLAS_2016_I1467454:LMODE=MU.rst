:orphan:

Standard Model Predictions for ATLAS_2016_I1467454:LMODE=MU
===========================================================


 FEWZ :cite:`Li:2012wna,Melnikov:2006kv,Gavin:2010az`: Predictions from the paper, taken from the ll ratio plot (Born) but applied to the dressed level ee & mm data as mult. factors.
 Combined p-value for this prediction is 8.66E-01.


   :doc:`ATLAS_2016_I1467454:LMODE=MU prediction A <ATLAS_2016_I1467454:LMODE=MU/ATLAS_2016_I1467454:LMODE=MU_A>`.
