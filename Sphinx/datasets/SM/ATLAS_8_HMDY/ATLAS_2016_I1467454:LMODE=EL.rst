:orphan:

Standard Model Predictions for ATLAS_2016_I1467454:LMODE=EL
===========================================================


 FEWZ :cite:`Li:2012wna,Melnikov:2006kv,Gavin:2010az`: Predictions from the paper, taken from the ll ratio plot (Born) but applied to the dressed level ee & mm data as mult. factors.
 Combined p-value for this prediction is 8.74E-01.


   :doc:`ATLAS_2016_I1467454:LMODE=EL prediction A <ATLAS_2016_I1467454:LMODE=EL/ATLAS_2016_I1467454:LMODE=EL_A>`.
