:orphan:

Standard Model Predictions for ATLAS_2016_I1467454:LMODE=EL
===========================================================

Predictions from the paper, taken from the ll ratio plot (Born) but applied to the dressed level ee & mm data as mult. factors. (Prediction ID A)



Stored in file: ATLAS_2016_I1467454:LMODE=EL-Theory.yoda 

FEWZ: d18-x01-y01

.. figure:: d18-x01-y01_A.png
           :scale: 80%

