:orphan:

Standard Model Predictions for ATLAS_2024_I2765017:TYPE=BSM
===========================================================

From measurement paper arXiv: 2403.02793 [hep-ex] (Prediction ID A)



Stored in file: ATLAS_2024_I2765017:TYPE=BSM-Theory.yoda 

MEPS@NLO $\times$ nNLO EW: rmiss_cr1ie_dphijj_vbf

.. figure:: rmiss_cr1ie_dphijj_vbf_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr1ie_met_mono

.. figure:: rmiss_cr1ie_met_mono_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr1ie_met_vbf

.. figure:: rmiss_cr1ie_met_vbf_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr1ie_mjj_vbf

.. figure:: rmiss_cr1ie_mjj_vbf_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr1im_dphijj_vbf

.. figure:: rmiss_cr1im_dphijj_vbf_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr1im_met_mono

.. figure:: rmiss_cr1im_met_mono_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr1im_met_vbf

.. figure:: rmiss_cr1im_met_vbf_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr1im_mjj_vbf

.. figure:: rmiss_cr1im_mjj_vbf_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr2ie_dphijj_vbf

.. figure:: rmiss_cr2ie_dphijj_vbf_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr2ie_met_mono

.. figure:: rmiss_cr2ie_met_mono_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr2ie_met_vbf

.. figure:: rmiss_cr2ie_met_vbf_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr2ie_mjj_vbf

.. figure:: rmiss_cr2ie_mjj_vbf_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr2im_dphijj_vbf

.. figure:: rmiss_cr2im_dphijj_vbf_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr2im_met_mono

.. figure:: rmiss_cr2im_met_mono_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr2im_met_vbf

.. figure:: rmiss_cr2im_met_vbf_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: rmiss_cr2im_mjj_vbf

.. figure:: rmiss_cr2im_mjj_vbf_A.png
           :scale: 80%

HEJ: sr0l_cr1ie_0v_dphijj_vbf

.. figure:: sr0l_cr1ie_0v_dphijj_vbf_A.png
           :scale: 80%

NNLO QCD $\times$ nNLO EW: sr0l_cr1ie_0v_met_mono

.. figure:: sr0l_cr1ie_0v_met_mono_A.png
           :scale: 80%

HEJ: sr0l_cr1ie_0v_met_vbf

.. figure:: sr0l_cr1ie_0v_met_vbf_A.png
           :scale: 80%

HEJ: sr0l_cr1ie_0v_mjj_vbf

.. figure:: sr0l_cr1ie_0v_mjj_vbf_A.png
           :scale: 80%

HEJ: sr0l_cr1im_0v_dphijj_vbf

.. figure:: sr0l_cr1im_0v_dphijj_vbf_A.png
           :scale: 80%

NNLO QCD $\times$ nNLO EW: sr0l_cr1im_0v_met_mono

.. figure:: sr0l_cr1im_0v_met_mono_A.png
           :scale: 80%

HEJ: sr0l_cr1im_0v_met_vbf

.. figure:: sr0l_cr1im_0v_met_vbf_A.png
           :scale: 80%

HEJ: sr0l_cr1im_0v_mjj_vbf

.. figure:: sr0l_cr1im_0v_mjj_vbf_A.png
           :scale: 80%

HEJ: sr0l_cr2ie_0v_dphijj_vbf

.. figure:: sr0l_cr2ie_0v_dphijj_vbf_A.png
           :scale: 80%

NNLO QCD $\times$ nNLO EW: sr0l_cr2ie_0v_met_mono

.. figure:: sr0l_cr2ie_0v_met_mono_A.png
           :scale: 80%

HEJ: sr0l_cr2ie_0v_met_vbf

.. figure:: sr0l_cr2ie_0v_met_vbf_A.png
           :scale: 80%

HEJ: sr0l_cr2ie_0v_mjj_vbf

.. figure:: sr0l_cr2ie_0v_mjj_vbf_A.png
           :scale: 80%

HEJ: sr0l_cr2im_0v_dphijj_vbf

.. figure:: sr0l_cr2im_0v_dphijj_vbf_A.png
           :scale: 80%

NNLO QCD $\times$ nNLO EW: sr0l_cr2im_0v_met_mono

.. figure:: sr0l_cr2im_0v_met_mono_A.png
           :scale: 80%

HEJ: sr0l_cr2im_0v_met_vbf

.. figure:: sr0l_cr2im_0v_met_vbf_A.png
           :scale: 80%

HEJ: sr0l_cr2im_0v_mjj_vbf

.. figure:: sr0l_cr2im_0v_mjj_vbf_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: sr0l_dphijj_vbf

.. figure:: sr0l_dphijj_vbf_A.png
           :scale: 80%

NNLO QCD $\times$ nNLO EW: sr0l_met_mono

.. figure:: sr0l_met_mono_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: sr0l_met_vbf

.. figure:: sr0l_met_vbf_A.png
           :scale: 80%

MEPS@NLO $\times$ nNLO EW: sr0l_mjj_vbf

.. figure:: sr0l_mjj_vbf_A.png
           :scale: 80%

