:orphan:

Standard Model Predictions for ATLAS_2024_I2765017:TYPE=BSM
===========================================================


 NNLO QCD $\times$ nNLO EW :cite:`ATLAS:2024vqf`: From measurement paper arXiv: 2403.02793 [hep-ex]
 Combined p-value for this prediction is 1.00E+00.


   :doc:`ATLAS_2024_I2765017:TYPE=BSM prediction A <ATLAS_2024_I2765017:TYPE=BSM/ATLAS_2024_I2765017:TYPE=BSM_A>`.

 MEPS@NLO $\times$ nNLO EW :cite:`ATLAS:2024vqf`: From paper measurement arXiv: 2403.02793 [hep-ex]
 Combined p-value for this prediction is 1.00E+00.


   :doc:`ATLAS_2024_I2765017:TYPE=BSM prediction B <ATLAS_2024_I2765017:TYPE=BSM/ATLAS_2024_I2765017:TYPE=BSM_B>`.
