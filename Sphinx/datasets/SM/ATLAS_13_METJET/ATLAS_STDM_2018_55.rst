:orphan:

Standard Model Predictions for ATLAS_STDM_2018_55
=================================================


 Sherpa 2.2.11 QCD V+jets MEPS@NLO (Could not find bibtex key) From paper measurement arXiv: 2403.02793 [hep-ex]
 Combined p-value for this prediction is 1.00E+00.


   :doc:`ATLAS_STDM_2018_55 prediction A <ATLAS_STDM_2018_55/ATLAS_STDM_2018_55_A>`.
