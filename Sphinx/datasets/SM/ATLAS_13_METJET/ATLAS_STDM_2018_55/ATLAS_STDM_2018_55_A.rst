:orphan:

Standard Model Predictions for ATLAS_STDM_2018_55
=================================================

From paper measurement arXiv: 2403.02793 [hep-ex] (Prediction ID A)



Stored in file: ATLAS_STDM_2018_55-Theory.yoda 

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_cr1ie_0v_dphijj_vbf

.. figure:: sr0l_cr1ie_0v_dphijj_vbf_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets reweighted to NNLO: sr0l_cr1ie_0v_met_mono

.. figure:: sr0l_cr1ie_0v_met_mono_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_cr1ie_0v_met_vbf

.. figure:: sr0l_cr1ie_0v_met_vbf_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_cr1ie_0v_mjj_vbf

.. figure:: sr0l_cr1ie_0v_mjj_vbf_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_cr1im_0v_dphijj_vbf

.. figure:: sr0l_cr1im_0v_dphijj_vbf_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets reweighted to NNLO: sr0l_cr1im_0v_met_mono

.. figure:: sr0l_cr1im_0v_met_mono_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_cr1im_0v_met_vbf

.. figure:: sr0l_cr1im_0v_met_vbf_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_cr1im_0v_mjj_vbf

.. figure:: sr0l_cr1im_0v_mjj_vbf_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_cr2ie_0v_dphijj_vbf

.. figure:: sr0l_cr2ie_0v_dphijj_vbf_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets reweighted to NNLO: sr0l_cr2ie_0v_met_mono

.. figure:: sr0l_cr2ie_0v_met_mono_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_cr2ie_0v_met_vbf

.. figure:: sr0l_cr2ie_0v_met_vbf_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_cr2ie_0v_mjj_vbf

.. figure:: sr0l_cr2ie_0v_mjj_vbf_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_cr2im_0v_dphijj_vbf

.. figure:: sr0l_cr2im_0v_dphijj_vbf_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets reweighted to NNLO: sr0l_cr2im_0v_met_mono

.. figure:: sr0l_cr2im_0v_met_mono_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_cr2im_0v_met_vbf

.. figure:: sr0l_cr2im_0v_met_vbf_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_cr2im_0v_mjj_vbf

.. figure:: sr0l_cr2im_0v_mjj_vbf_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_dphijj_vbf

.. figure:: sr0l_dphijj_vbf_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets reweighted to NNLO: sr0l_met_mono

.. figure:: sr0l_met_mono_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_met_vbf

.. figure:: sr0l_met_vbf_A.png
           :scale: 80%

Sherpa 2.2.11 QCD V+jets MEPS@NLO: sr0l_mjj_vbf

.. figure:: sr0l_mjj_vbf_A.png
           :scale: 80%

