:orphan:

Standard Model Predictions for ATLAS_2016_I1458270
==================================================

Background expectation from search paper. (Prediction ID A)



Stored in file: ATLAS_2016_I1458270-Theory.yoda 

Expected SM events: d04-x01-y01

.. figure:: d04-x01-y01_A.png
           :scale: 80%

Expected SM events: d05-x01-y01

.. figure:: d05-x01-y01_A.png
           :scale: 80%

Expected SM events: d06-x01-y01

.. figure:: d06-x01-y01_A.png
           :scale: 80%

Expected SM events: d07-x01-y01

.. figure:: d07-x01-y01_A.png
           :scale: 80%

Expected SM events: d08-x01-y01

.. figure:: d08-x01-y01_A.png
           :scale: 80%

Expected SM events: d09-x01-y01

.. figure:: d09-x01-y01_A.png
           :scale: 80%

Expected SM events: d10-x01-y01

.. figure:: d10-x01-y01_A.png
           :scale: 80%

