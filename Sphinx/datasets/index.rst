Measurements
============

To be useful in our approach, measurements must be made in as model-independent a fashion as possible. 

Cross sections should be measured in a kinematic region closely matching the detector acceptance
— commonly called ’fiducial cross sections’ — to avoid extrapolation into unmeasured regions. This is because 
such extrapolations must always make theoretical assumptions; usually that the SM is valid in the
unmeasured region. 

The measurements should generally be made in terms of observable final state particles
(e.g. leptons, photons) or objects constructed from such particles (e.g.hadronic jets, missing energy) 
rather than assumed intermediate states (:math:`W, Z, H`, top). 

Finally, differential measurements are most reliable for Contur, as features in the shapes of distributions are usually 
more robust against theory uncertainties than simple event rates -- especially when error correlation information is provided
by the experiments.

The measurements are split into statistically orthogonal data sets (analysis pools), based upon experiment, beam energy and
final state.


.. include:: data-list.rst


