.. raw:: html 
 
 <style> .red {color:red} </style> 
 
.. role:: red

Current Data 
------------ 

 Pool: **ATLAS_13_4L**  *four leptons* 

   * `ATLAS_2017_I1625109 <https://rivet.hepforge.org/analyses/ATLAS_2017_I1625109.html>`_, Measurement of $ZZ -> 4\ell$ production at 13 TeV :cite:`ATLAS:2017bcd`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2019_I1720442 <https://rivet.hepforge.org/analyses/ATLAS_2019_I1720442.html>`_, Inclusive 4-lepton lineshape at 13 TeV :cite:`ATLAS:2019qet`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_4L/ATLAS_2019_I1720442>`.
   * `ATLAS_2021_I1849535 <https://rivet.hepforge.org/analyses/ATLAS_2021_I1849535.html>`_, Inclusive 4-lepton cross sections at 13 TeV :cite:`ATLAS:2021kog`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_4L/ATLAS_2021_I1849535>`.
   * `ATLAS_2023_I2690799 <https://rivet.hepforge.org/analyses/ATLAS_2023_I2690799.html>`_, Measurement of electroweak 4 lepton + 2 jet production :cite:`ATLAS:2023dkz`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_4L/ATLAS_2023_I2690799>`.

 Pool: **ATLAS_13_GAMMA**  *inclusive (multi)photons* 

   * `ATLAS_2021_I1887997 <https://rivet.hepforge.org/analyses/ATLAS_2021_I1887997.html>`_, $\gamma\gamma$ production at 13 TeV :cite:`ATLAS:2021mbt`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_GAMMA/ATLAS_2021_I1887997>`.
   * `ATLAS_2017_I1645627 <https://rivet.hepforge.org/analyses/ATLAS_2017_I1645627.html>`_, Isolated photon + jets at 13 TeV :cite:`ATLAS:2017xqp`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_GAMMA/ATLAS_2017_I1645627>`.
   * `ATLAS_2019_I1772071 <https://rivet.hepforge.org/analyses/ATLAS_2019_I1772071.html>`_, Measurement of isolated-photon plus two-jet production :cite:`ATLAS:2019iaa`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_GAMMA/ATLAS_2019_I1772071>`.

 Pool: **ATLAS_13_GAMMA_MET**  *photon plus missing transverse momentum* 

   * `ATLAS_2018_I1698006:LVETO=ON <https://rivet.hepforge.org/analyses/ATLAS_2018_I1698006.html>`_, Z(vv)+gamma measurement at 13 TeV :cite:`ATLAS:2018nci`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_GAMMA_MET/ATLAS_2018_I1698006:LVETO=ON>`.

 Pool: **ATLAS_13_JETS**  *inclusive hadronic final states* 

   * `ATLAS_2018_I1634970 <https://rivet.hepforge.org/analyses/ATLAS_2018_I1634970.html>`_, ATLAS Inclusive jet and dijet cross section measurement at sqrt(s)=13TeV :cite:`ATLAS:2017ble`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_JETS/ATLAS_2018_I1634970>`.
   * `ATLAS_2019_I1724098 <https://rivet.hepforge.org/analyses/ATLAS_2019_I1724098.html>`_, Jet substructure at 13 TeV :cite:`ATLAS:2019kwg`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2017_I1637587 <https://rivet.hepforge.org/analyses/ATLAS_2017_I1637587.html>`_, Soft-Drop Jet Mass at 13 TeV :cite:`ATLAS:2017zda`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2020_I1808726 <https://rivet.hepforge.org/analyses/ATLAS_2020_I1808726.html>`_, Hadronic event shapes in multijet final states :cite:`ATLAS:2020vup`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2016_I1419652 <https://rivet.hepforge.org/analyses/ATLAS_2016_I1419652.html>`_, Track-based minimum bias at 13 TeV in ATLAS :cite:`ATLAS:2016zkp`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2016_I1467230 <https://rivet.hepforge.org/analyses/ATLAS_2016_I1467230.html>`_, Track-based minimum bias with low-pT tracks at 13 TeV :cite:`ATLAS:2016zba`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2019_I1740909 <https://rivet.hepforge.org/analyses/ATLAS_2019_I1740909.html>`_, Jet fragmentation using charged particles :cite:`ATLAS:2019rqw`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2020_I1790256 <https://rivet.hepforge.org/analyses/ATLAS_2020_I1790256.html>`_, Lund jet plane with charged particles :cite:`ATLAS:2020bbn`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **ATLAS_13_L1L2MET**  *unlike dileptons plus missing transverse momentum channel, with jet veto* 

   * `ATLAS_2022_I2103950 <https://rivet.hepforge.org/analyses/ATLAS_2022_I2103950.html>`_, WW production in pp at 13 TeV in electroweak SUSY inspired phase space :cite:`ATLAS:2022jat`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_L1L2MET/ATLAS_2022_I2103950>`.
   * `ATLAS_2019_I1734263 <https://rivet.hepforge.org/analyses/ATLAS_2019_I1734263.html>`_, WW production at 13 TeV :cite:`ATLAS:2019rob`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_L1L2MET/ATLAS_2019_I1734263>`.

 Pool: **ATLAS_13_L1L2METJET**  *unlike dileptons plus missing transverse momentum and jets* 

   * `ATLAS_2019_I1718132:LMODE=ELMU <https://rivet.hepforge.org/analyses/ATLAS_2019_I1718132.html>`_, Control region measurements for leptoquark search at 13 TeV :cite:`ATLAS:2019ebv`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_L1L2METJET/ATLAS_2019_I1718132:LMODE=ELMU>`.
   * `ATLAS_2023_I2648096 <https://rivet.hepforge.org/analyses/ATLAS_2023_I2648096.html>`_, dileptonic ttbar at 13 TeV :cite:`ATLAS:2023gsl`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_L1L2METJET/ATLAS_2023_I2648096>`.
   * `ATLAS_2021_I1852328 <https://rivet.hepforge.org/analyses/ATLAS_2021_I1852328.html>`_, WW + $\geq$1 jet production at 13 TeV :cite:`ATLAS:2021jgw`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_L1L2METJET/ATLAS_2021_I1852328>`.
   * `ATLAS_2019_I1759875 <https://rivet.hepforge.org/analyses/ATLAS_2019_I1759875.html>`_, dileptonic ttbar at 13 TeV :cite:`ATLAS:2019hau`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_L1L2METJET/ATLAS_2019_I1759875>`.

 Pool: **ATLAS_13_L1L2MET_GAMMA**  *fully leptonic ttbar plus photon* 

   * `ATLAS_2018_I1707015:LMODE=DILEPTON <https://rivet.hepforge.org/analyses/ATLAS_2018_I1707015.html>`_, Measurement of normalized differential distributions in ttbar+photon production at 13 TeV :cite:`ATLAS:2018sos`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_L1L2MET_GAMMA/ATLAS_2018_I1707015:LMODE=DILEPTON>`.
   * `ATLAS_2024_I2768921:LMODE=DILEPTON <https://rivet.hepforge.org/analyses/ATLAS_2024_I2768921.html>`_, ttbar + gamma at 13 TeV :cite:`ATLAS:2024hmk`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_L1L2MET_GAMMA/ATLAS_2024_I2768921:LMODE=DILEPTON>`.

 Pool: **ATLAS_13_LLJET**  *dileptons at the Z pole, plus optional jets* 

   * `ATLAS_2017_I1627873 <https://rivet.hepforge.org/analyses/ATLAS_2017_I1627873.html>`_, EW Zjj using early Run-2 data :cite:`ATLAS:2017nei`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LLJET/ATLAS_2017_I1627873>`.
   * `ATLAS_2022_I2077570 <https://rivet.hepforge.org/analyses/ATLAS_2022_I2077570.html>`_, Z + high transverse momentum jets at ATLAS :cite:`ATLAS:2022nrp`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LLJET/ATLAS_2022_I2077570>`.
   * `ATLAS_2017_I1514251:LMODE=EMU <https://rivet.hepforge.org/analyses/ATLAS_2017_I1514251.html>`_, Z plus jets at 13 TeV :cite:`ATLAS:2017sag`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2020_I1788444 <https://rivet.hepforge.org/analyses/ATLAS_2020_I1788444.html>`_, Differential cross-sections for Z + b-jets :cite:`ATLAS:2020juj`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LLJET/ATLAS_2020_I1788444>`.
   * `ATLAS_2020_I1803608 <https://rivet.hepforge.org/analyses/ATLAS_2020_I1803608.html>`_, Electroweak Zjj at 13 TeV :cite:`ATLAS:2020nzk`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2019_I1768911 <https://rivet.hepforge.org/analyses/ATLAS_2019_I1768911.html>`_, Z pT and Z phi* at 13 TeV :cite:`ATLAS:2019zci`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2019_I1718132:LMODE=ELEL <https://rivet.hepforge.org/analyses/ATLAS_2019_I1718132.html>`_, Control region measurements for leptoquark search at 13 TeV :cite:`ATLAS:2019ebv`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LLJET/ATLAS_2019_I1718132:LMODE=ELEL>`.
   * `ATLAS_2019_I1718132:LMODE=MUMU <https://rivet.hepforge.org/analyses/ATLAS_2019_I1718132.html>`_, Control region measurements for leptoquark search at 13 TeV :cite:`ATLAS:2019ebv`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LLJET/ATLAS_2019_I1718132:LMODE=MUMU>`.
   * `ATLAS_2024_I2771257 <https://rivet.hepforge.org/analyses/ATLAS_2024_I2771257.html>`_, Z+b(b) and Z+c at 13 TeV :cite:`ATLAS:2024tnr`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LLJET/ATLAS_2024_I2771257>`.

 Pool: **ATLAS_13_LL_GAMMA**  *dileptons plus photon* 

   * `ATLAS_2019_I1764342 <https://rivet.hepforge.org/analyses/ATLAS_2019_I1764342.html>`_, Z(ll)y cross-section at 13 TeV :cite:`ATLAS:2019gey`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LL_GAMMA/ATLAS_2019_I1764342>`.
   * `ATLAS_2022_I2593322 <https://rivet.hepforge.org/analyses/ATLAS_2022_I2593322.html>`_, Zyy cross-section measurement at 13 TeV :cite:`ATLAS:2022wmu`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LL_GAMMA/ATLAS_2022_I2593322>`.
   * `ATLAS_2022_I2614196 <https://rivet.hepforge.org/analyses/ATLAS_2022_I2614196.html>`_, Zy+jets at 13 TeV :cite:`ATLAS:2022wnf`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LL_GAMMA/ATLAS_2022_I2614196>`.

 Pool: **ATLAS_13_LMETJET**  *lepton, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)* 

   * `ATLAS_2017_I1614149 <https://rivet.hepforge.org/analyses/ATLAS_2017_I1614149.html>`_, Resolved and boosted ttbar l+jets cross sections at 13 TeV :cite:`ATLAS:2017cez`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LMETJET/ATLAS_2017_I1614149>`.
   * `ATLAS_2019_I1750330:TYPE=BOTH <https://rivet.hepforge.org/analyses/ATLAS_2019_I1750330.html>`_, Semileptonic ttbar at 13 TeV :cite:`ATLAS:2019hxz`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LMETJET/ATLAS_2019_I1750330:TYPE=BOTH>`.
   * `ATLAS_2023_I2628732 <https://rivet.hepforge.org/analyses/ATLAS_2023_I2628732.html>`_, W+D production in pp at 13 TeV :cite:`ATLAS:2023ibp`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2022_I2037744 <https://rivet.hepforge.org/analyses/ATLAS_2022_I2037744.html>`_, Semileptonic ttbar with high pT top at 13 TeV, single- and double-differential cross-sections :cite:`ATLAS:2022xfj`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LMETJET/ATLAS_2022_I2037744>`.
   * `ATLAS_2018_I1656578 <https://rivet.hepforge.org/analyses/ATLAS_2018_I1656578.html>`_, Differential $t\bar{t}$ $l$+jets cross-sections at 13~TeV :cite:`ATLAS:2018acq`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LMETJET/ATLAS_2018_I1656578>`.
   * `ATLAS_2018_I1705857 <https://rivet.hepforge.org/analyses/ATLAS_2018_I1705857.html>`_, ttbb at 13 TeV :cite:`ATLAS:2018fwl`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LMETJET/ATLAS_2018_I1705857>`.

 Pool: **ATLAS_13_LMET_GAMMA**  *semileptonic ttbar plus photon* 

   * `ATLAS_2018_I1707015:LMODE=SINGLE <https://rivet.hepforge.org/analyses/ATLAS_2018_I1707015.html>`_, Measurement of normalized differential distributions in ttbar+photon production at 13 TeV :cite:`ATLAS:2018sos`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LMET_GAMMA/ATLAS_2018_I1707015:LMODE=SINGLE>`.
   * `ATLAS_2024_I2768921:LMODE=SINGLE <https://rivet.hepforge.org/analyses/ATLAS_2024_I2768921.html>`_, ttbar + gamma at 13 TeV :cite:`ATLAS:2024hmk`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_LMET_GAMMA/ATLAS_2024_I2768921:LMODE=SINGLE>`.

 Pool: **ATLAS_13_SSLLMET**  *two same-sign leptons (e/mu) plus missing transverse momentum and jets* 

   * `ATLAS_2019_I1738841 <https://rivet.hepforge.org/analyses/ATLAS_2019_I1738841.html>`_, Same-sign $WW$ Observation at 13 TeV :cite:`ATLAS:2019cbr`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_SSLLMET/ATLAS_2019_I1738841>`.

 Pool: **ATLAS_13_TTHAD**  *fully hadronic top events* 

   * `ATLAS_2018_I1646686 <https://rivet.hepforge.org/analyses/ATLAS_2018_I1646686.html>`_, All-hadronic boosted ttbar at 13 TeV :cite:`ATLAS:2018orx`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_TTHAD/ATLAS_2018_I1646686>`.
   * `ATLAS_2020_I1801434 <https://rivet.hepforge.org/analyses/ATLAS_2020_I1801434.html>`_, Top-quark pair single- and double-differential cross-sections in the all-hadronic channel :cite:`ATLAS:2020ccu`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_TTHAD/ATLAS_2020_I1801434>`.
   * `ATLAS_2022_I2077575 <https://rivet.hepforge.org/analyses/ATLAS_2022_I2077575.html>`_, All-hadronic boosted ttbar at 13 TeV :cite:`ATLAS:2022mlu`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_TTHAD/ATLAS_2022_I2077575>`.

 Pool: **ATLAS_7_4L**  *four leptons* 

   * `ATLAS_2012_I1203852:LMODE=LL <https://rivet.hepforge.org/analyses/ATLAS_2012_I1203852.html>`_, Measurement of the $ZZ(*)$ production cross-section in $pp$ collisions at 7 TeV with ATLAS :cite:`ATLAS:2012bra`.  SM theory predictions are available :doc:`here <SM/ATLAS_7_4L/ATLAS_2012_I1203852:LMODE=LL>`.

 Pool: **ATLAS_7_GAMMA**  *inclusive (multi)photons* 

   * `ATLAS_2012_I1093738 <https://rivet.hepforge.org/analyses/ATLAS_2012_I1093738.html>`_, Isolated prompt photon + jet cross-section :cite:`ATLAS:2012ar`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2013_I1244522 <https://rivet.hepforge.org/analyses/ATLAS_2013_I1244522.html>`_, Photon + jets :cite:`ATLAS:2013hsm`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2013_I1263495 <https://rivet.hepforge.org/analyses/ATLAS_2013_I1263495.html>`_, Inclusive isolated prompt photon analysis with 2011 LHC data :cite:`ATLAS:2013sdn`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2012_I1199269 <https://rivet.hepforge.org/analyses/ATLAS_2012_I1199269.html>`_, Inclusive diphoton $+ X$ events at $\sqrt{s} = 7$~TeV :cite:`ATLAS:2012fgo`.  SM theory predictions are available :doc:`here <SM/ATLAS_7_GAMMA/ATLAS_2012_I1199269>`.

 Pool: **ATLAS_7_HMDY**  *dileptons above the Z pole* 

   * `ATLAS_2013_I1234228 <https://rivet.hepforge.org/analyses/ATLAS_2013_I1234228.html>`_, High-mass Drell-Yan at 7 TeV :cite:`ATLAS:2013xny`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **ATLAS_7_JETS**  *inclusive hadronic final states* 

   * `ATLAS_2016_I1478355 <https://rivet.hepforge.org/analyses/ATLAS_2016_I1478355.html>`_, Measurement of the bbar dijet dijet cross section at 7 TeV :cite:`ATLAS:2016anw`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2014_I1325553 <https://rivet.hepforge.org/analyses/ATLAS_2014_I1325553.html>`_, Measurement of the inclusive jet cross-section at 7 TeV :cite:`ATLAS:2014riz`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2014_I1268975 <https://rivet.hepforge.org/analyses/ATLAS_2014_I1268975.html>`_, High-mass dijet cross section :cite:`ATLAS:2013jmu`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2014_I1326641 <https://rivet.hepforge.org/analyses/ATLAS_2014_I1326641.html>`_, 3-jet cross section with 7 TeV data :cite:`ATLAS:2014qmg`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2014_I1307243 <https://rivet.hepforge.org/analyses/ATLAS_2014_I1307243.html>`_, Measurements of jet vetoes and azimuthal decorrelations in dijet events produced in pp collisions at $\sqrt{s}$ = 7 TeV using the ATLAS detector :cite:`ATLAS:2014lzu`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **ATLAS_7_L1L2MET**  *WW analyses in dileptons plus missing transverse momentum channel* 

   * `ATLAS_2013_I1190187 <https://rivet.hepforge.org/analyses/ATLAS_2013_I1190187.html>`_, Measurement of the $W^+ W^-$ production cross-section at 7 TeV :cite:`ATLAS:2012mec`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **ATLAS_7_LLJET**  *dielectrons at the Z pole, plus optional jets* 

   * `ATLAS_2013_I1230812 <https://rivet.hepforge.org/analyses/ATLAS_2013_I1230812.html>`_, $Z$ + jets in $pp$ at 7 TeV :cite:`ATLAS:2013lly`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2014_I1306294:LMODE=EL <https://rivet.hepforge.org/analyses/ATLAS_2014_I1306294.html>`_, Measurement of Z boson in association with b-jets at 7 TeV in ATLAS (electron channel) :cite:`ATLAS:2014rjv`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2016_I1502620:LMODE=Z <https://rivet.hepforge.org/analyses/ATLAS_2016_I1502620.html>`_, W and Z inclusive cross sections at 7 TeV :cite:`ATLAS:2016nqi`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **ATLAS_7_LLMET**  *dileptons plus missing transverse momentum* 

   * `ATLAS_2012_I1203852:LMODE=LNU <https://rivet.hepforge.org/analyses/ATLAS_2012_I1203852.html>`_, Measurement of the $ZZ(*)$ production cross-section in $pp$ collisions at 7 TeV with ATLAS :cite:`ATLAS:2012bra`.  SM theory predictions are available :doc:`here <SM/ATLAS_7_LLMET/ATLAS_2012_I1203852:LMODE=LNU>`.

 Pool: **ATLAS_7_LL_GAMMA**  *dileptons plus photon(s)* 

   * `ATLAS_2013_I1217863:LMODE=ZEL <https://rivet.hepforge.org/analyses/ATLAS_2013_I1217863.html>`_, W/Z + gamma production at 7 TeV :cite:`ATLAS:2013way`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2013_I1217863:LMODE=ZMU <https://rivet.hepforge.org/analyses/ATLAS_2013_I1217863.html>`_, W/Z + gamma production at 7 TeV :cite:`ATLAS:2013way`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **ATLAS_7_LMDY**  *dileptons below the Z pole* 

   * `ATLAS_2014_I1288706 <https://rivet.hepforge.org/analyses/ATLAS_2014_I1288706.html>`_, Measurement of the low-mass Drell-Yan differential cross section at 7 TeV :cite:`ATLAS:2014ape`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **ATLAS_7_LMETJET**  *lepton, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)* 

   * `ATLAS_2015_I1345452 <https://rivet.hepforge.org/analyses/ATLAS_2015_I1345452.html>`_, Pseudo-top-antitop cross sections :cite:`ATLAS:2015dbj`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2016_I1502620:LMODE=W <https://rivet.hepforge.org/analyses/ATLAS_2016_I1502620.html>`_, W and Z inclusive cross sections at 7 TeV :cite:`ATLAS:2016nqi`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2014_I1319490 <https://rivet.hepforge.org/analyses/ATLAS_2014_I1319490.html>`_, W + jets :cite:`ATLAS:2014fjg`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2014_I1282447 <https://rivet.hepforge.org/analyses/ATLAS_2014_I1282447.html>`_, W + charm production at 7 TeV :cite:`ATLAS:2014jkm`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2013_I1219109 <https://rivet.hepforge.org/analyses/ATLAS_2013_I1219109.html>`_, W + b production at 7 TeV :cite:`ATLAS:2013gjg`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **ATLAS_7_LMET_GAMMA**  *lepton, missing transverse momentum, plus photon* 

   * `ATLAS_2013_I1217863:LMODE=WEL <https://rivet.hepforge.org/analyses/ATLAS_2013_I1217863.html>`_, W/Z + gamma production at 7 TeV :cite:`ATLAS:2013way`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2013_I1217863:LMODE=WMU <https://rivet.hepforge.org/analyses/ATLAS_2013_I1217863.html>`_, W/Z + gamma production at 7 TeV :cite:`ATLAS:2013way`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **ATLAS_8_3L**  *trileptons* 

   * `ATLAS_2016_I1492320:LMODE=3L <https://rivet.hepforge.org/analyses/ATLAS_2016_I1492320.html>`_, WWW production at 8 TeV :cite:`ATLAS:2016jeu`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_3L/ATLAS_2016_I1492320:LMODE=3L>`.

 Pool: **ATLAS_8_4L**  *four leptons* 

   * `ATLAS_2015_I1394865 <https://rivet.hepforge.org/analyses/ATLAS_2015_I1394865.html>`_, Inclusive 4-lepton lineshape :cite:`ATLAS:2015rsx`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2014_I1310835 <https://rivet.hepforge.org/analyses/ATLAS_2014_I1310835.html>`_, H(125) -> 4l at 8 TeV :cite:`ATLAS:2014xzb`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_4L/ATLAS_2014_I1310835>`.
   * `ATLAS_2016_I1494075:LMODE=4L <https://rivet.hepforge.org/analyses/ATLAS_2016_I1494075.html>`_, ZZ -> 4 leptons / 2 leptons and 2 neutrinos measurment at 8TeV :cite:`ATLAS:2016bxw`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_4L/ATLAS_2016_I1494075:LMODE=4L>`.

 Pool: **ATLAS_8_GAMMA**  *inclusive (multi)photons* 

   * `ATLAS_2016_I1457605 <https://rivet.hepforge.org/analyses/ATLAS_2016_I1457605.html>`_, Inclusive prompt photons at 8 TeV :cite:`ATLAS:2016fta`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_GAMMA/ATLAS_2016_I1457605>`.
   * `ATLAS_2017_I1632756 <https://rivet.hepforge.org/analyses/ATLAS_2017_I1632756.html>`_, Photon + heavy flavour at 8 TeV :cite:`ATLAS:2017qlc`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2017_I1591327 <https://rivet.hepforge.org/analyses/ATLAS_2017_I1591327.html>`_, Inclusive diphoton cross-sections at 8 TeV :cite:`ATLAS:2017cvh`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_GAMMA/ATLAS_2017_I1591327>`.
   * `ATLAS_2017_I1644367 <https://rivet.hepforge.org/analyses/ATLAS_2017_I1644367.html>`_, Isolated triphotons at 8 TeV :cite:`ATLAS:2017lpx`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_GAMMA/ATLAS_2017_I1644367>`.

 Pool: **ATLAS_8_GAMMA_MET**  *photon plus missing transverse momentum* 

   * `ATLAS_2016_I1448301:LMODE=NU <https://rivet.hepforge.org/analyses/ATLAS_2016_I1448301.html>`_, $Z \gamma (\gamma)$ cross sections at 8 TeV :cite:`ATLAS:2016qjc`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_GAMMA_MET/ATLAS_2016_I1448301:LMODE=NU>`.

 Pool: **ATLAS_8_HMDY**  *dileptons above the Z pole* 

   * `ATLAS_2016_I1467454:LMODE=EL <https://rivet.hepforge.org/analyses/ATLAS_2016_I1467454.html>`_, High-mass Drell-Yan at 8 TeV :cite:`ATLAS:2016gic`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_HMDY/ATLAS_2016_I1467454:LMODE=EL>`.
   * `ATLAS_2016_I1467454:LMODE=MU <https://rivet.hepforge.org/analyses/ATLAS_2016_I1467454.html>`_, High-mass Drell-Yan at 8 TeV :cite:`ATLAS:2016gic`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_HMDY/ATLAS_2016_I1467454:LMODE=MU>`.

 Pool: **ATLAS_8_JETS**  *inclusive hadronic final states* 

   * `ATLAS_2015_I1394679 <https://rivet.hepforge.org/analyses/ATLAS_2015_I1394679.html>`_, Multijets at 8 TeV :cite:`ATLAS:2015xtc`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_JETS/ATLAS_2015_I1394679>`.
   * `ATLAS_2017_I1598613:BMODE=3MU <https://rivet.hepforge.org/analyses/ATLAS_2017_I1598613.html>`_, BB to Jpsi plus mu at 8 TeV :cite:`ATLAS:2017wfq`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2017_I1604271 <https://rivet.hepforge.org/analyses/ATLAS_2017_I1604271.html>`_, ATLAS Inclusive jet cross section measurement at sqrt(s)=8TeV :cite:`ATLAS:2017kux`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **ATLAS_8_L1L2MET**  *WW analyses in dileptons plus missing transverse momentum channel* 

   * `ATLAS_2016_I1426515 <https://rivet.hepforge.org/analyses/ATLAS_2016_I1426515.html>`_, WW production at 8 TeV :cite:`ATLAS:2016zwm`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2016_I1492320:LMODE=2L2J <https://rivet.hepforge.org/analyses/ATLAS_2016_I1492320.html>`_, WWW production at 8 TeV :cite:`ATLAS:2016jeu`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_L1L2MET/ATLAS_2016_I1492320:LMODE=2L2J>`.

 Pool: **ATLAS_8_LLJET**  *dileptons at the Z pole, plus optional jets* 

   * `ATLAS_2014_I1279489 <https://rivet.hepforge.org/analyses/ATLAS_2014_I1279489.html>`_, Measurements of electroweak production of dijets + $Z$ boson, and distributions sensitive to vector boson fusion :cite:`ATLAS:2014sjq`. :red:`No SM theory predictions available for this analysis.` 
   * `ATLAS_2015_I1408516:LMODE=EL <https://rivet.hepforge.org/analyses/ATLAS_2015_I1408516.html>`_, $Z$ $p_T$ and $Z$ $\phi^*$ :cite:`ATLAS:2015iiu`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_LLJET/ATLAS_2015_I1408516:LMODE=EL>`.
   * `ATLAS_2015_I1408516:LMODE=MU <https://rivet.hepforge.org/analyses/ATLAS_2015_I1408516.html>`_, $Z$ $p_T$ and $Z$ $\phi^*$ :cite:`ATLAS:2015iiu`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_LLJET/ATLAS_2015_I1408516:LMODE=MU>`.
   * `ATLAS_2019_I1744201 <https://rivet.hepforge.org/analyses/ATLAS_2019_I1744201.html>`_, Z+jet at 8 TeV :cite:`ATLAS:2019bsa`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_LLJET/ATLAS_2019_I1744201>`.
   * `ATLAS_2017_I1589844 <https://rivet.hepforge.org/analyses/ATLAS_2017_I1589844.html>`_, $k_T$ splittings in $Z$ events at 8 TeV :cite:`ATLAS:2017arb`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **ATLAS_8_LLMET**  *dileptons plus missing transverse momentum* 

   * `ATLAS_2016_I1494075:LMODE=2L2NU <https://rivet.hepforge.org/analyses/ATLAS_2016_I1494075.html>`_, ZZ -> 4 leptons / 2 leptons and 2 neutrinos measurment at 8TeV :cite:`ATLAS:2016bxw`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_LLMET/ATLAS_2016_I1494075:LMODE=2L2NU>`.

 Pool: **ATLAS_8_LL_GAMMA**  *dileptons plus photon(s)* 

   * `ATLAS_2016_I1448301:LMODE=LL <https://rivet.hepforge.org/analyses/ATLAS_2016_I1448301.html>`_, $Z \gamma (\gamma)$ cross sections at 8 TeV :cite:`ATLAS:2016qjc`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_LL_GAMMA/ATLAS_2016_I1448301:LMODE=LL>`.

 Pool: **ATLAS_8_LMETJET**  *lepton, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)* 

   * `ATLAS_2015_I1404878 <https://rivet.hepforge.org/analyses/ATLAS_2015_I1404878.html>`_, ttbar (to l+jets) differential cross sections at 8 TeV :cite:`ATLAS:2015lsn`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_LMETJET/ATLAS_2015_I1404878>`.
   * `ATLAS_2015_I1397637 <https://rivet.hepforge.org/analyses/ATLAS_2015_I1397637.html>`_, Boosted ttbar differential cross-section :cite:`ATLAS:2015mip`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_LMETJET/ATLAS_2015_I1397637>`.
   * `ATLAS_2017_I1517194 <https://rivet.hepforge.org/analyses/ATLAS_2017_I1517194.html>`_, Electroweak $Wjj$ production at 8~TeV :cite:`ATLAS:2017luz`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_LMETJET/ATLAS_2017_I1517194>`.
   * `ATLAS_2018_I1635273:LMODE=EL <https://rivet.hepforge.org/analyses/ATLAS_2018_I1635273.html>`_, W + jets production at 8 TeV :cite:`ATLAS:2017irc`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_LMETJET/ATLAS_2018_I1635273:LMODE=EL>`.
   * `ATLAS_2018_I1635273:LMODE=MU <https://rivet.hepforge.org/analyses/ATLAS_2018_I1635273.html>`_, W + jets production at 8 TeV :cite:`ATLAS:2017irc`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_13_3LJET**  *dileptons or trileptons + MET* 

   * `CMS_2020_I1794169 <https://rivet.hepforge.org/analyses/CMS_2020_I1794169.html>`_, Measurements of production cross sections of WZ and same-sign WW boson pairs in association with two jets in proton-proton collisions at $\sqrt{s} =$ 13 TeV :cite:`CMS:2020gfh`.  SM theory predictions are available :doc:`here <SM/CMS_13_3LJET/CMS_2020_I1794169>`.

 Pool: **CMS_13_HMDY**  *dileptons above the Z pole* 

   * `CMS_2018_I1711625 <https://rivet.hepforge.org/analyses/CMS_2018_I1711625.html>`_, Measurement of the differential Drell-Yan cross section in proton-proton collisions at $\sqrt{s} = 13\,TeV$ :cite:`CMS:2018mdl`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_13_JETS**  *inclusive hadronic final states* 

   * `CMS_2021_I1932460 <https://rivet.hepforge.org/analyses/CMS_2021_I1932460.html>`_, Measurement of double-parton scattering in inclusive production of four jets with low transverse momentum in proton-proton collisions at $\sqrt{s}$ = 13 TeV. :cite:`CMS:2021lxi`.  SM theory predictions are available :doc:`here <SM/CMS_13_JETS/CMS_2021_I1932460>`.
   * `CMS_2021_I1847230:MODE=QCD13TeV <https://rivet.hepforge.org/analyses/CMS_2021_I1847230.html>`_, Measurements of angular distance and momentum ratio distributions in three-jet and Z + two-jet final states in pp collisions at 8 and 13 TeV :cite:`CMS:2021hnp`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2016_I1459051 <https://rivet.hepforge.org/analyses/CMS_2016_I1459051.html>`_, Measurement of the inclusive jet cross-section in $pp$ collisions at $\sqrt{s} = 13~\TeV$ :cite:`CMS:2016jip`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2018_I1682495 <https://rivet.hepforge.org/analyses/CMS_2018_I1682495.html>`_, Jet mass in dijet events in $pp$ collisions at 13~TeV :cite:`CMS:2018vzn`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_13_L1L2MET**  *WW analyses in dileptons plus optional jets* 

   * `CMS_2020_I1814328 <https://rivet.hepforge.org/analyses/CMS_2020_I1814328.html>`_, W$^+$W$^-$ boson pair production in proton-proton collisions at $\sqrt{s} =$ 13 TeV :cite:`CMS:2020mxy`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2022_I2080534 <https://rivet.hepforge.org/analyses/CMS_2022_I2080534.html>`_, EW W+W- production at 13 TeV :cite:`CMS:2022woe`.  SM theory predictions are available :doc:`here <SM/CMS_13_L1L2MET/CMS_2022_I2080534>`.

 Pool: **CMS_13_L1L2MET_GAMMA**  *unlike dileptons, MET, photons* 

   * `CMS_2023_I2709669 <https://rivet.hepforge.org/analyses/CMS_2023_I2709669.html>`_, WWgamma production in emu final state at 13 TeV :cite:`CMS:2023rcv`.  SM theory predictions are available :doc:`here <SM/CMS_13_L1L2MET_GAMMA/CMS_2023_I2709669>`.

 Pool: **CMS_13_LLJET**  *dileptons at the Z pole, plus optional jets* 

   * `CMS_2022_I2079374 <https://rivet.hepforge.org/analyses/CMS_2022_I2079374.html>`_, Measurement of the mass dependence of the transverse momentum of lepton pairs in Drell--Yan production in proton-proton collisions at 13 TeV :cite:`CMS:2022ubq`.  SM theory predictions are available :doc:`here <SM/CMS_13_LLJET/CMS_2022_I2079374>`.
   * `CMS_2018_I1667854:LMODE=EMU <https://rivet.hepforge.org/analyses/CMS_2018_I1667854.html>`_, Differential cross section of Z boson production in association with jets in proton-proton collisions at $\sqrt{s} = 13\,$TeV :cite:`CMS:2018mdf`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2019_I1753680:LMODE=EMU <https://rivet.hepforge.org/analyses/CMS_2019_I1753680.html>`_, Measurements of differential Z boson production cross sections in proton-proton collisions at 13 TeV :cite:`CMS:2019raw`.  SM theory predictions are available :doc:`here <SM/CMS_13_LLJET/CMS_2019_I1753680:LMODE=EMU>`.
   * `CMS_2021_I1866118 <https://rivet.hepforge.org/analyses/CMS_2021_I1866118.html>`_, Study of Z boson plus jets events using variables sensitive to double-parton scattering in pp collisions at 13 TeV :cite:`CMS:2021wfx`.  SM theory predictions are available :doc:`here <SM/CMS_13_LLJET/CMS_2021_I1866118>`.

 Pool: **CMS_13_LMETJET**  *lepton, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)* 

   * `CMS_2017_I1610623 <https://rivet.hepforge.org/analyses/CMS_2017_I1610623.html>`_, Differential cross-sections for W boson and jets in proton-proton collisions at 13 TeV :cite:`CMS:2017gbl`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2016_I1491950 <https://rivet.hepforge.org/analyses/CMS_2016_I1491950.html>`_, Differential cross sections for top quark pair production using the lepton+jets final state in proton proton collisions at 13 TeV :cite:`CMS:2016oae`.  SM theory predictions are available :doc:`here <SM/CMS_13_LMETJET/CMS_2016_I1491950>`.
   * `CMS_2018_I1662081 <https://rivet.hepforge.org/analyses/CMS_2018_I1662081.html>`_, Measurement of the differential cross sections of top quark pair production as a function of kinematic event variables in pp collisions at sqrt(s) = 13 TeV :cite:`CMS:2018tdx`.  SM theory predictions are available :doc:`here <SM/CMS_13_LMETJET/CMS_2018_I1662081>`.
   * `CMS_2018_I1663958 <https://rivet.hepforge.org/analyses/CMS_2018_I1663958.html>`_, ttbar lepton+jets 13 TeV :cite:`CMS:2018htd`.  SM theory predictions are available :doc:`here <SM/CMS_13_LMETJET/CMS_2018_I1663958>`.
   * `CMS_2019_I1705068 <https://rivet.hepforge.org/analyses/CMS_2019_I1705068.html>`_, Measurement of associated production of a W boson and a charm quark in proton-proton collisions at 13 TeV :cite:`CMS:2018dxg`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2019_I1744604 <https://rivet.hepforge.org/analyses/CMS_2019_I1744604.html>`_, $t$-channel single top-quark differential cross sections and charge ratios at 13 TeV :cite:`CMS:2019jjp`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2021_I1901295 <https://rivet.hepforge.org/analyses/CMS_2021_I1901295.html>`_, Differential cross sections for top quark pair production in the full kinematic range using the lepton+jets final state in proton proton collisions at 13 TeV :cite:`CMS:2021vhb`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_13_TTHAD**  *fully hadronic top events* 

   * `CMS_2019_I1753720 <https://rivet.hepforge.org/analyses/CMS_2019_I1753720.html>`_, Measurement of the ttbb production cross section in the all-jet final state in proton-proton collisions at centre-of-mass energy of 13 TeV with 35.9 fb^-1 of data collected in 2016. :cite:`CMS:2019eih`.  SM theory predictions are available :doc:`here <SM/CMS_13_TTHAD/CMS_2019_I1753720>`.
   * `CMS_2019_I1764472 <https://rivet.hepforge.org/analyses/CMS_2019_I1764472.html>`_, Measurement of the differential ttbar production cross section as a function of the jet mass and top quark mass in boosted hadronic top quark decays. :cite:`CMS:2019fak`.  SM theory predictions are available :doc:`here <SM/CMS_13_TTHAD/CMS_2019_I1764472>`.

 Pool: **CMS_2_76_DJET**  *inclusive and Mueller Navelet dijet final states* 

   * `CMS_2021_I1963239 <https://rivet.hepforge.org/analyses/CMS_2021_I1963239.html>`_, Measurement of inclusive and Mueller-Navelet dijet cross sections and their ratios at 2.76 TeV :cite:`CMS:2021maw`.  SM theory predictions are available :doc:`here <SM/CMS_2_76_DJET/CMS_2021_I1963239>`.

 Pool: **CMS_7_GAMMA**  *inclusive (multi)photons* 

   * `CMS_2014_I1266056 <https://rivet.hepforge.org/analyses/CMS_2014_I1266056.html>`_, Photon + jets triple differential cross-section :cite:`CMS:2013myp`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_7_JETS**  *inclusive hadronic final states* 

   * `CMS_2014_I1298810 <https://rivet.hepforge.org/analyses/CMS_2014_I1298810.html>`_, Ratios of jet pT spectra, which relate to the ratios of inclusive, differential jet cross sections :cite:`CMS:2014nvq`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2013_I1273574 <https://rivet.hepforge.org/analyses/CMS_2013_I1273574.html>`_, Studies of 4-jet production in proton-proton collisions at $\sqrt{s} = 7$ TeV :cite:`CMS:2013slh`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2013_I1208923 <https://rivet.hepforge.org/analyses/CMS_2013_I1208923.html>`_, Jet-pT and dijet mass at sqrt(s) = 7 TeV :cite:`CMS:2012ftr`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2012_I1089835 <https://rivet.hepforge.org/analyses/CMS_2012_I1089835.html>`_, Inclusive b-jet production in pp collisions at 7 TeV :cite:`CMS:2012pgw`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_7_LLJET**  *dileptons at the Z pole, plus optional jets* 

   * `CMS_2015_I1310737 <https://rivet.hepforge.org/analyses/CMS_2015_I1310737.html>`_, Jet multiplicity and differential cross-sections of $Z$+jets events in $pp$ at $\sqrt{s} = 7$ TeV :cite:`CMS:2014bkk`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2013_I1224539:JMODE=Z <https://rivet.hepforge.org/analyses/CMS_2013_I1224539.html>`_, CMS jet mass measurement in $W, Z$ and dijet events :cite:`CMS:2013kfv`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2013_I1256943 <https://rivet.hepforge.org/analyses/CMS_2013_I1256943.html>`_, Cross-section and angular correlations in $Z$ boson with $b$-hadrons events at $\sqrt{s} = 7$ TeV :cite:`CMS:2013xck`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_7_LL_GAMMA**  *dimuons plus photon(s)* 

   * `CMS_2015_I1346843 <https://rivet.hepforge.org/analyses/CMS_2015_I1346843.html>`_, Measurement of differential cross-section of FSR photons in $Z$ decays :cite:`CMS:2015vap`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_7_LMETJET**  *electron, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)* 

   * `CMS_2013_I1224539:JMODE=W <https://rivet.hepforge.org/analyses/CMS_2013_I1224539.html>`_, CMS jet mass measurement in $W, Z$ and dijet events :cite:`CMS:2013kfv`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2014_I1303894 <https://rivet.hepforge.org/analyses/CMS_2014_I1303894.html>`_, Differential cross-section of $W$ bosons + jets in $pp$ collisions at $\sqrt{s}=7$ TeV :cite:`CMS:2014oon`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_8_3L**  *trileptons* 

   * `CMS_2016_I1487288 <https://rivet.hepforge.org/analyses/CMS_2016_I1487288.html>`_, $WZ$ production cross-section in $pp$ collisions at 8~TeV :cite:`CMS:2016qth`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_8_JETS**  *inclusive hadronic final states* 

   * `CMS_2016_I1487277 <https://rivet.hepforge.org/analyses/CMS_2016_I1487277.html>`_, Measurement and QCD analysis of double-differential inclusive jet cross sections in $pp$ collisions at $\sqrt{s} = 8$\,TeV  and cross-section ratios to 2.76 and 7 TeV :cite:`CMS:2016lna`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2017_I1598460 <https://rivet.hepforge.org/analyses/CMS_2017_I1598460.html>`_, Triple-differential dijet pT cross section and PDF constraints at 8 TeV :cite:`CMS:2017jfq`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2021_I1847230:MODE=QCD8TeV <https://rivet.hepforge.org/analyses/CMS_2021_I1847230.html>`_, Measurements of angular distance and momentum ratio distributions in three-jet and Z + two-jet final states in pp collisions at 8 and 13 TeV :cite:`CMS:2021hnp`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_8_LLJET**  *dileptons at the Z pole, plus optional jets* 

   * `CMS_2016_I1471281:VMODE=Z <https://rivet.hepforge.org/analyses/CMS_2016_I1471281.html>`_, Measurement of the transverse momentum spectra of weak vector bosons produced in proton-proton collisions at $\sqrt{s} = 8$ TeV :cite:`CMS:2016mwa`.  SM theory predictions are available :doc:`here <SM/CMS_8_LLJET/CMS_2016_I1471281:VMODE=Z>`.
   * `CMS_2017_I1499471 <https://rivet.hepforge.org/analyses/CMS_2017_I1499471.html>`_, Measurements of the associated production of a Z boson and b jets in pp collisions at $\sqrt{s} = 8$ TeV :cite:`CMS:2016gmz`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2021_I1847230:MODE=ZJet <https://rivet.hepforge.org/analyses/CMS_2021_I1847230.html>`_, Measurements of angular distance and momentum ratio distributions in three-jet and Z + two-jet final states in pp collisions at 8 and 13 TeV :cite:`CMS:2021hnp`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_8_LMETJET**  *lepton, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)* 

   * `CMS_2016_I1471281:VMODE=W <https://rivet.hepforge.org/analyses/CMS_2016_I1471281.html>`_, Measurement of the transverse momentum spectra of weak vector bosons produced in proton-proton collisions at $\sqrt{s} = 8$ TeV :cite:`CMS:2016mwa`.  SM theory predictions are available :doc:`here <SM/CMS_8_LMETJET/CMS_2016_I1471281:VMODE=W>`.
   * `CMS_2016_I1491953 <https://rivet.hepforge.org/analyses/CMS_2016_I1491953.html>`_, Differential cross sections for associated production of a W boson and jets at 8 TeV :cite:`CMS:2016sun`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2016_I1454211 <https://rivet.hepforge.org/analyses/CMS_2016_I1454211.html>`_, Boosted $t\bar{t}$ in $pp$ collisions at $\sqrt{s} = 8~\TeV$ :cite:`CMS:2016poo`. :red:`No SM theory predictions available for this analysis.` 
   * `CMS_2017_I1518399 <https://rivet.hepforge.org/analyses/CMS_2017_I1518399.html>`_, Differential $t\bar{t}$ production cross-section as a function of the leading jet mass for boosted top quarks at 8 TeV :cite:`CMS:2017pcy`.  SM theory predictions are available :doc:`here <SM/CMS_8_LMETJET/CMS_2017_I1518399>`.

 Pool: **LHCB_13_L1L2B**  *top pairs via e mu plus b* 

   * `LHCB_2018_I1662483 <https://rivet.hepforge.org/analyses/LHCB_2018_I1662483.html>`_, Measurement of the forward top pair production cross-section in the dilepton channel :cite:`LHCb:2018usb`.  SM theory predictions are available :doc:`here <SM/LHCB_13_L1L2B/LHCB_2018_I1662483>`.

 Pool: **LHCB_7_LLJET**  *dimuons at the Z pole, plus optional jets* 

   * `LHCB_2014_I1262703 <https://rivet.hepforge.org/analyses/LHCB_2014_I1262703.html>`_, Study of forward Z + jet production in $pp$ collisions at $\sqrt{s}=7$ TeV in the LHCb fiducial phase-space :cite:`LHCb:2013mpk`. :red:`No SM theory predictions available for this analysis.` 
   * `LHCB_2012_I1208102 <https://rivet.hepforge.org/analyses/LHCB_2012_I1208102.html>`_, Differential cross-sections of $\mathrm{Z}/\gamma^* \to e^{+}e^{-}$ vs rapidity and $\phi^*$ :cite:`LHCb:2012gii`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **LHCB_8_LJET**  *muons (aimed at W)* 

   * `LHCB_2016_I1454404:MODE=WJET <https://rivet.hepforge.org/analyses/LHCB_2016_I1454404.html>`_, Measurement of forward W and Z boson production in association with jets at LHCb :cite:`LHCb:2016nhs`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **LHCB_8_LLJET**  *dimuons at the Z pole, plus optional jets* 

   * `LHCB_2016_I1454404:MODE=ZJET <https://rivet.hepforge.org/analyses/LHCB_2016_I1454404.html>`_, Measurement of forward W and Z boson production in association with jets at LHCb :cite:`LHCb:2016nhs`. :red:`No SM theory predictions available for this analysis.` 

Ratio measurements
------------------ 

 *These typically use SM theory for the denominator, and may give unreliable results if your model contributes to both numerator and denominator. On by default, can be turned off via command-line.* 

 Pool: **ATLAS_13_METJET**  *missing transverse momentum plus jets* 

   * `ATLAS_2017_I1609448 <https://rivet.hepforge.org/analyses/ATLAS_2017_I1609448.html>`_, $p_\text{T}^\text{miss}$+jets cross-section ratios at 13 TeV :cite:`ATLAS:2017txd`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_METJET/ATLAS_2017_I1609448>`.
   * `ATLAS_2024_I2765017:TYPE=BSM <https://rivet.hepforge.org/analyses/ATLAS_2024_I2765017.html>`_, pTmiss+jets cross-sections and ratios at 13 TeV :cite:`ATLAS:2024vqf`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_METJET/ATLAS_2024_I2765017:TYPE=BSM>`.

Higgs to diphotons
------------------ 

 *Higgs to two photons use a data-driven background subtraction. If your model predicts non-resonant photon production this may lead to unreliable results. On by default, can be turned off via command-line.* 

 Pool: **ATLAS_13_GAMMA**  *inclusive (multi)photons* 

   * `ATLAS_2022_I2023464 <https://rivet.hepforge.org/analyses/ATLAS_2022_I2023464.html>`_, H->yy differentual cross-sections at 13 TeV :cite:`ATLAS:2022fnp`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_GAMMA/ATLAS_2022_I2023464>`.

 Pool: **ATLAS_8_GAMMA**  *inclusive (multi)photons* 

   * `ATLAS_2014_I1306615 <https://rivet.hepforge.org/analyses/ATLAS_2014_I1306615.html>`_, Higgs diphoton events at 8 TeV in ATLAS :cite:`ATLAS:2014yga`.  SM theory predictions are available :doc:`here <SM/ATLAS_8_GAMMA/ATLAS_2014_I1306615>`.

Searches
-------- 

 *Detector-level, using Rivet smearing functions. Off by default, can be turned on via command-line.*

 Pool: **ATLAS_13_HMDY**  *dileptons above the Z pole* 

   * `ATLAS_2019_I1725190 <https://rivet.hepforge.org/analyses/ATLAS_2019_I1725190.html>`_, Dilepton mass spectrum in 13 TeV pp collisions with 139/fb Run 2 dataset :cite:`ATLAS:2019erb`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_HMDY/ATLAS_2019_I1725190>`.

 Pool: **ATLAS_13_METJET**  *missing transverse momentum plus jets* 

   * `ATLAS_2016_I1458270 <https://rivet.hepforge.org/analyses/ATLAS_2016_I1458270.html>`_, 0-lepton SUSY search with 3.2/fb of 13 TeV $pp$ data :cite:`ATLAS:2016dwk`.  SM theory predictions are available :doc:`here <SM/ATLAS_13_METJET/ATLAS_2016_I1458270>`.

Neutrino Truth
-------------- 

 *Uses neutrino flavour truth info, may be misleading for BSM. Off by default, can be turned on via command-line.*

 Pool: **ATLAS_13_3L**  *trileptons* 

   * `ATLAS_2016_I1469071 <https://rivet.hepforge.org/analyses/ATLAS_2016_I1469071.html>`_, Measurement of the $WZ$ production cross section at 13 TeV :cite:`ATLAS:2016nlr`. :red:`No SM theory predictions available for this analysis.` 

Higgs to WW
----------- 

 *Typically involve large data-driven top background subtraction. If your model contributes to the background as well the results maybe unreliable. Off by default, can be turned on via command-line.* 

 Pool: **ATLAS_8_L1L2MET**  *WW analyses in dileptons plus missing transverse momentum channel* 

   * `ATLAS_2016_I1444991 <https://rivet.hepforge.org/analyses/ATLAS_2016_I1444991.html>`_, Higgs-to-WW differential cross sections at 8 TeV :cite:`ATLAS:2016vlf`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_8_L1L2MET**  *WW analyses in dileptons plus missing transverse momentum channel* 

   * `CMS_2017_I1467451 <https://rivet.hepforge.org/analyses/CMS_2017_I1467451.html>`_, Measurement of the transverse momentum spectrum of the Higgs boson produced in $pp$collisions at $\sqrt{s} = 8$ TeV using H to WW decays :cite:`CMS:2016ipg`.  SM theory predictions are available :doc:`here <SM/CMS_8_L1L2MET/CMS_2017_I1467451>`.

b-jet veto issue
---------------- 

 *The following measurements apply a detector-level b-jet veto which is not part of the particle-level fiducial definition and therefore not applied in Rivet. Also applies to CMS Higgs-to-WW analysis. Off by default, can be turned on via command-line, but use with care.* 

 Pool: **CMS_13_3LJET**  *dileptons or trileptons + MET* 

   * `CMS_2020_I1794169 <https://rivet.hepforge.org/analyses/CMS_2020_I1794169.html>`_, Measurements of production cross sections of WZ and same-sign WW boson pairs in association with two jets in proton-proton collisions at $\sqrt{s} =$ 13 TeV :cite:`CMS:2020gfh`.  SM theory predictions are available :doc:`here <SM/CMS_13_3LJET/CMS_2020_I1794169>`.

 Pool: **CMS_13_LMETJET**  *lepton, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)* 

   * `CMS_2017_I1610623 <https://rivet.hepforge.org/analyses/CMS_2017_I1610623.html>`_, Differential cross-sections for W boson and jets in proton-proton collisions at 13 TeV :cite:`CMS:2017gbl`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_7_LMETJET**  *electron, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)* 

   * `CMS_2014_I1303894 <https://rivet.hepforge.org/analyses/CMS_2014_I1303894.html>`_, Differential cross-section of $W$ bosons + jets in $pp$ collisions at $\sqrt{s}=7$ TeV :cite:`CMS:2014oon`. :red:`No SM theory predictions available for this analysis.` 

 Pool: **CMS_8_L1L2MET**  *WW analyses in dileptons plus missing transverse momentum channel* 

   * `CMS_2017_I1467451 <https://rivet.hepforge.org/analyses/CMS_2017_I1467451.html>`_, Measurement of the transverse momentum spectrum of the Higgs boson produced in $pp$collisions at $\sqrt{s} = 8$ TeV using H to WW decays :cite:`CMS:2016ipg`.  SM theory predictions are available :doc:`here <SM/CMS_8_L1L2MET/CMS_2017_I1467451>`.

 Pool: **CMS_8_LMETJET**  *lepton, missing transverse momentum, plus optional jets (typically W, semi-leptonic ttbar analyses)* 

   * `CMS_2016_I1491953 <https://rivet.hepforge.org/analyses/CMS_2016_I1491953.html>`_, Differential cross sections for associated production of a W boson and jets at 8 TeV :cite:`CMS:2016sun`. :red:`No SM theory predictions available for this analysis.` 
