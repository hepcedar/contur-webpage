Electroweak SUSY and Global Fits (2023)
=======================================

*GAMBIT Collaboration, and Tetiana Berger-Hrynova, Louie Corpe, Benjamin Fuks, Melissa van Beekveld, David Yallup, Tony Yue*

Contur is interfaced to GAMBIT, and was used in Collider constraints on electroweakinos in the presence of a light gravitino :cite:`GAMBIT:2023yih`.

Other results are discussed below.


See the contribution *Search and measurement complementarity* in the Les Houches 2019 proceedings :cite:`Brooijmans:2020yij`.

The Gambit allowed points

.. image:: images/slha_grid_plot_mC1_mN1__3sigma__40x40.png
            :scale: 40%

.. image:: images/slha_grid_plot_mN2_mN3__3sigma__20x20.png
            :scale: 40%

.. image:: images/slha_grid_plot_mN4_mN3__3sigma__20x20.png
            :scale: 40%

and the Contur disfavoured points with latest update:

The new analyses recently added to Rivet and Contur have increased the sensitivities on the points with higher masses as well
as the compressed and high-mass-difference regions in the :math:`\chi_1^0` and :math:`\chi_1^+` mass plane. 

.. image:: images2/slha_grid_plot_mC1_mN1__3sigma__40x40.png
            :scale: 50%

.. image:: images2/slha_grid_plot_mN2_mN3__3sigma__20x20.png
            :scale: 50%

.. image:: images2/slha_grid_plot_mN4_mN3__3sigma__20x20.png
            :scale: 50%

The contributions of the new analyses to the CL of the mass parameter spaces can be visualised by the following dominant pools plots:

.. image:: images2/slha_grid_plot_mC1_mN1__3sigma__40x40dominantPools.png
            :scale: 50%

.. image:: images2/slha_grid_plot_mN2_mN3__3sigma__20x20dominantPools.png
            :scale: 50%

.. image:: images2/slha_grid_plot_mN4_mN3__3sigma__20x20dominantPools.png
            :scale: 50%


It can be seen that the increase comes from several sources. The new four-lepton measurements (ATLAS_13_4L) the like-flavour dilepton+jet (ATLAS_8_LLJET),
and unlike flavour dilepton, missing energy (with and without jets) (ATLAS_13_L1L2 and ATLAS_13_L1L2JET) all have an impact.

The SLHA files for these parameter points are available `here <https://gitlab.com/hepcedar/contur/-/tree/main/Models/Gambit>`_



 

