Vector-like Leptons (2024)
==========================

Vector-like leptons (VLLs) are a well motivated extension to the Standard Model (SM) that appear in several theories of new physics.
They have been less-studied at the LHC that their close relations, Vector-like quarks, presumably because the production cross-sections
are generally smaller. At time of writing, most LHC studies have focussed on VLLs which couple only to the third
generation leptons of the SM. Here we also look at first generation couplings.

Four cases of a simple VLL model, partnered to the first or third generations of the SM, are considered, using measurements made in
13 TeV collisions which are available in Contur version 2.5.1. The studies are carried in the context of the Kumar and Martin model :cite:`Kumar:2015tna`,
using the implementation of :cite:`Bhattiprolu:2019vdu`.

We constrain values of VLL mass (:math:`M_{\tau'}`) and VLL mixing with SM leptons (:math:`\epsilon`).
For weak isodoublet VLLs partnered to the first generation, Contur excludes :math:`M_{\tau'}<850` GeV, greatly improving on
previous limits :cite:`L3:2001xsz,ATLAS:2015qoy`.
The rest of the exclusions derived by Contur are comparable to or less strict than existing limits.

The following heatmaps show the Contur constraints (black solid line). Additionally, the expected exclusion
(black dotted contours) and the expected :math:`2 \sigma` exclusion at the HL-LHC (red dotted line) are shown.
The solid and dashed lines represent the 95% and 68% exclusion respectively.
The HL-LHC exclusion is estimated as described in :cite:`Butterworth:2024eyr`.
Top row: singlets; bottom row, doublets.
Left column first generation coupling, right column, third generation.

|S1_DP| |S3_DP| |D1_DP| |D3_DP|

.. |S1_DP| image:: figures/S1_DP.png
    :width: 45%
    :alt: Singlet, first generation
	    
.. |S3_DP| image:: figures/S3_DP.png
    :width: 45%

.. |D1_DP| image:: figures/D1_DP.png
    :width: 45%
   
.. |D3_DP| image:: figures/D3_DP.png
    :width: 45%
   
The colouring in the heatmaps indicated which final states dominate the sensitivity at each point.
The pink, which provides most of the sensitivity, it four lepton final states, specifically the measurements
from ATLAS :cite:`ATLAS:2021kog`. The brown, which is the next most important contributor, is dilepton+photon
measurements :cite:`ATLAS:2019gey,ATLAS:2022wnf`.


For convenience, the model files are also available in the Contur installation in the
`VLL model directory <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/VLL>`_


*Contributors: Jon Butterworth, Emma Elkington*




