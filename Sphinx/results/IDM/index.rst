Inert Doublet Model (2024)
==========================


The Inert Doublet Model :cite:`PhysRevD.18.2574,Cao:2007rm,Barbieri:2006dq` is a simple two-Higgs doublet model with the
scalar potential 
 
.. math::
   \begin{array}{c}
   V=-\frac{1}{2}\left[m_{11}^{2}\left(\phi_{S}^{\dagger} \phi_{S}\right)+m_{22}^{2}\left(\phi_{D}^{\dagger} \phi_{D}\right)\right]+\frac{\lambda_{1}}{2}\left(\phi_{S}^{\dagger} \phi_{S}\right)^{2}+\frac{\lambda_{2}}{2}\left(\phi_{D}^{\dagger} \phi_{D}\right)^{2}+\lambda_{3}\left(\phi_{S}^{\dagger} \phi_{S}\right)\left(\phi_{D}^{\dagger} \phi_{D}\right) \\
   +\lambda_{4}\left(\phi_{S}^{\dagger} \phi_{D}\right)\left(\phi_{D}^{\dagger} \phi_{S}\right)+\frac{\lambda_{5}}{2}\left[\left(\phi_{S}^{\dagger} \phi_{D}\right)^{2}+\left(\phi_{D}^{\dagger} \phi_{S}\right)^{2}\right]
   \end{array}

that obeys a discrete :math:`Z_2` symmetry and provides a dark matter (DM) candidate :cite:`Kalinowski:2018ylg,Ilnicka:2015jba`. 

In this model, :math:`H^{0}` is considered to be the DM candidate, and only regions where it is the lightest new particle are
considered.

We use parameter values :math:`\lambda_{2} = 2`, :math:`\lambda_{L} = \frac{1}{2}(\lambda_{3}+\lambda_{4}+\lambda_{5}) = 0.005`,
:math:`m_{H^{\pm}}=m_{A^0}+20` GeV, and scan :math:`m_{H^{0}}` and :math:`m_{A^{0}}` over the range [50, 500] GeV
in which we anticipate the highest likely LHC sensitivity.
In Ref :cite:`Kalinowski:2018ylg`, it was shown that larger :math:`\lambda` values and larger mass splittings
between :math:`A^{0}` and the charged Higgs are excluded by electroweak precision observables and theory
considerations such as perturbativity, unitarity and positivitity.
      
A 10x10 grid is used, with 30,000 simulated events for each grid point. The regions where :math:`H^{0}` is not the lightest particle are excluded.
 
.. image:: images/p_p__to__H0_H0.png
      :scale: 40%
.. image:: images/p_p__to__H0_Hpm.png
      :scale: 40%
.. image:: images/cbar.png
      :scale: 50%
.. image:: images/p_p__to__A0_H0.png
      :scale: 40%
.. image:: images/p_p__to__A0_A0.png
      :scale: 40%
.. image:: images/cbar.png
      :scale: 50%

The figures above show some of the dominant cross sections at each point according to the Herwig event generator.  
The lower half of the plane, coloured in orange, is not considered. White sections of the plots indicate regions where the cross-section was not sampled because it was negligible compared to other processes.  The largest cross-section is for low-mass :math:`m_{H^{0}}` pair-production, 
shown in the upper-left figure. In this case the DM candidate is  produced with no other SM objects, and would therefore be a challenging missing-transverse-energy (:math:`E_T^{\rm miss}`) -only signature.
In other parts of the plane, :math:`H^{0}` may also be produced with other new particles such as :math:`A^{0}`, or :math:`H^{\pm}`, 
or pair-production of these other BSM particles may occur.
These BSM particles then decay back to a :math:`H^{0}` in association with a SM vector boson, which follow their normal decays to quarks or leptons. For instance, pair-produced :math:`A^{0}` particles may
decay to pairs of :math:`H^{0}` and two SM Z bosons.

The figures below show the results of propagating these production and decay processes to final states
which could potentially be observed at the LHC.
The final states with the highest cross-sections are shown.
Although the :math:`E_T^{\rm miss}`-only signature would have the highest cross-sections,
it is experimentally very challenging to analyse this final state at the LHC. 
The signatures more easily observable at the LHC would come from :math:`H^{0}` production associated with jets, leading 
to :math:`E_T^{\rm miss}` +jets final states.
Signatures with one or more leptons with :math:`E_T^{\rm miss}` 
and jets may also play a role.

.. image:: images/nLeptons0_MET.png
      :scale: 50%
.. image:: images/nLeptons0_nJets2_MET.png
      :scale: 50%
.. image:: images/nLeptons1_MET.png
      :scale: 50%
.. image:: images/cbar.png
      :scale: 50%

The heatmap below shows the results of applying the CONTUR method to this model,
using all currently-available LHC data preserved in Rivet (7, 8 and 13 TeV runs in Rivet as of 27/05/2024, including
a preliminary version of the full run 2 :math:`p_T^{\rm miss}` +jets measurement from ATLAS :cite:`ATLAS:2024vqf`.

.. image:: images/combinedOverlay.png
      :scale: 90%
.. image:: images/combinedMeshcbar.png
      :scale: 100%

 
As expected from the cross section plots, there is very little sensitivity, with only a small amount of exclusion at about the
40-60% level showing at low :math:`m_{A^0}`. The dotted red line indicates the estimated boundary of 95% sensitivity at the HL-LHC,
obtained by scaling the experimental uncertainties of 13 TeV data by the square root of the ratio of the integrated luminosity
used in the measurement to the HL-LHC target of 3/ab. The small area to the lower right of the line is potentially within reach.

The colours in the pot below indicate the signatures which give the most sensitivity.
While many signatures involving leptons appear, in the region where the HL-LHC has a chance
of accessing the model, the :math:`p_T^{\rm miss}` +jets analysis and :math:`p_T^{\rm miss}` +lepton analysis
are the most sensitive.

.. image:: images/dominantPools0.png
      :scale: 60%
 
The Inert Doublet Model is thus very challenging to indentify, as it appears to evade many of the measurements
made at the LHC thus far.

The UFO files come from the Feynrules model library https://feynrules.irmp.ucl.ac.be/wiki/InertDoublet
and can also be found in the InertDoublet_UFO directory here: https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/DM

Events are generated with Herwig7 :cite:`Bellm:2015jjp`, including all 2-to-2 processes involving a beyond-the-SM (BSM) particle in the matrix-element or an outgoing leg.

*Contributors: Jon Butterworth, Louie Corpe, Tania Robens, Haoyu Wang, Gustavs Zilgalvis*
