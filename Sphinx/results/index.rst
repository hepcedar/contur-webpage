Results
=======

Results at various stages of maturity, the dates reflect the date of the most recent update. 
Models may of course appear in more than one category.


Dark Matter Models
------------------

.. toctree::
   :maxdepth: 1

   DMsimp_s_spin1/index
   LLP/index	      
   HeavyDarkMesons/index
   Pseudoscalar_2HDM/index
   IDM/index
   LeptoBaryons/index
   DMsimp_t/index
   DM_HF/index
   DM_LF/index

   
:math:`Z^\prime` Models
-----------------------

.. toctree::
   :maxdepth: 1

   LeptoBaryons/index
   BL3/index
   ZPBAnom/index
   TopColour/index


Light Scalars
-------------

.. toctree::
   :maxdepth: 1

   LLP/index
   lightscalarLH/index
   DarkEnergy/index

Extended Higgs Sector
---------------------

.. toctree::
   :maxdepth: 1

   Pseudoscalar_2HDM/index
   G-W/index
   IDM/index
   LeptoBaryons/index
   SigmaSM/index
   TypeIISeesaw/index
   BL3/index
   2HDMLH/index


Heavy Neutral Leptons
---------------------

.. toctree::
   :maxdepth: 2

   HeavyN/index
   LLP/index
   TypeIISeesaw/index
   BL3/index
   NeutrinoEFT/index

Vector-like Fermions
--------------------

.. toctree::
   :maxdepth: 1

   VLL/index
   CompH/index
   vlq/index
   VQ_LQ_LL/index

Supersymmetry
-------------

.. toctree::
   :maxdepth: 1

   gambit_susy/index
   Comparison_to_ATLAS_SUSY_summaries/index
   rpvsusy/index

Long-lived Particles
--------------------

.. toctree::
   :maxdepth: 1

   LLP/index

Axion-like Particles
--------------------

.. toctree::
   :maxdepth: 1

   LLP/index
