Leptophobic Top-Colour (2021)
=============================

*Mohammad Mahdi Altakach, Jon Butterworth, Tomas Ježo,  Michael Klasen, Ingo Schienbein*

See 
**Probing a leptophobic top-colour model with cross section measurements and precise signal and background predictions: a case study**
:cite:`Altakach:2021lkq` 

For convenience, the model files are also available in the Contur installation in the 
TopColour directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/>`_







