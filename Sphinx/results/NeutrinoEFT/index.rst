Higgs phenomenology as a probe of sterile neutrinos (2019)
==========================================================

*Jon Butterworth, Michael Chala, Christoph Englert, Michael Spannowsky, Arsenii Titov*

See reference :cite:`Butterworth:2019iff`

.. image:: images/combinedHybrid-ScanB.png
            :scale: 100%

The model files are available in the Neutrino_EFT_maj_UFO directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/NeutrinoEFT>`_ .

*This research was supported by te Munich Institute for Astro- and Particle Physics (MIAPP) of the 
DFG cluster of excellence "Origin and Structure of the Universe (2019)*

