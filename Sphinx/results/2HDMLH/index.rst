Two Higgs-doublet model with enhanced decays to muons (2017)
============================================================

*Jon Butterworth, David Grellscheid, Ken Lane, Kristin Lohwasser, Lucas Pritchett*

See the contribution *Death and the Model: a Contur case study* in
the Les Houches 2017 proceedings :cite:`Brooijmans:2018xbu`.

The model files are available `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/2HDM/KL-LH2017>`_





 

