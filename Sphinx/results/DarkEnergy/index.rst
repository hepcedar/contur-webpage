Scalar Dark Energy Field coupling to the SM (2019)
==================================================

*Jon Butterworth, Christoph Englert, Peter Richardson, Michael Spannowsky* 

A study of the model which is introduced and discussed by Brax, Burrage, Englert and Spannowsky in :cite:`Brax:2016did`.
A neutral scalar dark energy field of mass :math:`M_\phi` couples to Standard Model particles via various Effective Field 
Theory (EFT) operators which are suppressed by powers of a scale parameter :math:`M_{SCALE}`.

Following :cite:`Brax:2016did`, we concentrate on the couplings :math:`C_1` and :math:`C_2` which appear in front
on the leading EFT operators, setting the others to zero. This means that :math:`\phi` is pair-produced and stable, so the dominant
signatures are expected to involve missing transverse energy.

First setting :math:`C_1 = C_2 = 1`, we scan in :math:`M_\phi` and :math:`M_{SCALE}`.

Heatmap and contour for all available 13 TeV data in Rivet 2.7 as of 4/6/2019:

.. image:: images/combinedHybrid-mphi.png
	    :scale: 100%

The cut-off in sensitivity is independent of :math:`M_\phi` over this range, at :math:`M_{SCALE} \approx 1` TeV, slightly higher than the 820 GeV
or so estimated in :cite:`Brax:2016did` for :math:`C_2 = 1` using 8 TeV monojet data. In the current scan the most sensitive measurement is 
the ATLAS missing energy ratio :cite:`ATLAS:2017txd`, as expected.

We then set :math:`M_\phi = 0.1` GeV, the nominal value chosen in :cite:`Brax:2016did`, and setting :math:`C_2 = 1 - C_1`, we scan in :math:`C_1` and 
:math:`M_{SCALE}`.

Heatmap and contour for all available 7,8 and 13 TeV data in Rivet 2.7 as of 4/6/2019:

.. image:: images/combinedHybrid-c1.png
	    :scale: 100%

Again the results are comparable to :cite:`Brax:2016did`, with a cut off in sensitivity at around 200 GeV for :math:`C_1 = 1, C_2 = 0` and 
around 1 TeV for :math:`C_1 = 0, C_2 = 1`. And again, the missing energy measurement drives the sensitivity, although many other measurements
have sensitivity up :math:`M_{SCALE}` of a few 100 GeV.

This model was also studied in :cite:`Aaboud:2019yqu`. 

The model files are available in the directory Standard_Model_cosmo_UFO `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/DE>`_ .

*This research was supported by the Munich Institute for Astro- and Particle Physics (MIAPP) of the 
DFG cluster of excellence "Origin and Structure of the Universe (2019)*
