Comparison to ATLAS SUSY summaries (2021)
=========================================

*Jon Butterworth, Luzhan (Tony) Yue*

Some of the plots in the latest ATLAS SUSY summary documents :cite:`ATL-COM-PHYS-2021-331` were reproduced with Contur, and the results are shown in this page.

In what follows, the "dominant pool" plots from Contur shows the pools which make the biggest contribution to the exclusion
at each SUSY parameter points are shown first, followed by the ATLAS SUSY plots for comparison.

The Contur events are (as usual) generated using Herwig, with the SLHA files provided by the ATLAS analyses. (ATLAS in general
uses MadGraph for these studies.)


**Figure 1 in the SUSY summary document:**

.. image:: image/SUSY_contur_plots/1/dominantPools0-1.png
            :Scale: 35%

.. image:: image/SUSY_plots/fig_01/fig_01-1.png
            :Scale: 30%

Two decay processes from the pair production of :math:`\widetilde{g}` were investigated individually with 100\% branching ratio. The magenta and the dark violet contour lines on the contur plot are showing the 0.68 (dashed) and 0.95 (solid) confidence level limits for :math:`\widetilde{g}\rightarrow t\bar{t}\widetilde\chi_1^0` and :math:`\widetilde{g}\rightarrow b\bar{b}\widetilde\chi_1^0` processes respectively, with the background being the dominant pool plot for :math:`\widetilde{g}\rightarrow t\bar{t}\widetilde\chi_1^0`. Both processes have assumed an on-shell intermediate stop or sbottom quark with mass :math:`0.5(M(\widetilde\chi_1^0)+M(\widetilde{g}))` (as described in the plot caption for figure 1) which decays to a :math:`\widetilde\chi_1^0` and a corresponding same-flavour SM quark. Since all other processes have multiple final states possiblilities, the BR ratio for each set of probable final states would need to be specifically specified. The currently possessed SLHA files do not support this criteria, and therefore these processes are not studied. This situation apllies especially for the yellow contour where a new LSP (graviton) was introduced.

.. image:: image/SUSY_contur_plots/1/dominantPools0_bb-1.png
            :scale: 30%


This plot is showing the same contours but with the background changed to the dominant pool plot for
:math:`\widetilde{g}\rightarrow b\bar{b}\widetilde\chi_1^0` since different pools have contributed.
           
**Figure 3 in the SUSY summary document:**

.. image:: image/SUSY_contur_plots/3/dominantPools0-1.png
            :Scale: 35%

.. image:: image/SUSY_plots/fig_03/fig_03-1.png
            :Scale: 30%

Two :math:`\widetilde{g}s` were produced with each :math:`\widetilde{g}` decaying via the process :math:`\widetilde{g}\rightarrow t\bar{t}\widetilde\chi^0_1`. The Branching Ratio (BR) was set to 1 for this :math:`\widetilde{g}` three-body decay, which is mediated via an off-shell stop quark for each :math:`\widetilde{g}` produced. The final states for the process are a top quark pair (which then decay according to the SM) and a lightest neutralino.



**Figure 6 in the SUSY summary document:**

.. image:: image/SUSY_contur_plots/6/dominantPools0-1.png
            :Scale: 30%

.. image:: image/SUSY_plots/fig_06/fig_06-1.png
            :Scale: 30%

In this case, the process studied is pair production of stop quarks. Three decay modes of the stop quark were considered separately,
with the BR to each in turn set to 100\%. The Contur limits have been overlaid onto the dominant pool plot
for :math:`\widetilde{t_1}\rightarrow W + b + \widetilde\chi_1^0` in the left diagram: 

Pair production of stop quarks was the process monitored and three decay modes of stop quark were considered separately with 100% BR.

The contour limits were overlaid onto the dominant pool plot for :math:`\widetilde{t_1}\rightarrow W + b + \widetilde\chi_1^0` on the left diagram

  1. (Green) :math:`\widetilde{t_1}\rightarrow t + \widetilde\chi_1^0` with the light blue lines showing the interpolated data.

  2. (Purple)  :math:`1\sigma` limit for :math:`\widetilde{t_1}\rightarrow W + b + \widetilde\chi_1^0` (3-body decay for :math:`m(\widetilde{t})<m(t)+m(\widetilde\chi_1^0))`.

The :math:`\widetilde{t_1}\rightarrow f+{f}^{\prime}+b+\widetilde\chi_1^0` (4-body decay) was not yet studied in Contur, 
since the SLHA does not specify the required SUSY particle BRs.


**Figure 8 in the SUSY summary document:**

.. image:: image/SUSY_contur_plots/8/dominantPools0-1.png
            :Scale: 35%

.. image:: image/SUSY_plots/fig_08/fig_08-1.png
            :Scale: 30%

Among all the plotted processes in figure 8, the pair productions of stop and sbottom quarks were considered for this contur run. Due to the SLHA file availability restriction, the only two decay channel sets considered were :math:`\widetilde{t_1}\rightarrow b\widetilde\chi_1^\pm,t\chi_{1,2}^0` and :math:`\widetilde\chi_1^\pm\rightarrow W\widetilde\chi_1^0` since all other processes requires further information to adjust the input SLHA file. At the same time, although the SLHA file used do allow the :math:`\widetilde{t_1}` particle to decay through channels other than the ones listed above, the sum of the BR of these extra channels are not over 1\% and therefore kept to avoid over assumption.



**Figure 11 in the SUSY summary document:**

.. image:: image/SUSY_contur_plots/11/dominantPools0-1.png
            :Scale: 35%

.. image:: image/SUSY_plots/fig_11/fig_11-1.png
            :Scale: 35%

Only the combination of either :math:`\widetilde\chi_1^+\widetilde\chi_1^-` or :math:`\widetilde\chi_1^\pm\widetilde\chi_0^2` productions at 13 and 8 TeV were considered which then proceed with :math:`\widetilde{l}`-mediated decays (mass :math:`\widetilde\chi_1^\pm` = :math:`\widetilde\chi_0^2`). Since the masses of all :math:`\widetilde{l}` were set to the same value and varied as 0.5(:math:`m(\widetilde\chi_1^0)+m(\widetilde\chi_2^0))` at each parameter point, the BR were equally divided for all decay modes. The final states are two leptons and a LSP(:math:`\widetilde\chi_0^1`)



**Figure 12 in the SUSY summary document:**

.. image:: image/SUSY_contur_plots/12/dominantPools0-1.png
            :Scale: 35%

.. image:: image/SUSY_plots/fig_12/fig_12-1.png
            :Scale: 35%

Only the combination of either :math:`\widetilde\chi_1^+\widetilde\chi_1^-` or :math:`\widetilde\chi_1^\pm\widetilde\chi_0^2` productions were considered which then proceed with SM-boson-mediated decays (mass :math:`\widetilde\chi_1^\pm` = :math:`\widetilde\chi_0^2`). The BR of :math:`\widetilde\chi_1^\pm` and :math:`\widetilde\chi_0^2` decay to a SM-boson and a :math:`\widetilde\chi_0^1` were set to 1. The final states are two leptons and a LSP(:math:`\widetilde\chi_0^1`).



**Figure 13 in the SUSY summary document:**

.. image:: image/SUSY_contur_plots/13/dominantPools0-1.png
            :Scale: 35%

.. image:: image/SUSY_plots/fig_13/fig_13-1.png
            :Scale: 35%

The plot shows the CL exclusion on :math:`\widetilde\chi_1^\pm\widetilde\chi_0^2` production, but this time with an extra process allowed :math:`\widetilde\chi_2^0\rightarrow\widetilde\chi_1^1h` where h is the SM-like Higgs boson.


**Figure 14 in the SUSY summary document:**

.. image:: image/SUSY_contur_plots/14/dominantPools0-1.png
            :Scale: 35%

.. image:: image/SUSY_plots/fig_14/fig_14-1.png
            :Scale: 35%

The plot shows the CL exclusion on the production of sleptons which then decay to a lepton and a neutralino as final states (:math:`\widetilde{l}\rightarrow{l}\widetilde\chi_1^0`) with 100\% BR for each slepton generation. All sleptons were set to the same mass so that the total BR were evenly divided by the decay modes.



**Figure 15 in the SUSY summary document:**

.. image:: image/SUSY_contur_plots/15/dominantPools0-1.png
            :Scale: 35%

.. image:: image/SUSY_plots/fig_15/fig_15-1.png
            :Scale: 35%

The plot shows the CL exclusion specifically for the direct production of smuons which each decay to a muon and a neutralino as final states (:math:`\widetilde\mu\rightarrow\mu\chi_1^0`) with 100\% BR. regions are compatible with the observed g-2 anomaly(arXiv:2104.03281) at the :math:`\pm1\sigma` level, which were indicated in the original SUSY plot.


**Figure 16 in the SUSY summary document:**

.. image:: image/SUSY_contur_plots/16/dominantPools0-1.png
            :Scale: 35%

.. image:: image/SUSY_plots/fig_16/fig_16-1.png
            :Scale: 35%

The parameters and monitored processes are the same as the previous plot (figure 15 in SUSY document), however, the plot used the mass difference between :math:`\widetilde\mu{_L,_R}` and :math:`\widetilde\chi_1^0` as y-axis instead.


**Figure 17 in the SUSY summary document:**

.. image:: image/SUSY_contur_plots/17/dominantPools0-1.png
            :Scale: 35%

.. image:: image/SUSY_plots/fig_17/fig_17-1.png
            :Scale: 50%

The plot shows the CL exclusion for production of :math:`\widetilde\chi_1^+\widetilde\chi_1^-`, :math:`\widetilde\chi_1^\pm\widetilde\chi_1^0`, :math:`\widetilde\chi_1^\pm\widetilde\chi_2^0`, and :math:`\widetilde\chi_1^0\widetilde\chi_2^0` with off-shell SM-boson-mediated decays to the LSP(:math:`\widetilde\chi_1^0`). The off-shell criteria was achieved by setting 3-body decays to two leptons/quarks and a :math:`\widetilde\chi_1^0`. The usual BR for W and Z bosons were used for the lepton final states: :math:`W\rightarrow{e}\nu_e(0.1046),\mu\nu_{\mu}(0.1050),\tau\nu_{\tau}(0.1075)`. The remaining BR were divided roughly evenly for the final states :math:`u\bar{d}` and :math:`c\bar{s}` with the latter being very slight bigger due to the small difference in CKM matrix parameters on the diagonal. For Z boson, the total BR were evenly divided between the 3 opposite charged lepton pair final states. The blank part in the middle of the contur plot is the region where W production is not feasible.
































 
