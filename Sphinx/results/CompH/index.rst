Composite Higgs scenarios and vector-like quarks (2022)
=======================================================

*A. Banerjee et al*

See **Phenomenological aspects of composite Higgs scenarios: exotic scalars and vector-like quarks** :cite:`Banerjee:2022xmu`.





