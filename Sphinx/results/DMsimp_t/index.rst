DM with a t-channel mediator (2022)
===================================

*Jon Butterworth, Charlie Velasquez, Yoran Yeh*


The model files are available in the DMsimp_t directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/DM>`_.

Model features
---------------

The DMSimpt model is an implementation of the t-channel DM model in Feynrules :cite:`Arina:2020udz`. The UFO models can be used to generate BSM events with for example MadGraph5_aMC@NLO. As illustrated in the paper, the t-channel DM model is characterised by a DM particle coupling to a SM quark and a DM mediator. This allows a typical 2 to scattering diagram where two quarks interact and produce two DM particles (see the Feynman diagram below).

.. image:: images/intro/tchan_diagram.png
        :scale: 10%
		
Two quarks decaying into two DM particles X mediated by particle Y. Image from :cite:`Arina:2020udz`.

Different configurations of this model vary in the following ways. First, the DM particles can carry a spin of 0, 1/2 or 1. Second, the DM particles can assumed to be self-conjugate or not. Finally, one can make assumptions about which SM quarks the DM particles couple to. This webpage shows results of a few different model configurations. By default, the package comes with three possible sets of couplings to the SM:

* To all quarks ("uni")
* To quarks of the third generation ("3rd")
* To right-handed up-quarks ("uR")

For each of these setups, the model assumes three free parameters: the DM mass, the mediator mass and the coupling strength. In order to conserve the quantum numbers in an interaction, every SM quark has a unique DM mediator. 
As the *uR* configuration considers only one SM quark to couple to DM particles , there is only one type of mediator and one coupling.
In the case of the *uni* and *3rd* setups, the UFO model provides a separate mediator and coupling for every quark. These are assumed to be degenerate, i.e. all mediators have the same mass and all SM quarks couple equally strong to DM particles.

..
  Add info for other decay modes ; p p > Y Y, p p > X Y?

S3D_uR
---------------

In the S3D_uR configuration, non self-conjugate DM particles with spin 1/2 couple only to right-handed up-quarks. An example MG5 script is provided in the DMsimp_t directory on the Contur Gitlab page.
A CMS search in events with large missing transverse momentum and jets :cite:`CMS:2021far` obtained limits in the DM mass vs. mediator mass plane. This search used 137/fb of data and the coupling strength was set equal to 1.0. 

.. image:: images/S3D_uR/cms.png

This excludes the mediator mass up to 1.5 TeV at 95% confidence level for low DM masses.

Contur generates the heatmap below with an exclusion 

.. image:: images/S3D_uR/dominantPools0.png

(*Contur 2.3.x, Rivet 3.1.6, MadGraph5_aMC@NLO 2.9.10, Aug 2022*)

In this heatmap, the dotted line indicates the expected region of 95% exclusion, using the Standard Model (SM) predictions and the data uncertainties. The solid line delineates the actual 95% exclusion region, calculated using the SM as the background. The dashed line is the same as the solid line but delineates the 68% (2-sigma) exclusion region.
The 95% observed limit excluded the mediator mass up to around 800 GeV for low DM masses. The exclusion is driven by the ATLAS missing energy plus jets (MET+X) measurement. The fact that the exclusion from Contur is lower than the CMS search, can be understood because the ATLAS MET+X measurement uses 3.2/fb. The MET+X analysis measures a ratio between missing energy plus jets and a a lepton region, hence the SM prediction is used in Contur and the "data as background" contour is not shown. The expected and observed region of exclusion are comparable, but the slight difference can be explained as the SM prediction sometimes undershoots the data.

..
  S3D_3rd
  -------
  
  S3D_uni
  ---------------
  
  * Link to model files
  * First results


The model files are available in the DMsimp_t directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/DM>`_

