Non-standard VLQ and LQ signatures from cascade decays (2020)
=============================================================

*Jon Butterworth, Giacomo Cacciapaglia, Louie Corpe, Thomas Flacke, Benjamin Fuks, Luca Panizzi, Werner Porod, David Yallup*

See *Non-standard VLQ and LQ signatures from cascade decays* in :cite:`Brooijmans:2020yij` .

.. The model files will available in the  directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/2HDM>`_

 

