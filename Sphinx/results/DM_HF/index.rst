Vector Mediator coupling only to quarks, Majorana Dark Matter (2021)
====================================================================

*Jon Butterworth, David Grellscheid, Michael Krämer, David Yallup*

This is a small generalisation of the model analysed in the 'white paper' :cite:`Butterworth:2016sqg`. 
It is a simplified model with a dark matter Majorana fermion, :math:`\psi`, which interacts with the SM through a new vector particle, :math:`Z^\prime`.
The couplings of the mediator :math:`Z^\prime` to the dark matter :math:`\psi` and to the SM are specified as

.. math::

   \begin{aligned}
   \label{eq:vector_mediator}
    {\cal L} \supset  {{g_{\rm DM}}}\,\overline{\psi} \gamma_{\mu}\gamma_5 \psi\,Z'^{\mu} + {{g_{q}}}\sum_{q} \bar q \gamma_{\mu} q \,Z'^{\mu} \,,\end{aligned}

where the sum in the second term now runs over all three generations SM quarks, :math:`q \in \{u,d\}`. Again, the model 
has only four free parameters - two couplings and two masses: :math:`g_{\rm DM}`, :math:`g_{q}`,
:math:`M_\psi \equiv {{M_{\rm DM}}}`, and :math:`M_{Z^{\prime}}`. The width of the mediator, :math:`\Gamma_{Z'}`,
is determined by these four parameters. The differences with the 1st-generation only model will be:

* Slightly higher production cross section for the :math:`Z^{\prime}` due to the strange, charm and bottom content of the proton
* Higher BR for :math:`Z^{\prime}` to decay back to quarks rather than DM, for any given :math:`g_{\rm DM}`.
* For high enough :math:`M_{Z^{\prime}}`, top quark decays open up, with their distinctive final states.

Some first results were discussed in :cite:`Butterworth:2019wnt`.  

We scan a range in plausible mediator masses
(:math:`M_{Z^{\prime}}`) and dark matter masses (:math:`M_{\rm DM}`) within this model the common benchmark choice of coupling:
:math:`{{g_{q}}}= 0.25, {{g_{\rm DM}}}= 1`: 
	 
.. figure:: images/dominantPools0_corr.png
            :scale: 80%

(*Last updated Contur 2.0.x, Rivet 3.1.4, Herwig 7.2,  23/05/2021, using correlation information where available*)
The purple line indicates the perturbative unitarity constraint.
Note: the lowest mass point generated is :math:`M_{\rm Z^\prime}= 10` GeV, so the limit does not really extend to zero. For a
zoom on the low mass region in a very similar model, see :doc:`../DMsimp_s_spin1/index`.

The measurements have more sensitivity to this model for a given coupling than the 1st-generation-only one 
The main effects are the increased cross section and BR to jets, and the presence of top quarks and the inclusion of several top 
measurements from ATLAS and CMS. Again, more details are shown with a similar model :doc:`here <../DMsimp_s_spin1/index>`.

Note that a dedicated study using ATLAS :math:`t\bar{t}` measurements to constrain top production via *scalar* resonances is presented 
in :cite:`BuarqueFranzosi:2017qlm`.

The model files are available in the DM_vector_mediator_HF_UFO directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/Models/DM>`_

