Long-lived Particles (2024)
===========================

While the long-lived particle signature itself is not something the measurements in Contur work well with, that fraction of
events in which the particles with long half-lifes nevertheless decay promptly can still provide sensitivity, as shown for the
first time in *Probing exotic long-lived particles from the prompt side using the CONTUR method* :cite:`Corpe:2024ntq` for the
cases of feebly-interacting dark matter, hidden sector models mediated by a heavy neutral scalar,
dark photon models and a model featuring photo-phobic axion-like particles.

*Contributors: Louie Corpe, Andreas Goudelis, Simon Jeannot, Si Hyun Jeon*



 

