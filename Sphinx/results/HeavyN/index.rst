Heavy RH Majorana Neutrinos coupled via mixing (2024)
=====================================================

*Jon Butterworth, Joe Egan*

A study of a model :cite:`Atre:2009rg` with three Right-Handed heavy Majorana neutrinos which couple in to the SM only by mixing with the active neutrinos.
The UFO files came from `the Feynrules model library <http://feynrules.irmp.ucl.ac.be/wiki/HeavyN>`_ (see :cite:`Alva:2014gxa,Degrande:2016aje`).

There are in principle three heavy neutrinos in this model, but we follow :cite:`Alva:2014gxa` and set two to very high masses, effectively decoupling them. 
We also set the mixing to the second and third generation SM neutrinos to zero. The 
remaining parameters of the model are then the heavy neutrino mass :math:`M_{\nu H}` and its mixing to electron neutrinos :math:`V_{eN}`\. 

The sensitivities derived from multiple measured distributions are
combined into heatmaps which delineate the sensitivity of the SM measurements to this model in the parameter space of
:math:`V_{eN}`\ and :math:`M_{\nu H}`. In the left contour plot, solid and dashed black lines represent the excluded regions at :math:`CL_s` values of 95% and 68% respectively.
The dotted black line shows the expected sensitivity in the case that measurements were equal to the SM predictions, and the dotted red line is an estimate of the expected sensitivity at the HL-LHC.
The right plot shows the grid of :math:`CL_s` values from which the interpolated contours were derived.

Heatmap and contour for all available data (measurements from 7, 8 and 13 TeV runs in Rivet as of August 2024)

.. image:: images/combinedHybrid.png
	    :scale: 25%

Sensitivity is seen for mixing values :math:`|V_{eN}|^2 \gtrsim 3 \times 10^{-2}` up to :math:`M_{\nu H} ~ 100` GeV, which decreases at higher HNL masses.
In the low mass, small coupling region in the bottom left hand corner of the plot, the expected limit becomes stronger than the observed limit.
In this region, the BSM+SM signal is less disfavoured by comparison to experimental data than by comparison to the SM prediction.

The plot below shows the exclusion contours overlayed on the analysis pool that contributed the most to the exclusion at that point.

.. image:: images/dominantPools0.png
	    :scale: 25%

In the red region, sensitivity comes from a measurement of the fiducial three lepton cross section, shown below.

.. image:: images/ATLAS_2016_I1492320_3L_d01-x01-y01.png
	    :scale: 100%

In the low mass region, there is also sensitivity from differential lepton measurements, particularly in the low :math:`p_T` bins.

.. list-table::
   :widths: 50 50
   :header-rows: 0

   * - .. image:: images/ATLAS_2019_I1759875_d01-x01-y01.png
         :width: 100%

     - .. image:: images/ATLAS_2019_I1759875_d03-x01-y01.png
         :width: 100%


Note that these results currently do not include the photon-induced processes (considered in :cite:`Alva:2014gxa`), which 
generally contribute around 20% to the cross-section, with somewhat different final state kinematics.

The model files are available in the SM_HeavyN_NLO directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/HeavyN>`_ .
