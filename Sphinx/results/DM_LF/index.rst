Vector Mediator coupling only to 1st generation quarks, Majorana Dark Matter (2019)
===================================================================================

*Jon Butterworth, David Grellscheid, Michael Krämer, Bjorn Sarrazin, David Yallup*

This is the model discussed in the 'white paper' :cite:`Butterworth:2016sqg`. 
Some later results are also discussed in :cite:`Butterworth:2019wnt`. 
It is a simplified model with a
dark matter Majorana fermion, :math:`\psi`, which interacts with the SM through a new vector particle, :math:`Z^\prime`.
The couplings of the mediator :math:`Z^\prime` to the dark matter :math:`\psi` and to the SM are specified as

.. math::

   \begin{aligned}
   \label{eq:vector_mediator}
    {\cal L} \supset  {{g_{\rm DM}}}\,\overline{\psi} \gamma_{\mu}\gamma_5 \psi\,Z'^{\mu} + {{g_{q}}}\sum_{q} \bar q \gamma_{\mu} q \,Z'^{\mu} \,,\end{aligned}

where the sum in the second term includes only the first generation SM quarks, :math:`q \in \{u,d\}`. The model 
has only four free parameters - two couplings and two masses: :math:`g_{\rm DM}`, :math:`g_{q}`,
:math:`M_\psi \equiv {{M_{\rm DM}}}`, and :math:`M_{Z^{\prime}}`. The width of the mediator, :math:`\Gamma_{Z'}`,
is determined by these four parameters.

Following Ref. :cite:`Kahlhoefer:2015bea` the mediator couples to dark matter and to the SM quarks through an
axial-vector and vector current, respectively. An axial-vector coupling of the mediator to dark matter leads to spin-dependent dark
matter-nucleon interactions and thus weaker bounds from direct dark matter searches. Such a coupling structure naturally arises for Majorana
fermion dark matter.

To investigate the exclusion power of the particle-level measurements considered, we scanned a range in plausible mediator masses
(:math:`M_{Z^{\prime}}`) and dark matter masses (:math:`M_{\rm DM}`) within this model for three choices of the coupling of the mediator to the 
SM (:math:`g_{q}`). 
The results at the time are shown in the paper :cite:`Butterworth:2016sqg`. By now, however, most of the parameter plane is excluded for all
of them except the "challenging" scenario, which (:math:`{{g_{q}}}= 0.25, {{g_{\rm DM}}}= 1`:) is also a common benchmark choice for
other studies of similar models, e.g. LPCC led studies (see :doc:`../DMsimp_s_spin1/index` ) and is the only one updated here.
(*Last updated Contur 2.0.x, Rivet 3.1.4, Herwig 7.2.2,  23/05/2021, using correlation information from the experimental measurements where available.*)

.. image:: images/dominantPools0_corr.png
            :scale: 80%

The purple line indicates the perturbative unitarity constraint.
At low :math:`M_{Z^{\prime}}`, vector-boson-plus-jet measurements (especially photon-plus-jet) have sensitivity.
At low :math:`M_{\rm DM}`, the ATLAS missing-energy-plus-jet measurement :cite:`ATLAS:2017txd` dominates.
At higher :math:`M_{\rm DM}` and :math:`M_{Z^{\prime}}`, the dijet analyses have most impact 
(:cite:`CMS:2017jfq`, :cite:`CMS:2016lna`, :cite:`ATLAS:2017ble`).

NB the lowest mass point generated is :math:`M_{\rm Z^\prime}= 10` GeV, so the limit does not really extend to zero. For a
zoom on the low mass region in a similar model, see :doc:`../DMsimp_s_spin1/index`.

The model files are available in the DM_vector_mediator_UFO directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/DM>`_


