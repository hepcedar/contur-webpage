Gildener-Weinberg Higgs Bosons (2024)
=====================================

*Jon Butterworth, Ken Lane, David Sperka, Joshua Giblin-Burnham, Haoyu Wang*

An initial study was performed at the Les Houches workshop 2019, see
*Confronting Gildener-Weinberg Higgs bosons with LHC measurements* in :cite:`Brooijmans:2020yij` 
for the model details etc. Essentially the model imposes a relationship between the Higgs masses indicated by the
purple line on the mass plot.
Note the inverted definition of :math:`\tan\beta` described in the proceedings.
Updated results are given below (skip to the end for the latest).

The most recent update with Contur 2.5.1 is shown below.
The solid black and dashed black contours now show the 95% and 68% limits where the SM predictions for the background are used directly.
The expected 95% exclusion is show by the dotted black line, and the red dotted line shows a rough estimate of the eventual HL-LHC sensitivity.

.. image:: images/combinedHybrid-scan1-2024.png
            :scale: 70%

.. image:: images/combinedHybrid-scan2-2024.png
            :scale: 70%

The scan in :math:`\tan\beta` is performed for points on the purple line in the previous plot.		    

For the mass scan, the whole region is disfavoured at the one sigma level for :math:`\tan\beta = 0.5`, with the majority of
masses below 250 GeV being disfavoured at two sigma. For mass values respecting the G-W sum constraint,
:math:`\tan\beta > 0.6` is mostly difavoured at two sigma.

The plot below shows which final states give the most exclusion. Hadronic tops dominate over most of the region,
with isolated photons giving the highest exclusion for :math:`M_A \approx 340` GeV.

.. image:: images/dominantPools0-scan1-2024.png
            :scale: 45%

.. image:: images/dominantPools0-scan2-2024.png
            :scale: 45%

Note that the hadronic top SM predictions used (as provided in the publications) do not include estimates of the 
SM theory uncertainties.
Since these analyses dominate in much of the parameter space, these limits should be treated as preliminary, and will
be updated to include estimated SM theory uncertainties as soon as possible.

The model files are available in the Gildener-Weinberg directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/2HDM>`_
 
