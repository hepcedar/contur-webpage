Real Scalar Triplet (2023)
==========================

*Jon Butterworth, Hridoy Debnath, Pavel Fileviez Perez, Francis Mitchell*

See 
**Custodial Symmetry Breaking and Higgs Signatures at the LHC**
:cite:`Butterworth:2023rnw` 

For convenience, the model files are also available in the Contur installation in the 
models directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/>`_







