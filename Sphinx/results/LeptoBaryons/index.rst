Dark Matter from Anomaly Cancellation (2024)
============================================

*Jon Butterworth, Hridoy Debnath, Pavel Fileviez Perez, Yoran Yeh*

See 
**Dark Matter from Anomaly Cancellation at the LHC**
:cite:`Butterworth:2024eyr` 

For convenience, the model files are also available in the Contur installation in the 
models directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/LeptoBaryons>`_







