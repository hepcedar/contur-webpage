B, T, X, Y production with coupling to first, second or third generation SM quarks (2020)
=========================================================================================


*Khadeejah Bepari, Andy Buckley, Jack Burton, Jon Butterworth, Louie Corpe, Dan Ping (Joanna) Huang, Puwen Sun, Ben Waugh*

Vector-Like quarks (VLQ) are hypothetical coloured, non-chiral, spin-1/2 particles. 
While fourth-generation quarks with chiral couplings are in general excluded by 
measurements at the LHC and elsewhere :cite:`Djouadi:2012ae`, vector-like quarks are not, and remain the simplest example of 
coloured fermions not yet excluded by data :cite:`Aguilar-Saavedra:2013qpa`.
VLQs occur in several non-supersymmetric theories beyond-the-standard model that propose solutions to the hierarchy problem, 
offering mechanisms by which the Higgs mass is stabilised :cite:`Aguilar-Saavedra:2013qpa`. 
New sources of CP violation can also be introduced by these new particles :cite:`Chen:2015uza`.

The main results are in an **extensive study of these models using Contur 1.2.2/Rivet 3.1.1 carried out in 2020** 
:cite:`Buckley:2020wzk` 
using a generic model from Buchkremer et al :cite:`Buchkremer:2013bha`, where the four VLQ states X, T, B and Y are 
added as new coloured spin-1/2 objects to the SM, embedded in complete :math:`SU(2)_L` representations. 

Some of the results in that paper have been updated with a later version of Contur and Rivet, containing more
LHC measurements. These results, shown below, give the limits from Contur in the VLQ mass versus overall
coupling :math:`\kappa` plane.

**First quark generation, W:H:Z=0:0:1**

.. image:: images/1GenWHZ001.png
            :scale: 70%
.. image:: images/1GenWHZ001_conturv2.png
            :scale: 45%

*Left: Result from paper using Contur 1.2.2 and Rivet 3.1.1*
*Right: Updated scan using Contur 2.0.x and Rivet 3.1.4, 07/07/2021.*

		    
**Second quark generation, W:H:Z=0:0:1**

.. image:: images/2GenWHZ001.png
            :scale: 70%
.. image:: images/2GenWHZ001_conturv2.png
            :scale: 45%

*Left: Result from paper using Contur 1.2.2 and Rivet 3.1.1*
*Right: Updated scan using Contur 2.0.x and Rivet 3.1.4, 07/07/2021.*

		    
**Third quark generation, W:H:Z=0:0:1**

.. image:: images/3GenWHZ001.png
            :scale: 70%
.. image:: images/3GenWHZ001_conturv2.png
            :scale: 45%

*Left: Result from paper using Contur 1.2.2 and Rivet 3.1.1*
*Right: Updated scan using Contur 2.0.x and Rivet 3.1.4, 07/07/2021.*

*The ATLAS WW pool is sky blue in the plot above, and is replced by as "L1L2" pools, appearing as turquoise, in the updated plot.*

Note the change in the most sensitive analysis pool used to set the limit at each parameter point.
This change comes from the addition of the ATLAS 13 TeV inclusive four lepton measurement :cite:`ATLAS:2021kog`
for VLQs coupling to the third generation quarks,
and from the CMS 13 TeV Z boson cross-section measurements :cite:`CMS:2019raw`, :cite:`CMS:2018mdf`
for VLQs coupling to the first and second generation quarks. In all cases the exclusion at 95% CL
(solid white line) is more stringent with the additional data.

The model files and example Herwig steering file are available in the VLQ_UFO 
directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/VLQ>`_ .

Initial studies were carried out by KB and JB, supervised by JMB, DH and BW, as UCL final year undergraduate projects. Paper
authors as given, updates by DH.


