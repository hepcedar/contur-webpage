Heavy Dark Mesons and Composite Dark Matter (2024)
==================================================

A composite dark matter model for Gaugephobic and Gaugephilic :math:`SU(N_D) \,\,;\,N_D\in\{2,4\}` was analysed using CONTUR and data as background in *New sensitivity of LHC measurements to Composite Dark Matter Models* :cite:`Butterworth:2021jto`.

The following heatmaps show new constraints using the **Standard Model as background** (SMBG) (black contours) in the SU(2) case. Additionally, the expected exclusion (black dotted contours) and the expected :math:`2 \sigma` exclusion at the HL-LHC (red dotted line) are shown. The solid and dashed lines represent the 95% and 68% exclusion respectively. The HL-LHC exclusions is estimated as described in :cite:`Butterworth:2024eyr`.

All plots were made using the Contur 3.0.0 and used available data from :math:`\sqrt{s} =` 7,8 and 13 TeV.

|legend|

.. |legend| image:: figures/2024_update_plots/legend.jpg
   :width: 100%

Legend for the dominant pools.

-------------------
Gaugephilic L
-------------------

|GaugephilicSU2L_myscan00| |GaugephilicSU2L_myscan02| |GaugephilicSU2L_myscan01|

.. |GaugephilicSU2L_myscan00| image:: figures/2024_update_plots/GaugephilicSU2L_myscan00.jpg
    :width: 32%

.. |GaugephilicSU2L_myscan02| image:: figures/2024_update_plots/GaugephilicSU2L_myscan02.jpg
    :width: 32%

.. |GaugephilicSU2L_myscan01| image:: figures/2024_update_plots/GaugephilicSU2L_myscan01.jpg
    :width: 32%
   
   
Heatmaps showing new dominant pools for the Gaugephilic :math:`SU(2)_L` model.

For :math:`\eta>0.50`, the high-mass Drell-Yan process dominates, as :math:`\rho_D` is not kinematically allowed to decay into :math:`\pi_D \pi_D`, and therefore decays resonantly to fermions.

|histogram1|

.. |histogram1| image:: figures/2024_update_plots/d57_philic_L.jpeg
    :width: 49%

Example histogram for point (:math:`m_{\pi_D}=574`, :math:`\eta=0.36`) in the Gaugephilic :math:`SU(2)_L` model.

From the above figures there is a large disparity between the expected exclusion and the actual exclusion.
This can be explained by the fact that the standard model prediction is already above the data is several measurements,
most notable in the top-antitop measurents and in the ttbb measurement, as shown below. The BSM contribution thus moves the prediction even further from the data.
Contributions from other histograms lead to a 99.57% exclusion, which is larger than the expected 82.55% for that reason.

Note that the hadronic top SM predictions used (as provided in the publications) do not include estimates of the 
SM theory uncertainties.
Since these analyses dominate in much of the parameter space, these limits should be treated as preliminary, and will
be updated to include estimated SM theory uncertainties as soon as possible.


-----------------
Gaugephobic L
-----------------


|GaugephobicSU2L_myscan01| |GaugephobicSU2L_myscan00|


.. |GaugephobicSU2L_myscan00| image:: figures/2024_update_plots/GaugephobicSU2L_myscan00.jpg
    :width: 49%

.. |GaugephobicSU2L_myscan01| image:: figures/2024_update_plots/GaugephobicSU2L_myscan01.jpg
    :width: 49%


Heatmaps showing new dominant pools for the Gaugephobic :math:`SU(2)_L` model. The plot on the right has been made as a direct comparison
to the one obtained at :cite:`ATLAS:2024xbu` (Fig. 13b).
As can be seen, the updated limits from Contur are quite similar to the ATLAS search.


Again there is a large disparity between the expected exclusion and the actual exclusion due to the same reason as before.

-----------------
Gaugephobic R
-----------------


|GaugephobicSU2R_myscan01| |GaugephobicSU2R_myscan00|


.. |GaugephobicSU2R_myscan00| image:: figures/2024_update_plots/GaugephobicSU2R_myscan00.jpg
    :width: 49%
	    
.. |GaugephobicSU2R_myscan01| image:: figures/2024_update_plots/GaugephobicSU2R_myscan01.jpeg
    :width: 49%


Heatmaps showing new dominant pools for the Gaugephobic :math:`SU(2)_R` model.

At lower :math:`m_{\pi_D}`, there is a rapid change in CLs between the :math:`\eta<0.50` (where :math:`\rho_D` can decay into :math:`\pi_D \pi_D`), and :math:`\eta>0.50` (where it cannot).

For convenience, the model files are also available in the Contur installation in the 
Heavy Dark Mesons directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/>`_


*Contributors: Daniel Baig, Jon Butterworth, Louie Corpe, Xiangchen Kong, Suchita Kulkarni, Pawel Mucha, Marion Thomas*



