Spontaneously-broken B-L Gauge Model with a Z', second Higgs and Heavy Neutrinos (2021)
=======================================================================================

*Shyam Amrith, Jon Butterworth, Frank Deppisch, Wei Liu, Abhinav Varma and David Yallup*

See *LHC Constraints on a B-L Gauge Model using Contur* :cite:`Amrith:2018yfb`.

**Case A**


.. image:: images/CaseA/combinedHybrid.png
            :scale: 100%

*Image updated for Rivet 3.0.1, 30/10/2019*

**Case B**


.. image:: images/CaseB/combinedHybrid.png
            :scale: 100%

*Image updated for Rivet 3.0.1, 30/10/2019.*

*Note decrease in sensitivity at low masses. This is due to a bug in the isolation criteria for leptons applied in ATLAS_2012_I1203852. Bug was present in Rivet 2.7, fixed in 3.0*


**Case C**

*Image updated for Rivet 3.0.1, 30/10/2019.* 

*Note decrease in sensitivity at low masses as for Case B*


.. image:: images/CaseC/combinedHybrid.png
            :scale: 100%

**Case D**


.. image:: images/CaseD/combinedHybrid.png
            :scale: 100%

*Image updated for Rivet 3.0.1, 31/10/2019.* 


**Case E**

The first Figure below is from the paper, and the second Figure is an updated scan using Contur 2.0.x and Rivet 3.1.4
where the colour indicates the most sensitive analysis pool at each scan point. An increase in sensitivity is visible
particularly in the high mass region, driven largely by the addition of the ATLAS run 2 inclusive four lepton measurement :cite:`ATLAS:2021kog`.

.. image:: images/CaseE/combinedHybrid.png
            :scale: 100%

*Image updated for Rivet 3.0.1, 31/10/2019.* 

.. image:: images/CaseE/update-release2/dominantPools0.png
            :scale: 70%

*Updated scan using Contur 2.1.x and Rivet 3.1.4, 07/07/2021.*

See :cite:`Amrith:2018yfb` for explanation of which each Case means and more details.

The model files are available in the B-L-3 directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/Models/B-L>`_
