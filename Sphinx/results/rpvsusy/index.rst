UDD term with a :math:`\tilde{\tau}` LSP (2018)
-----------------------------------------------

*Jon Butterworth, Marta Czurylo, Ben Waugh, David Yallup*

*Preliminary*

Supersymmetric extensions of the SM typically conserve a quantum number known as R-Parity, written as
:math:`R_p = (−1)^{3B+L+2S}`. This has the effect of suppressing proton decay, excluding the single production 
of supersymmetric particles, and ensuring the stability of the lightest SUSY particle (LSP). However, the most 
general SUSY lagrangian allows R-Parity violating (RPV) terms; subsets of these terms can be included in a model without 
inducing unacceptably high rates of proton decay. RPV superymmetric models retain many of the theoretical 
motivations of supersymmetry (and may in some cases even retain a viable Dark Matter candidate). However,
R-Parity violation in general can change the collider phenomenology significantly. For example in
many cases the LSP decay removes the typical missing momentum signatures upon which many collider searches rely. For this reason
the phenomenology of RPV models has been studied at past colliders (see for example  :cite:`Allanach:1999bf,Butterworth:1992tc,Giudice:1996dm`) 
and is currently a topic of interest at the LHC :cite:`Dercks:2017lfq`, perhaps especially as more conventional SUSY searches continue to draw blanks. 

This work starts from the SUSY Les Houches accord :cite:`Skands:2003cj` file bundled with the Herwig 7.1.2 release :cite:`Bellm:2017bvx`. 
This provides a minimal supergravity (mSUGRA) model with parameters 
:math:`m_0 = 30` GeV, :math:`m_{1/2} = 250` GeV, :math:`A_0 = −100` GeV, 
:math:`\tan\beta = 10` and :math:`sgn(\mu) = +`. The mass spectrum for the SUSY particles is generated using ISASUGRA from ISAJET :cite:`Paige:2003mg`.

Rather than modify the ultraviolet mSUGRA parameters and regenerate the spectrum, as would be required for a fully complete model, we treat this as
a simplified model and scale the masses of all the SUSY particles by a common factor to get an estimate of the sensitvity of the LHC measurements
to RPV UDD models. (The SM Higgs mass is also set to its measured value.)

The UDD couplings are commonly denoted :math:`\lambda^{''}_{ijk}`, with the :math:`ijk` labelling generation indices. 
A recent extensive study :cite:`Dercks:2017lfq` using CheckMATE :cite:`Drees:2013wra,Kim:2015wza,Dercks:2016npn` re-interpreting LHC searches in terms of RPV phenomenology summarises the limits on these couplings
from existing measurements and searches; the limits vary from aorund 0.1 to 0.5 or above, depending upon the generation. Here we allow disregard this 
and allow the coupling to be as high as 1, to see if single production of SUSY particles via the coupling itself becomes significant. However, 
even at high values of coupling, such processes were observed to not contribute significantly to the sensitivity; pair production dominates and
so the cross section for SUSY particle production is largely independent of the coupling. In addition, the decay chain and kinematics are
not strongly affected by the value of the coupling (though they may be by the generation indices), at least for values of coupling large enough 
that the LSP decays promptly, as is the case for the values considered here.

The sensitivity of the existing LHC data is illustrated for a number of different couplings, as a function of the scale factor applied 
to the SUSY mass spectrum, in the heatmap nd contour below for the :math:`\lambda^{''}_{113}` coupling. None of the other couplings considered
(:math:`\lambda^{''}_{112}, \lambda^{''}_{222}, \lambda^{''}_{323}, \lambda^{''}_{333}`) look significantly different. 
(While scanning across one coupling, the others are set to zero.)

Legend:  

.. figure:: images/colorbarkey.png
	    :scale: 90%

.. image:: archive/latest/cl_UDD113.png
           :scale: 120%
.. image:: archive/latest/ct_UDD113.png
           :scale: 120%


Generally the data are sensitive to the model up to a scale factor of around 1.6, for all couplings and independent of the generations involved. 
The sensitivity comes from a range of data, depending on the masses considered. Of particular importance in various regions 
are the W+jet measurements :cite:`CMS:2016sun`, Z+jets :cite:`ATLAS:2014sjq` and the four-lepton lineshape :cite:`ATLAS:2015rsx`, 
presumably because of enhanced gauge boson production in SUSY cascades,
and the multijet measurements :cite:`ATLAS:2015xtc`, which will also be affected by the RPV decay of the LSP. 
Translating the scale factors into terms of the
masses of the SUSY particles allows some rough comparison with previous results as shown below (where [4] refers to :cite:`Dercks:2017lfq`).

.. image:: archive/latest/table.png

The sensitivity is comparable, but generally below that of the reinterpreted searches. This is likely due to the fact that even for 8 TeV data 
(as used in :cite:`Dercks:2017lfq`) there are many more searches than measurements available, and is possibly also affected by our 
approximate treatment of the SUSY mass spectrum. The results are encouraging enough to merit further
study, with more data as it becomes available, with a more correct treatment of the the SUSY parameters, and for a wider range of SUSY scenarios.



