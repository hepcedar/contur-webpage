Type II Seesaw model and the W mass (2022)
==========================================

*Jon Butterworth, Julian Heeck, Si Hyun Jeon, Olivier Mattelaer, Richard Ruiz*

See 
**Testing the Scalar Triplet Solution to CDF's Fat WW Problem at the LHC**
:cite:`Butterworth:2022dkt` 

For convenience, the model files are also available in the Contur installation in the 
models directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/>`_







