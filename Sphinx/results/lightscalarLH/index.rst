Light Scalars with EFT couplings (2021)
=======================================

*Andy Buckley, Jon Butterworth, Louie Corpe, Sylvain Fichet, Linda Finco, Suzanne Gascon-Shotkin, David Grellscheid, Gregory Moreau, Peter Richardson, Graeme Watt, Xilin Wang, David Yallup, Song Zhang*

See: 
   * *Sensitivity of current (and future?) LHC measurements to a new light scalar particle* in the 2017 Les Houches proceedings :cite:`Brooijmans:2018xbu` for a description of the model and the motivation.
   * Some discussion and updated results in :cite:`Butterworth:2019wnt`
   * More updates, where the model is used as the first demonstrator for the use of both correlation information and theory information, in the Les Houches 2019 proceedings :cite:`Brooijmans:2020yij`.

The key features of the model are the addition of a light scalar particle :math:`\phi` which may be odd or even under CP. 
The parameters are then the mass :math:`M_\phi` and the couplings to SM particles, which
are taken to be effective couplings governed by some set of scales :math:`\Lambda_i` for each class of SM particle :math:`i`. 
For the CP-odd case, all scales are set to very high values except for those to :math:`W, B`, which are set equal to each other and scanned over a range. 
For the CP-even case, the scale associated with the coupling to the Higgs is also set equal to this value and scanned over. 
	 
The sensitivities derived from multiple measured distributions are
combined into heatmaps which delineate exclusion regions and contours in the parameter space of
:math:`\Lambda`\ and :math:`M_{\phi}`.

**CP-odd Light Scalar Model**

Heatmap and contour for all available data as of Rivet 3.1.6,  Contur 2.4.x.

.. image:: images/combinedHybrid-cpo-sm.png
            :scale: 70%


**CP-even Light Scalar Model**

Heatmap and contour for all available data as of Rivet 3.1.6 Contur 2.4.x.

.. image:: images/combinedHybrid-cpe-sm.png
            :scale: 70%

The white line shows the 95% exclusion assuming the SM is identical to the data. The solid and dashed black lines show the 95% and 68% exclusions
respectively using the SM predictions directly as background. The dotted black line show the expected 95% exclusion.
		    
Note that after the first study was was completed, we became aware of a closely related and comparable 
study by A. Mariotti et al :cite:`Mariotti:2017vtv` which reached some similar conclusions.

The latest results greatly extend the previous exclusion to high scales, driven primarily by the ATLAS full run 2 diphoton
measurement :cite:`ATLAS:2021mbt`, as can been seen in
the plot below, where the dominant signature at each parameter point is colour-coded.

The sensitivity drops for :math:`M_\phi < 100` GeV because the fiducial phase space of the measurement is restricted in this region by
the transverse momentum cuts on the photons. The expected sensitivity in the region exceeds the actual sensitivity because the SM prediction lies somewhat
below the data here. At higher masses the expected limit also exceed the actual limit, again primarily because the SM predictions drop
below the data in this region; in neither case however does the data exhibit the resonant bump predicted by the model.

**CP-odd Light Scalar Model**

.. image:: images/dominantPools-cpo-sm.png
            :scale: 45%
	   
	   
**CP-even Light Scalar Model**

.. image:: images/dominantPools-cpe-sm.png
            :scale: 45%

The model files are available in the NSCPO, NSCPE directories `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/Neutral_Scalar>`_
