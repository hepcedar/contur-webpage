:math:`Z^\prime` Models for Neutral Current B−Anomalies (2021)
==============================================================

*Ben Allanach, Jon Butterworth, Tyler Corbett*

For the latest on this, see :cite:`Allanach:2021gmj`, with updated models and fits.

The first analysis was :cite:`Allanach:2019mfl`, and for the Third Family Hypercharge model in that paper there is an update below.

The Contur limits for three simple models were shown in Fig.5 of the first paper (as well as in the Fig.4 summary for the TFHM).
These are updated below for the TFHM with the data in Contur 2.0.2, Rivet 3.1.4, and also making use of the correlation
information in the measurement uncertainties where available (something Contur could not do at the time of the publication).

.. image:: images/TFHM/combinedHybrid-paper.png
            :scale: 90%

Third family hypercharge model exclusion for measurements in Rivet 2.7 (from the paper):

.. image:: images/TFHM/combinedHybrid.png
            :scale: 80%

Third family hypercharge model exclusion for measurements in Contur 2.0.2,  Rivet 3.1.4.

In the plot below on the left, the colours indicate which final states are dominantly providing the sensitivity.
The main relevant addition since the paper is the CMS 13 TeV dilepton measurement :cite:`CMS:2018mdl`, which provides additional
exclusion even though it uses only 3.2/fb of Run 2 data. (Note that the interpolation settings in Contur were changed,
meaning that the sensitivity around 300 GeV mass and high :math:`\theta_{sb}` seems significantly less,
although the heatmap itself shows very little difference in this region.)

.. image:: images/TFHM/dominantPools0.png
            :scale: 45%

.. image:: images/TFHM/dominantPools0_searches.png
            :scale: 45%

Contur now also has a small number of searches available, exploiting the detector-smearing machinery now added
to Rivet :cite:`Buckley:2019stt`. Amongst these searches is the ATLAS full run 2 dilepton search :cite:`ATLAS:2019erb` used in
:cite:`Allanach:2019mfl`. The righthand plot above shows the impact of adding this. The combined  exclusion goes up to :math:`Z^\prime`
masses of around 2 TeV, somewhat higher than the exclusion from this search alone based on the fit in :cite:`Allanach:2019mfl`.

For reference, the summary including searches from :cite:`Allanach:2019mfl` :

.. image:: images/atlas139y3.png
            :scale: 100%

The model files are available in the Zprime_TFHM_Mix_UFO, Zprime_MUM_UFO and Zprime_MDM_UFO directories `here <https://gitlab.com/hepcedar/contur/-/tree/main/Models/TFHM>`_

 

