Two Higgs-Doublet Dark Matter Model with Pseudoscalar Mediator (2024)
=====================================================================


`This model <https://github.com/LHC-DMWG/model-repository/tree/main/models/Pseudoscalar_2HDM>`_ :cite:`Bauer:2017ota` is well known and used within the 
`LHC Dark Matter working group <https://lpcc.web.cern.ch/content/lhc-dm-wg-dark-matter-searches-lhc>`_, and by ATLAS :cite:`Aaboud:2019yqu` and CMS :cite:`Sirunyan:2018gdw`.

The major results and discussion are in 
**A study of collider signatures for two Higgs doublet models with a Pseudoscalar mediator to Dark Matter** :cite:`Butterworth:2020vnb` 
(JMB, MH, PP, AV), which supersedes a study of the model presented in the Les Houches 2019 proceedings :cite:`Brooijmans:2020yij`: 
(*Sensitivity of LHC measurements to a two-Higgs-doublet plus pseudoscalar DM model*).

The model was also used in *Determining sensitivity of future measurements to new physics signals* (LC, DK), in the same proceedings.

For convenience, the model files are also available in the Contur installation in the 
Pseudoscalar_2HDM directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/DM>`_

To observe the effect of lifting the degeneracy of the exotic Higgs bosons: :math:`H,H^{\pm},A`, three separate cases are studied, as detailed in 
:cite:`Butterworth:2020vnb`.

Case 1 involves a scan over the parameter space (:math:`M_a`, :math:`\tan \beta`).
Case 2 involves a scan over the parameter space (:math:`M_H = M_H^{\pm}`, :math:`M_A`).
Case 3 involves a scan over the parameter space (:math:`M_A = M_H^{\pm}`, :math:`M_H`).
The parameters values used for each scan are given in :cite:`Butterworth:2020vnb`.


Case 1
------

Below is an update for Figure 2 of the paper (see the paper for explanation) using Contur 2.5.x/Rivet 3.1.10. The previous plot
was made using the data as background, which is less rigorous than using SM predictions. The plot below is made using the SM predictions.
It can be observed that the 95% exclusion region at the top of the plot has receded.
The entire parameter space is expected to be  excluded at the 95% confidence level with data from the High-Luminosity LHC.

.. figure:: images/contur_2_5/case_1_fine.png
            :scale: 40%

Case 2 & 3
----------

The exclusions for both case 2 and case 3 are relatively independent from :math:`M_a`. The exclusions come from a 
number of measurements, but by far the dominant is ATLAS_13_TTHAD. At low :math:`M_a` in the 95% exclusion region, a lot of
exclusion comes from the ATLAS_8_LLMET measurement, however, this disappears almost entirely by :math:`M_a` = 500 GeV.
The entire parameter space is expected to be excluded at the 95% confidence level with data from the High-Luminosity LHC
for all following plots. Note that the hadronic top SM predictions used (as provided in the publications)
do not include estimates of the SM theory uncertainties.

Case 2
~~~~~~

.. container:: grid-container

   .. container:: grid-row

      .. container:: grid-item

         .. figure:: images/contur_2_5/case_2_100.png
            :width: 100%
            :align: center

            :math:`M_a` *= 100 GeV*

      .. container:: grid-item

         .. figure:: images/contur_2_5/case_2_200.png
            :width: 100%
            :align: center

            :math:`M_a` *= 200 GeV*

   .. container:: grid-row

      .. container:: grid-item

         .. figure:: images/contur_2_5/case_2_300.png
            :width: 100%
            :align: center

            :math:`M_a` *= 300 GeV*

      .. container:: grid-item

         .. figure:: images/contur_2_5/case_2_500.png
            :width: 100%
            :align: center

            :math:`M_a` *= 500 GeV*

Case 3
~~~~~~

.. container:: grid-container

   .. container:: grid-row

      .. container:: grid-item

         .. figure:: images/contur_2_5/case_3_100.png
            :width: 100%
            :align: center

            :math:`M_a` *= 100 GeV*

      .. container:: grid-item

         .. figure:: images/contur_2_5/case_3_200.png
            :width: 100%
            :align: center

            :math:`M_a` *= 200 GeV*

   .. container:: grid-row

      .. container:: grid-item

         .. figure:: images/contur_2_5/case_3_300.png
            :width: 100%
            :align: center

            :math:`M_a` *= 300 GeV*

      .. container:: grid-item

         .. figure:: images/contur_2_5/case_3_500.png
            :width: 100%
            :align: center

            :math:`M_a` *= 500 GeV*


*Contributors: Jon Butterworth, Louie Corpe, Martin Habedank, Deepak Kar, Mungo Mangat, Priscilla Pani, Andrius Vaitkus*
