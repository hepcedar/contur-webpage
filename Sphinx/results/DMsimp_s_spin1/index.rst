Vector or Axial-Vector mediator, Dirac fermion DM (2025)
========================================================

This class of models is one of those considered in the ATLAS search summary :cite:`ATLAS:2024nkb`,
see: https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2024-010/ , from where
the ATLAS plots are taken.
We compare equivalent scenarios here (see :cite:`Butterworth:2019wnt` for an early set of Contur results):

Vector mediator
---------------

*ATLAS Searches* 

.. image:: images/VV/Scan1/ATLAS_VV_noLep.png
            :scale: 30%

*Contur*

.. image:: images/VV/Scan1/dominantPools.png
            :scale: 60%

(*Contur 3.1.0, Rivet 4.0.3, Herwig 7.2, 13 TeV data only, 3/3/2025*)

In the heatmap above, the dotted line delineates the expected region of 95% exclusion
The solid line delineates the actual 95% exclusion region.
The dashed line is the same as the solid line but delineates the 68% (2-sigma) exclusion region.

The colour of each square indicates which analysis pool contributes the largest exclusion,
according to the key in the sidebar of the heatmap. The measurements in each pool are
defined on the :doc:`../../datasets/index` page.

Over most of the region, top measurements are the most sensitive.
The switch-on of :math:`t\bar{t}` production at :math:`M_{Z^{\prime}} = 350` GeV can be seen, when the dilepton
and missing energy analyses become important, with the boosted hadronic top measurements e.g. :cite:`ATLAS:2018orx`,
taking over above a higher threshold, due to the high transverse momentum cut on the tops.
In the ATLAS search result, the dijet channel dominates at the highest :math:`M_{\rm Z^\prime}` region,
The dijet measurements are less sensitive, because they currently only includes 3.2/fb of
data, while the search uses the full Run2 140/fb. Where both measurement and search exists with
the full Run2 integrated luminosity (for example, top :cite:`ATLAS:2022mlu` and missing transverse energy :cite:`ATLAS:2024vqf`),
limits obtained are very similar.

Another scenario has the mediator coupling to quarks set to 0.1, and a coupling of 0.01 to leptons.

*ATLAS Searches*

.. image:: images/VV/Scan2/ATLAS_VV_Lep.png
            :scale: 30%

*Contur*

The measurements responsible for the highest exclusion using SM as background are shown in the heatmap below.

.. image:: images/VV/Scan2/dominantPools.png
            :scale: 60% 

(*Contur 3.1.0, Rivet 4.0.3, Herwig 7.2, 13 TeV data only, 3/3/2025*)

The sensitivity is again limited at higher :math:`M_{\rm Z^\prime}` compared to the searches because the
dijet measurements :cite:`ATLAS:2017ble` only use 3.2/fb of data


Axial-Vector mediator
---------------------

As with the vector case, the coupling to quarks is set to 0.25, no coupling to leptons.

*ATLAS Searches*

.. image:: images/AA/Scan1/ATLAS_AA_noLep.png
            :scale: 30%

*Contur*

.. image:: images/AA/Scan1/dominantPools.png
            :scale: 60%          

(*Contur 3.1.0, Rivet 4.0.3, Herwig 7.2, 13 TeV only, 2/3/2025*)
		    
As with the vector case, the coupling to quarks is set to 0.1, but now the coupling to
leptons is also 0.1.

*ATLAS Searches*

.. image:: images/AA/Scan2/ATLAS_AA_Lep.png
            :scale: 30%

*Contur*

.. image:: images/AA/Scan2/dominantPools.png       
            :scale: 60% 

(*Contur 3.1.0, Rivet 4.0.3, Herwig 7.2, 13 TeV only. 5/3/2025*)

Lepton signatures now dominate due to the higher coupling compared to that used in the vector case,
with the 13 TeV 139/fb dilepton resonance search from ATLAS
extending the sensitivity at high mass.

Summary
-------

In general when made with the same luminosity, the limits using the measurements match those from the searches.
When the coupling to the SM quarks is 0.25, the measurments in Contur do not have the reach of the searches
at high :math:`M_{\rm Z^\prime}`, because the dijet measurement in the end drives this region, and the available
measurement only uses 3.2/fb, while the searches use 140/fb. The top measurements are quite powerful, particularly
the ATLAS fully-hadronic boosted top measurement :cite:`ATLAS:2018orx`.

When the coupling to the SM quarks is reduced to 0.1, and some coupling to the SM leptons is introduced, 
the limits from measurements at higher DM masses are again limited, primarily due to the lack of
high-luminosity measurments of dijets. 

The model files are available in the DMsimp_s_spin1 directory `here <https://gitlab.com/hepcedar/contur/-/tree/main/data/Models/DM>`_

*Contributors: Jon Butterworth, Mungo Mangat, Charlie Velasquez, Yoran Yeh*




