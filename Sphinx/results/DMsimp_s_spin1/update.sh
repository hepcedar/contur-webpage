#!/bin/bash

RDIR=~/contur-web-results
echo "Updating from $RDIR"

STUB=VV/Scan1
SDIR=$RDIR/DMsimp_s_spin1/$STUB/ANALYSIS
contur-plot $SDIR/contur.map mY1 mXd -o $SDIR/conturPlot 
convert -density 192  $SDIR/conturPlot/dominantPools0.pdf -quality 100 images/$STUB/dominantPools0.png

STUB=VV/Scan1_lowmass
SDIR=$RDIR/DMsimp_s_spin1/$STUB/ANALYSIS
contur-plot $SDIR/contur.map mY1 mXd -o $SDIR/conturPlot -xl
convert -density 192  $SDIR/conturPlot/dominantPools0.pdf -quality 100 images/$STUB/dominantPools0.png

STUB=VV/Scan2
SDIR=$RDIR/DMsimp_s_spin1/$STUB/ANALYSIS
contur-plot $SDIR/contur.map mY1 mXd -o $SDIR/conturPlot 
convert -density 192  $SDIR/conturPlot/dominantPools0.pdf -quality 100 images/$STUB/dominantPools0.png

STUB=VV/Scan2
SDIR=$RDIR/DMsimp_s_spin1/$STUB/SEARCHES
contur-plot $SDIR/contur.map mY1 mXd -o $SDIR/conturPlot 
convert -density 192  $SDIR/conturPlot/dominantPools0.pdf -quality 100 images/$STUB/dominantPools0_searches.png

STUB=VV/Scan2_lowmass
SDIR=$RDIR/DMsimp_s_spin1/$STUB/SEARCHES
contur-plot $SDIR/contur.map mY1 mXd -o $SDIR/conturPlot -xl
convert -density 192  $SDIR/conturPlot/dominantPools0.pdf -quality 100 images/$STUB/dominantPools0_searches.png

STUB=AA/Scan1
SDIR=$RDIR/DMsimp_s_spin1/$STUB/ANALYSIS
contur-plot $SDIR/contur.map mY1 mXd -o $SDIR/conturPlot 
convert -density 192  $SDIR/conturPlot/dominantPools0.pdf -quality 100 images/$STUB/dominantPools0.png

STUB=AA/Scan2
SDIR=$RDIR/DMsimp_s_spin1/$STUB/ANALYSIS
contur-plot $SDIR/contur.map mY1 mXd -o $SDIR/conturPlot 
convert -density 192  $SDIR/conturPlot/dominantPools0.pdf -quality 100 images/$STUB/dominantPools0.png

STUB=AA/Scan2
SDIR=$RDIR/DMsimp_s_spin1/$STUB/SEARCHES
contur-plot $SDIR/contur.map mY1 mXd -o $SDIR/conturPlot -xl
convert -density 192  $SDIR/conturPlot/dominantPools0.pdf -quality 100 images/$STUB/dominantPools0_searches.png

