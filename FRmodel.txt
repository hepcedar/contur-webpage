Not recommended to change these directly, but this is the documentation of the ParticleData object parameter from
Peter R:

create ThePEG::ParticleData H
# values set to 999999 are recalculated later from other model parameters
#    Name of reposotory
#    PDG code
#    Name of particle printed
#    Mass
#    Width
#    Limit on how far off shell (GeV)
#    Lifetime
#    Electric charge
#    Colour charge (antidown=1)
#    Spin (2S+1)
#    0=unstable, 1=stable
#    (for constituent particles, constiuent mass)
setup H 25 H 125 0.00407 0.0407 4.8483283317e-11 0 0 1 0
insert /Herwig/NewPhysics/NewModel:DecayParticles 0 H
insert /Herwig/Shower/ShowerHandler:DecayInShower 0 25 #  H
insert /Herwig/FRModel/V_GenericHPP:Bosons 0 H
insert /Herwig/FRModel/V_GenericHGG:Bosons 0 H


PID digits (base 10) are: n nr nl nq1 nq2 nq3 nj (cf. Location)

